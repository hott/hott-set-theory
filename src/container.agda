{-# OPTIONS --without-K #-}

open import foundation.action-on-identifications-functions
open import foundation.dependent-pair-types
open import foundation.embeddings
open import foundation.equality-dependent-pair-types
open import foundation.function-extensionality
open import foundation.function-types
open import foundation.functoriality-dependent-pair-types
open import foundation.homotopies
open import foundation.monomorphisms
open import foundation.universe-levels

module container where

Container : ∀ la lb → UU (lsuc (la ⊔ lb))
Container la lb = Σ (UU la) (λ A → A → UU lb)

pattern _◁_ A B = (A , B)

module _ {la lb} ((A ◁ B) : Container la lb) where

  ⟦_⟧ : ∀ {l} → UU l → UU (la ⊔ lb ⊔ l)
  ⟦_⟧ X = Σ A (λ a → B a → X)

  map-⟦_⟧ : ∀ {l l'}
          → {X : UU l} {Y : UU l'}
          → (X → Y)
          → ⟦_⟧ X → ⟦_⟧ Y
  map-⟦_⟧ f = tot (λ _ → f ∘_)

  map-⟦_⟧-~ : ∀ {l l'}
            → {X : UU l} {Y : UU l'}
            → {f g : X → Y}
            → f ~ g
            → map-⟦_⟧ f ~ map-⟦_⟧ g
  map-⟦_⟧-~ {Y = Y} H (a , φ) = ap (λ h → (a , h ∘ φ)) (eq-htpy H)

  map-⟦_⟧-~-refl : ∀ {l l'}
                 → {X : UU l} {Y : UU l'}
                 → {f : X → Y}
                 → map-⟦_⟧-~ {f = f} {g = f} refl-htpy ~ refl-htpy
  map-⟦_⟧-~-refl (a , φ) = ap (ap (λ h → a , h ∘ φ)) (eq-htpy-refl-htpy _)

  is-emb-map-⟦_⟧ : ∀ {l l'}
                 → {X : UU l} {Y : UU l'}
                 → {f : X → Y}
                 → is-emb f
                 → is-emb (map-⟦_⟧ f)
  is-emb-map-⟦_⟧ {f = f} is-emb-f =
    is-emb-tot (λ a → is-mono-is-emb f is-emb-f _)

  emb-⟦_⟧ : ∀ {l l'}
          → {X : UU l} {Y : UU l'}
          → (X ↪ Y)
          → ⟦_⟧ X ↪ ⟦_⟧ Y
  pr1 emb-⟦ f , is-emb-f ⟧ = map-⟦_⟧ f
  pr2 emb-⟦ f , is-emb-f ⟧ = is-emb-map-⟦_⟧ is-emb-f

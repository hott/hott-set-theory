{-# OPTIONS --without-K #-}

open import foundation.action-on-identifications-functions
open import foundation.cartesian-product-types
open import foundation.contractible-types
open import foundation.dependent-pair-types
open import foundation.embeddings
open import foundation.equality-dependent-pair-types
open import foundation.equivalences
open import foundation.fibers-of-maps
open import foundation.function-extensionality
open import foundation.function-types
open import foundation.functoriality-dependent-function-types
open import foundation.functoriality-dependent-pair-types
open import foundation.homotopies
open import foundation.identity-types
open import foundation.monomorphisms
open import foundation.propositional-maps
open import foundation.propositions
open import foundation.structure-identity-principle
open import foundation.transport-along-identifications
open import foundation.univalence
open import foundation.universal-property-equivalences
open import foundation.universe-levels
open import foundation.whiskering-homotopies

open import container
open import notation

module container.coalgebra where

module _ {la lb} ((A ◁ B) : Container la lb) where

  ⟦_⟧-Coalg : ∀ l → UU (la ⊔ lb ⊔ lsuc l)
  ⟦_⟧-Coalg l = Σ (UU l) (λ X → X → ⟦ A ◁ B ⟧ X)
  

module _ {la lb l l'} ((A ◁ B) : Container la lb)
  ((X , m) : ⟦ A ◁ B ⟧-Coalg l) ((Y , m') : ⟦ A ◁ B ⟧-Coalg l') where

  is-hom-⟦_⟧-Coalg : (X → Y)
                   → UU (la ⊔ lb ⊔ l ⊔ l')
  is-hom-⟦_⟧-Coalg f = (m' ∘ f) ~ (map-⟦ A ◁ B ⟧ f ∘ m)

  hom-⟦_⟧-Coalg : UU (la ⊔ lb ⊔ l ⊔ l')
  hom-⟦_⟧-Coalg = Σ (X → Y) is-hom-⟦_⟧-Coalg

-- Equality of coalgebras
module _ {la lb l} ((A ◁ B) : Container la lb)
  ((X , m) : ⟦ A ◁ B ⟧-Coalg l) ((Y , n) : ⟦ A ◁ B ⟧-Coalg l)
  where

  ⟦_⟧-Coalg-Eq : UU (la ⊔ lb ⊔ l)
  ⟦_⟧-Coalg-Eq = Σ (X ≃ Y) λ e → is-hom-⟦ A ◁ B ⟧-Coalg (X , m) (Y , n) (map-equiv e)

  equiv-⟦_⟧-Coalg-Eq : ((X , m) == (Y , n))
                     ≃ ⟦_⟧-Coalg-Eq
  equiv-⟦_⟧-Coalg-Eq =
    extensionality-Σ
      (λ {Y'} n' e → is-hom-⟦ A ◁ B ⟧-Coalg (X , m) (Y' , n') (map-equiv e))
      id-equiv
      refl-htpy
      (λ Y → equiv-univalence)
      (λ m' → equiv-funext ∘e equiv-inv _ _)
      (Y , n)

module _ {la lb lx ly lz} ((A ◁ B) : Container la lb)
  ((X , m) : ⟦ A ◁ B ⟧-Coalg lx) ((Y , n) : ⟦ A ◁ B ⟧-Coalg ly)
  ((Z , k) : ⟦ A ◁ B ⟧-Coalg lz) where

  comp-hom-⟦_⟧-Coalg : hom-⟦ A ◁ B ⟧-Coalg (Y , n) (Z , k)
                     → hom-⟦ A ◁ B ⟧-Coalg (X , m) (Y , n)
                     → hom-⟦ A ◁ B ⟧-Coalg (X , m) (Z , k)
  pr1 (comp-hom-⟦_⟧-Coalg (g , β) (f , α)) = g ∘ f
  pr2 (comp-hom-⟦_⟧-Coalg (g , β) (f , α)) =
    (β ·r f) ∙h (map-⟦ A ◁ B ⟧ g ·l α)

module _ {la lb lx ly} ((A ◁ B) : Container la lb)
  ((X , m) : ⟦ A ◁ B ⟧-Coalg lx) ((Y , n) : ⟦ A ◁ B ⟧-Coalg ly)
  ((f , α) : hom-⟦ A ◁ B ⟧-Coalg (X , m) (Y , n))
  where

  comp-hom-⟦_⟧-Coalg-id-hom-right : comp-hom-⟦ A ◁ B ⟧-Coalg (X , m) (X , m) (Y , n) (f , α) (id , refl-htpy)
                                  == (f , α)
  comp-hom-⟦_⟧-Coalg-id-hom-right =
    eq-pair-eq-pr2 (eq-htpy (λ x → right-unit))

-- Subcoalgebras
module _ {la lb l} ((A ◁ B) : Container la lb)
  ((X , m) : ⟦ A ◁ B ⟧-Coalg l) where

  is-sub-⟦_⟧-Coalg : ∀ {l'} → ⟦ A ◁ B ⟧-Coalg l' → UU (la ⊔ lb ⊔ l ⊔ l')
  is-sub-⟦_⟧-Coalg (Y , n) =
    Σ (hom-⟦ A ◁ B ⟧-Coalg (X , m) (Y , n))
      (λ (f , _) → is-emb f)

  sub-⟦_⟧-Coalg : ∀ l' → UU (la ⊔ lb ⊔ l ⊔ lsuc l')
  sub-⟦_⟧-Coalg l' = Σ (⟦ A ◁ B ⟧-Coalg l') is-sub-⟦_⟧-Coalg


-- Terminal ⟦ A ◁ B ⟧-coalgebras
module _ {la lb l} ((A ◁ B) : Container la lb)
  ((X , m) : ⟦ A ◁ B ⟧-Coalg l) where

  is-terminal-⟦_⟧-Coalg : ∀ l' → UU (la ⊔ lb ⊔ l ⊔ lsuc l')
  is-terminal-⟦_⟧-Coalg l' =
    ((Y , m') : ⟦ A ◁ B ⟧-Coalg l') →
      is-contr (hom-⟦ A ◁ B ⟧-Coalg (Y , m') (X , m))


-- Equality of ⟦ A ◁ B ⟧-coalgebra homomorphisms
module _ {la lb l l'} ((A ◁ B) : Container la lb)
  {(X , m) : ⟦ A ◁ B ⟧-Coalg l} {(Y , m') : ⟦ A ◁ B ⟧-Coalg l'}
  ((f , α) (g , β) : hom-⟦ A ◁ B ⟧-Coalg (X , m) (Y , m')) where

  hom-⟦_⟧-Coalg-Eq : UU (la ⊔ lb ⊔ l ⊔ l')
  hom-⟦_⟧-Coalg-Eq =
    Σ (f ~ g) (λ H →
      α ∙h (map-⟦ A ◁ B ⟧-~ H ·r m) ~ (m' ·l H) ∙h β)

  equiv-hom-⟦_⟧-Coalg-Eq : ((f , α) == (g , β))
                         ≃ hom-⟦_⟧-Coalg-Eq
  equiv-hom-⟦_⟧-Coalg-Eq =
    extensionality-Σ
      (λ β' H' → α ∙h (map-⟦ A ◁ B ⟧-~ H' ·r m) ~ (m' ·l H') ∙h β')
      refl-htpy
      (λ x → ap (α x ∙_) (map-⟦ A ◁ B ⟧-~-refl (m x)) ∙ right-unit)
      (λ f → equiv-funext)
      (λ α' → equiv-Π-equiv-family (λ x →
                equiv-concat (ap (α x ∙_) (map-⟦ A ◁ B ⟧-~-refl (m x)) ∙ right-unit) (α' x))
              ∘e equiv-funext)
      ((g , β))

-- Embeddings of containers
module _ {la' lb' la lb} ((A' ◁ B') : Container la' lb') ((A ◁ B) : Container la lb)
  (f : A' ↪ A) (g : (a : A') → B (map-emb f a) ≃ B' a)
  where

  emb-⟦_⟧-⟦_⟧ : ∀ {l} (X : UU l) → ⟦ A' ◁ B' ⟧ X ↪ ⟦ A ◁ B ⟧ X
  emb-⟦_⟧-⟦_⟧ X =
    emb-Σ _ f (λ a' → emb-equiv (equiv-precomp (g a') X))

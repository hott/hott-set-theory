{-# OPTIONS --without-K #-}

open import foundation.dependent-pair-types
open import foundation.equivalences
open import foundation.families-of-equivalences
open import foundation.function-types
open import foundation.functoriality-dependent-function-types
open import foundation.functoriality-dependent-pair-types
open import foundation.universe-levels

module container.indexed where

IndexedContainer : ∀ {li} la lb → UU li → UU (li ⊔ lsuc (la ⊔ lb))
IndexedContainer la lb I =
  Σ (I → UU la) λ A →
  Σ ({i : I} → A i → UU lb) λ B →
    {i : I} {a : A i} → B a → I

pattern _◁_◂_ A B r = (A , (B , r))

module _ {li la lb} {I : UU li}
  ((A ◁ B ◂ r) : IndexedContainer la lb I) where

  ⦉_⦊ : ∀ {l} → (I → UU l) → (I → UU (la ⊔ lb ⊔ l))
  ⦉_⦊ X i = Σ (A i) (λ a → (b : B a) → X (r b))

module _ {li la lb l l'} {I : UU li}
  ((A ◁ B ◂ r) : IndexedContainer la lb I)
  {X : I → UU l} {Y : I → UU l'}
  where

  map-⦉_⦊ : ((i : I) → X i → Y i)
          → (i : I) → ⦉ A ◁ B ◂ r ⦊ X i → ⦉ A ◁ B ◂ r ⦊ Y i
  map-⦉_⦊ f i = tot (λ a → map-Π (f ∘ r))

  is-fiberwise-equiv-map-⦉_⦊ : (f : (i : I) → X i → Y i)
                             → is-fiberwise-equiv f
                             → is-fiberwise-equiv (map-⦉_⦊ f)
  is-fiberwise-equiv-map-⦉_⦊ f H i =
    is-equiv-tot-is-fiberwise-equiv (λ a →
      is-equiv-map-Π-is-fiberwise-equiv (λ b →
        H (r b)))

  fam-equiv-map-⦉_⦊ : (f : fam-equiv X Y)
                    → fam-equiv (⦉ A ◁ B ◂ r ⦊ X) (⦉ A ◁ B ◂ r ⦊ Y)
  pr1 (fam-equiv-map-⦉ f ⦊ i) = map-⦉_⦊ (map-equiv ∘ f) i
  pr2 (fam-equiv-map-⦉ f ⦊ i) =
    is-fiberwise-equiv-map-⦉_⦊ (map-equiv ∘ f) (is-equiv-map-equiv ∘ f) i


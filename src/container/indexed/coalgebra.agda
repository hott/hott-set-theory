{-# OPTIONS --without-K #-}

open import foundation.action-on-identifications-functions
open import foundation.contractible-types
open import foundation.dependent-pair-types
open import foundation.dependent-universal-property-equivalences
open import foundation.equality-dependent-pair-types
open import foundation.equivalences
open import foundation.families-of-equivalences
open import foundation.function-extensionality
open import foundation.function-types
open import foundation.functoriality-dependent-function-types
open import foundation.functoriality-dependent-pair-types
open import foundation.homotopies
open import foundation.identity-types
open import foundation.monomorphisms
open import foundation.postcomposition-functions
open import foundation.transport-along-identifications
open import foundation.universe-levels
open import foundation.whiskering-homotopies

open import container.indexed
open import notation

module container.indexed.coalgebra where

module _ {li la lb} {I : UU li}
  ((A ◁ B ◂ r) : IndexedContainer la lb I)
  where

  ⦉_⦊-Coalg : ∀ l → UU (li ⊔ la ⊔ lb ⊔ lsuc l)
  ⦉_⦊-Coalg l = Σ (I → UU l) (λ X → (i : I) → X i → ⦉ A ◁ B ◂ r ⦊ X i)

-- Homomorphisms
module _ {li la lb l l'} {I : UU li}
  ((A ◁ B ◂ r) : IndexedContainer la lb I)
  ((X , m) : ⦉ A ◁ B ◂ r ⦊-Coalg l)
  ((Y , n) : ⦉ A ◁ B ◂ r ⦊-Coalg l')
  where

  is-hom-⦉_⦊-Coalg : ((i : I) → X i → Y i)
                  → UU (li ⊔ la ⊔ lb ⊔ l ⊔ l')
  is-hom-⦉_⦊-Coalg f = (i : I) → (n i ∘ f i) ~ (map-⦉ A ◁ B ◂ r ⦊ f i ∘ m i)

  hom-⦉_⦊-Coalg : UU (li ⊔ la ⊔ lb ⊔ l ⊔ l')
  hom-⦉_⦊-Coalg = Σ ((i : I) → X i → Y i) is-hom-⦉_⦊-Coalg

-- Compositions of homomorphisms
module _ {li la lb l l' l''} {I : UU li}
  ((A ◁ B ◂ r) : IndexedContainer la lb I)
  ((X , m) : ⦉ A ◁ B ◂ r ⦊-Coalg l)
  ((Y , n) : ⦉ A ◁ B ◂ r ⦊-Coalg l')
  ((Z , k) : ⦉ A ◁ B ◂ r ⦊-Coalg l'')
  where

  comp-hom-⦉_⦊-Coalg : hom-⦉ A ◁ B ◂ r ⦊-Coalg (Y , n) (Z , k)
                     → hom-⦉ A ◁ B ◂ r ⦊-Coalg (X , m) (Y , n)
                     → hom-⦉ A ◁ B ◂ r ⦊-Coalg (X , m) (Z , k)
  pr1 (comp-hom-⦉_⦊-Coalg (g , β) (f , α)) i = g i ∘ f i
  pr2 (comp-hom-⦉_⦊-Coalg (g , β) (f , α)) i =
    (β i ·r f i) ∙h (map-⦉ A ◁ B ◂ r ⦊ g i ·l α i)

-- Isomorphisms
module _ {li la lb l l' l''} {I : UU li}
  ((A ◁ B ◂ r) : IndexedContainer la lb I)
  ((X , m) : ⦉ A ◁ B ◂ r ⦊-Coalg l)
  ((Y , n) : ⦉ A ◁ B ◂ r ⦊-Coalg l')
  where

  iso-⦉_⦊-Coalg : UU (li ⊔ la ⊔ lb ⊔ l ⊔ l')
  iso-⦉_⦊-Coalg =
    Σ ((i : I) → X i ≃ Y i) (λ e →
      is-hom-⦉ A ◁ B ◂ r ⦊-Coalg (X , m) (Y , n) (map-equiv ∘ e))

  equiv-hom-iso-⦉_⦊-Coalg : iso-⦉_⦊-Coalg
                          → ((Z , k) : ⦉ A ◁ B ◂ r ⦊-Coalg l'')
                          → hom-⦉ A ◁ B ◂ r ⦊-Coalg (Z , k) (X , m)
                          ≃ hom-⦉ A ◁ B ◂ r ⦊-Coalg (Z , k) (Y , n)
  equiv-hom-iso-⦉_⦊-Coalg H (Z , k) =
    equiv-Σ _
      (equiv-Π-equiv-family (λ i → equiv-postcomp (Z i) (pr1 H i)))
      (λ f → equiv-Π-equiv-family (λ i →
        equiv-Π-equiv-family (λ z →
          equiv-concat (pr2 H i (f i z)) _ ∘e
          equiv-ap (fam-equiv-map-⦉ A ◁ B ◂ r ⦊ (pr1 H) i) _ _)))

-- Terminal ⦉ A ◁ B ◂ r ⦊-coalgebras
module _ {li la lb l} {I : UU li}
  ((A ◁ B ◂ r) : IndexedContainer la lb I)
  ((X , m) : ⦉ A ◁ B ◂ r ⦊-Coalg l)
  where

  is-terminal-⦉_⦊-Coalg : ∀ l' → UU (li ⊔ la ⊔ lb ⊔ l ⊔ lsuc l')
  is-terminal-⦉_⦊-Coalg l' =
    ((Y , n) : ⦉ A ◁ B ◂ r ⦊-Coalg l') →
      is-contr (hom-⦉ A ◁ B ◂ r ⦊-Coalg (Y , n) (X , m))

-- If (X , m) is terminal then m is an equivalence
module _ {li la lb l} {I : UU li}
  {(A ◁ B ◂ r) : IndexedContainer la lb I}
  ((X , m) : ⦉ A ◁ B ◂ r ⦊-Coalg l)
  (X-is-terminal : ∀ {k} → is-terminal-⦉ A ◁ B ◂ r ⦊-Coalg (X , m) k)
  where

  is-equiv-coalg-map-is-terminal : (i : I) → is-equiv (m i)
  is-equiv-coalg-map-is-terminal i =
    is-equiv-is-invertible (f i) (htpy-eq (issec-f i)) (htpy-eq (isretr-f i))
    where

    f : (j : I) → ⦉ A ◁ B ◂ r ⦊ X j → X j
    f = pr1 (center (X-is-terminal (⦉ A ◁ B ◂ r ⦊ X , map-⦉ A ◁ B ◂ r ⦊ m)))

    is-hom-f : is-hom-⦉ A ◁ B ◂ r ⦊-Coalg (⦉ A ◁ B ◂ r ⦊ X , map-⦉ A ◁ B ◂ r ⦊ m) (X , m) f
    is-hom-f = pr2 (center (X-is-terminal (⦉ A ◁ B ◂ r ⦊ X , map-⦉ A ◁ B ◂ r ⦊ m)))

    isretr-f : (j : I) → (f j ∘ m j) == id
    isretr-f =
      htpy-eq
        (ap pr1
          (eq-is-contr'
            (X-is-terminal (X , m))
            ((λ j' → f j' ∘ m j') , (λ j' → (is-hom-f j') ·r (m j')))
            ((λ j' → id) , λ _ → refl-htpy)))

    issec-f : (j : I) → (m j ∘ f j) == id
    issec-f j =
      equational-reasoning
          m j ∘ f j
          ＝ map-⦉ A ◁ B ◂ r ⦊ f j ∘ map-⦉ A ◁ B ◂ r ⦊ m j by eq-htpy (is-hom-f j)
          ＝ map-⦉ A ◁ B ◂ r ⦊ (λ j' → f j' ∘ m j') j      by refl
          ＝ map-⦉ A ◁ B ◂ r ⦊ {X = X} (λ j' → id) j       by ap (λ h → map-⦉ A ◁ B ◂ r ⦊ h j) (eq-htpy isretr-f)
          ＝ id                                            by refl

  equiv-coalg-map-is-terminal : (i : I) → X i ≃ ⦉ A ◁ B ◂ r ⦊ X i
  pr1 (equiv-coalg-map-is-terminal i) = m i
  pr2 (equiv-coalg-map-is-terminal i) = is-equiv-coalg-map-is-terminal i

-- Any two terminal coalgebras are equivalent
module _ {li la lb l l'} {I : UU li}
  ((A ◁ B ◂ r) : IndexedContainer la lb I)
  ((X , m) : ⦉ A ◁ B ◂ r ⦊-Coalg l)
  ((X' , n) : ⦉ A ◁ B ◂ r ⦊-Coalg l')
  (X-is-terminal : ∀ {k} → is-terminal-⦉ A ◁ B ◂ r ⦊-Coalg (X , m) k)
  (X'-is-terminal : ∀ {k'} → is-terminal-⦉ A ◁ B ◂ r ⦊-Coalg (X' , n) k')
  where

  equiv-terminal-⦉_⦊-Coalg : (i : I) → X i ≃ X' i
  pr1 (equiv-terminal-⦉_⦊-Coalg i) = pr1 (pr1 (X'-is-terminal (X , m))) i
  pr2 (equiv-terminal-⦉_⦊-Coalg i) =
    is-equiv-is-invertible
      (pr1 (pr1 (X-is-terminal (X' , n))) i)
      (htpy-eq (htpy-eq (ap pr1
        (eq-is-contr'
          (X'-is-terminal (X' , n))
          ((λ i' →
            pr1 (pr1 (X'-is-terminal (X , m))) i' ∘
            pr1 (pr1 (X-is-terminal (X' , n))) i') ,
          (λ i' →
            pr2 (pr1 (X'-is-terminal (X , m))) i' ·r
              pr1 (pr1 (X-is-terminal (X' , n))) i' ∙h
            (map-⦉ A ◁ B ◂ r ⦊ (pr1 (pr1 (X'-is-terminal (X , m)))) i' ·l
              pr2 (pr1 (X-is-terminal (X' , n))) i')))
          ((λ _ → id) , (λ _ → refl-htpy)))) i))
      (htpy-eq (htpy-eq (ap pr1
        (eq-is-contr'
          (X-is-terminal (X , m))
          ((λ i' →
            pr1 (pr1 (X-is-terminal (X' , n))) i' ∘
            pr1 (pr1 (X'-is-terminal (X , m))) i') ,
          (λ i' →
            pr2 (pr1 (X-is-terminal (X' , n))) i' ·r
              pr1 (pr1 (X'-is-terminal (X , m))) i' ∙h
            (map-⦉ A ◁ B ◂ r ⦊ (pr1 (pr1 (X-is-terminal (X' , n)))) i' ·l
              pr2 (pr1 (X'-is-terminal (X , m))) i')))
          ((λ _ → id) , (λ _ → refl-htpy)))) i))


-- Equivalent type families are equivalently terminal coalgebras
module _ {li la lb l l'} {I : UU li}
  ((A ◁ B ◂ r) : IndexedContainer la lb I)
  ((X , m) : ⦉ A ◁ B ◂ r ⦊-Coalg l)
  {X' : I → UU l'}
  (e : (i : I) → X i ≃ X' i)
  where

  ⦉_⦊-Coalg-fam-equiv : ⦉ A ◁ B ◂ r ⦊-Coalg l'
  pr1 ⦉_⦊-Coalg-fam-equiv = X'
  pr2 ⦉_⦊-Coalg-fam-equiv i = tot (λ a → map-Π (λ b → map-equiv (e (r b)))) ∘ m i ∘ map-inv-equiv (e i)

  is-terminal-⦉_⦊-Coalg-equiv-type : ∀ {k}
                                   → is-terminal-⦉ A ◁ B ◂ r ⦊-Coalg (X , m) k
                                   → is-terminal-⦉ A ◁ B ◂ r ⦊-Coalg ⦉_⦊-Coalg-fam-equiv k
  is-terminal-⦉_⦊-Coalg-equiv-type {k = k} X-is-terminal (Y , h) =
    is-contr-equiv _
      (equiv-Σ _
        (equiv-Π-equiv-family (λ i →
          equiv-Π-equiv-family (λ y →
            inv-equiv (e i))))
        λ f →
          equiv-Π-equiv-family (λ i →
            equiv-Π-equiv-family (λ y →
              equiv-concat
                (eq-pair-eq-pr2
                  (eq-htpy
                    (λ b → inv (is-retraction-map-inv-equiv (e _ ) _))))
                _ ∘e
              equiv-ap
                (equiv-tot (λ a →
                  equiv-Π-equiv-family (λ b →
                    inv-equiv (e (r b)))))
                _ _)))
      (X-is-terminal (Y , h))

-- A homomorphism into a terminal coalgebra is an equivalence
-- if and only if the domain is also terminal
module _ {li la lb l l' k'} {I : UU li}
  ((A ◁ B ◂ r) : IndexedContainer la lb I)
  ((X , m) : ⦉ A ◁ B ◂ r ⦊-Coalg l)
  ((X' , m') : ⦉ A ◁ B ◂ r ⦊-Coalg l')
  ((f , α) : hom-⦉ A ◁ B ◂ r ⦊-Coalg (X , m) (X' , m'))
  where

  is-terminal-⦉_⦊-Coalg-is-fiberwise-equiv-hom : is-terminal-⦉ A ◁ B ◂ r ⦊-Coalg (X' , m') k'
                                               → is-fiberwise-equiv f
                                               → is-terminal-⦉ A ◁ B ◂ r ⦊-Coalg (X , m) k'
  is-terminal-⦉_⦊-Coalg-is-fiberwise-equiv-hom X'-is-terminal H (Y , n) =
    is-contr-equiv _
      (equiv-hom-iso-⦉ A ◁ B ◂ r ⦊-Coalg (X , m) (X' , m') ((λ i → (f i , H i)) , α) (Y , n))
      (X'-is-terminal (Y , n))

module _ {li la lb l l'} {I : UU li}
  ((A ◁ B ◂ r) : IndexedContainer la lb I)
  ((X , m) : ⦉ A ◁ B ◂ r ⦊-Coalg l)
  ((X' , m') : ⦉ A ◁ B ◂ r ⦊-Coalg l')
  ((f , α) : hom-⦉ A ◁ B ◂ r ⦊-Coalg (X , m) (X' , m'))
  where

  is-fiberwise-equiv-hom-is-terminal-⦉_⦊-Coalg : (∀ {k} → is-terminal-⦉ A ◁ B ◂ r ⦊-Coalg (X' , m') k)
                                               → (∀ {k} → is-terminal-⦉ A ◁ B ◂ r ⦊-Coalg (X , m) k)
                                               → is-fiberwise-equiv f
  is-fiberwise-equiv-hom-is-terminal-⦉_⦊-Coalg X'-is-terminal X-is-terminal i =
    is-equiv-is-invertible
      (pr1 (center (X-is-terminal (X' , m'))) i) -- X' i → X i
      (htpy-eq (htpy-eq (ap pr1 (eq-is-contr' (X'-is-terminal (X' , m'))
        (comp-hom-⦉ A ◁ B ◂ r ⦊-Coalg (X' , m') (X , m) (X' , m') (f , α) (center (X-is-terminal (X' , m'))))
        ((λ i → id) , (λ i → refl-htpy))))
        i))
      (htpy-eq (htpy-eq (ap pr1 (eq-is-contr' (X-is-terminal (X , m))
        (comp-hom-⦉ A ◁ B ◂ r ⦊-Coalg (X , m) (X' , m') (X , m) (center (X-is-terminal (X' , m'))) (f , α))
        ((λ i → id) , (λ i → refl-htpy))))
        i))

-- Equivalence of indexed containers
module _ {li la la' lb lb'} {I : UU li}
  ((A ◁ B ◂ r) : IndexedContainer la lb I)
  ((A' ◁ B' ◂ r') : IndexedContainer la' lb' I)
  (eA : (i : I) → A i ≃ A' i)
  (eB : (i : I) (a : A i) → B' (map-equiv (eA i) a) ≃ B a)
  (er : (i : I) (a : A i) (b : B' (map-equiv (eA i) a)) → r (map-equiv (eB i a) b) == r' b)
  where

  equiv-⦉_⦊-⦉_⦊ : ∀ {l} (X : I → UU l) (i : I)
                → ⦉ A ◁ B ◂ r ⦊ X i ≃ ⦉ A' ◁ B' ◂ r' ⦊ X i
  equiv-⦉_⦊-⦉_⦊ X i =
    equiv-Σ _
      (eA i)
      (λ a →
        equiv-Π-equiv-family (λ b → equiv-tr X (er i a b)) ∘e
        equiv-precomp-Π (eB i a) _)

  is-nat-equiv-⦉_⦊-⦉_⦊ : ∀ {l l'} (X : I → UU l) (Y : I → UU l')
                       → (f : (i : I) → X i → Y i)
                       → (i : I)
                       → map-equiv (equiv-⦉_⦊-⦉_⦊ Y i) ∘ map-⦉ A ◁ B ◂ r ⦊ f i
                       ~ map-⦉ A' ◁ B' ◂ r' ⦊ f i ∘ map-equiv (equiv-⦉_⦊-⦉_⦊ X i)
  is-nat-equiv-⦉_⦊-⦉_⦊ X Y f i (a , σ) =
    eq-pair-eq-pr2
      (eq-htpy
        (λ b → inv (preserves-tr f (er i a b) (σ (map-equiv (eB i a) b)))))

  equiv-⦉_⦊-⦉_⦊-Coalg : ∀ {l} → ⦉ A ◁ B ◂ r ⦊-Coalg l ≃ ⦉ A' ◁ B' ◂ r' ⦊-Coalg l
  equiv-⦉_⦊-⦉_⦊-Coalg =
    equiv-tot (λ X →
      equiv-Π-equiv-family (λ i →
        equiv-Π-equiv-family (λ x →
          equiv-⦉_⦊-⦉_⦊ X i)))

  equiv-hom-⦉_⦊-⦉_⦊-Coalg : ∀ {l l'}
                          → ((X , m) : ⦉ A ◁ B ◂ r ⦊-Coalg l)
                          → ((Y , n) : ⦉ A ◁ B ◂ r ⦊-Coalg l')
                          → hom-⦉ A ◁ B ◂ r ⦊-Coalg (X , m) (Y , n)
                          ≃ hom-⦉ A' ◁ B' ◂ r' ⦊-Coalg
                              (map-equiv equiv-⦉_⦊-⦉_⦊-Coalg (X , m))
                              (map-equiv equiv-⦉_⦊-⦉_⦊-Coalg (Y , n))
  equiv-hom-⦉_⦊-⦉_⦊-Coalg (X , m) (Y , n) =
    equiv-tot (λ f →
      equiv-Π-equiv-family (λ i →
        equiv-concat-htpy' _ (is-nat-equiv-⦉_⦊-⦉_⦊ X Y f i ·r m i) ∘e
        equiv-htpy-postcomp-emb (emb-equiv (equiv-⦉_⦊-⦉_⦊ Y i)) _ _))

  abstract --to speed up type checking
    equiv-is-terminal-⦉_⦊-⦉_⦊-Coalg : ∀ {l l'} ((X , m) : ⦉ A ◁ B ◂ r ⦊-Coalg l)
                                    → is-terminal-⦉ A ◁ B ◂ r ⦊-Coalg (X , m) l'
                                    ≃ is-terminal-⦉ A' ◁ B' ◂ r' ⦊-Coalg
                                        (map-equiv equiv-⦉_⦊-⦉_⦊-Coalg (X , m))
                                        l'
    equiv-is-terminal-⦉_⦊-⦉_⦊-Coalg (X , m) =
      equiv-Π _
        equiv-⦉_⦊-⦉_⦊-Coalg
        (λ (Y , n) →
          equiv-is-contr-equiv
            (equiv-hom-⦉_⦊-⦉_⦊-Coalg (Y , n) (X , m)))

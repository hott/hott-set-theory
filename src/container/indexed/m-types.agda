{-# OPTIONS --without-K #-}

open import foundation.action-on-identifications-functions
open import foundation.contractible-types
open import foundation.dependent-pair-types
open import foundation.equivalences
open import foundation.function-types
open import foundation.function-extensionality
open import foundation.homotopies
open import foundation.identity-types
open import foundation.universe-levels
open import foundation.whiskering-homotopies

open import container.indexed
open import container.indexed.coalgebra
open import notation

module container.indexed.m-types {li la lb} {I : UU li}
  ((A ◁ B ◂ r) : IndexedContainer la lb I) where

-- We postulate the existence of indexed M-types (see Ahrens et al.
-- "Non-wellfounded trees in Homotopy Type Theory")
postulate
  indexed-M-Coalg : ⦉ A ◁ B ◂ r ⦊-Coalg (la ⊔ lb)
  indexed-M-is-terminal : ∀ {l} → is-terminal-⦉ A ◁ B ◂ r ⦊-Coalg indexed-M-Coalg l

indexed-M : I → UU (la ⊔ lb)
indexed-M = pr1 indexed-M-Coalg

desup-indexed-M : (i : I) → indexed-M i → ⦉ A ◁ B ◂ r ⦊ indexed-M i
desup-indexed-M = pr2 indexed-M-Coalg

fixed-point-indexed-M : (i : I) → (indexed-M i) ≃ (⦉ A ◁ B ◂ r ⦊ indexed-M i)
fixed-point-indexed-M =
  equiv-coalg-map-is-terminal
    indexed-M-Coalg
    indexed-M-is-terminal


{-# OPTIONS --without-K #-}

open import foundation.action-on-identifications-functions
open import foundation.binary-relations
  renaming (total-space-Relation to tot-space)
open import foundation.cartesian-product-types
open import foundation.contractible-types
open import foundation.dependent-pair-types
  renaming (ev-pair to curry; ind-Σ to uncurry)
open import foundation.dependent-universal-property-equivalences
open import foundation.embeddings
open import foundation.equality-cartesian-product-types
open import foundation.equivalences
open import foundation.fibers-of-maps
open import foundation.function-types
open import foundation.function-extensionality
open import foundation.functoriality-cartesian-product-types
open import foundation.functoriality-dependent-function-types
open import foundation.functoriality-dependent-pair-types
open import foundation.homotopies
open import foundation.identity-types
open import foundation.postcomposition-functions
open import foundation.transport-along-identifications
open import foundation.type-arithmetic-dependent-pair-types
open import foundation.type-arithmetic-unit-type
open import foundation.unit-type
open import foundation.universe-levels
open import foundation.whiskering-homotopies

open import container
open import container.bisimulation
open import container.coalgebra
open import container.indexed
open import container.indexed.coalgebra
open import notation

module container.m-types {la lb}
  ((A ◁ B) : Container la lb) where

private
  C : IndexedContainer la lb unit
  C = (λ _ → A) ◁ B ◂ terminal-map

-- M-types are special cases of indexed M-types
import container.indexed.m-types C as C


equiv-unit-indexed-Coalg : ∀ {k} → ⦉ C ⦊-Coalg k ≃ ⟦ A ◁ B ⟧-Coalg k
equiv-unit-indexed-Coalg {k} =
  equiv-Σ
    (λ X → X → Σ A (λ a → B a → X))
    (left-unit-law-Π (λ _ → UU k))
    (λ X → left-unit-law-Π (λ t → X t → ⦉ C ⦊ X t))

abstract
  M : UU (la ⊔ lb)
  M = pr1 (map-equiv equiv-unit-indexed-Coalg C.indexed-M-Coalg)

  desup-M : M → ⟦ A ◁ B ⟧ M
  desup-M = pr2 (map-equiv equiv-unit-indexed-Coalg C.indexed-M-Coalg)

  is-equiv-desup-M : is-equiv desup-M
  is-equiv-desup-M = pr2 (C.fixed-point-indexed-M star)

fixed-point-M : M ≃ ⟦ A ◁ B ⟧ M
pr1 fixed-point-M = desup-M
pr2 fixed-point-M = is-equiv-desup-M

equiv-is-hom-unit-indexed-Coalg-is-hom-Coalg : ∀ {k k'}
  → ((X , m) : ⦉ C ⦊-Coalg k)
  → ((Y , m') : ⦉ C ⦊-Coalg k')
  → (f : (t : unit) → X t → Y t)
  → is-hom-⦉ C ⦊-Coalg (X , m) (Y , m') f
  ≃ is-hom-⟦ A ◁ B ⟧-Coalg
      (X star , m star)
      (Y star , m' star)
      (f star)
equiv-is-hom-unit-indexed-Coalg-is-hom-Coalg (X , m) (Y , m') f =
  left-unit-law-Π (λ t → (m' t ∘ f t) ~ (map-⦉ C ⦊ f t ∘ m t))

equiv-hom-unit-indexed-Coalg-hom-Coalg : ∀ {k k'}
  → (X : ⦉ C ⦊-Coalg k)
  → (Y : ⦉ C ⦊-Coalg k')
  → hom-⦉ C ⦊-Coalg X Y
  ≃ hom-⟦ A ◁ B ⟧-Coalg
      (map-equiv equiv-unit-indexed-Coalg X)
      (map-equiv equiv-unit-indexed-Coalg Y)
equiv-hom-unit-indexed-Coalg-hom-Coalg (X , m) (Y , m') =
  equiv-Σ
    (is-hom-⟦ A ◁ B ⟧-Coalg
      (map-equiv equiv-unit-indexed-Coalg (X , m))
      (map-equiv equiv-unit-indexed-Coalg (Y , m')))
    (left-unit-law-Π (λ i → X i → Y i))
    (equiv-is-hom-unit-indexed-Coalg-is-hom-Coalg (X , m) (Y , m'))

abstract
  M-is-terminal : ∀ {k} → is-terminal-⟦ A ◁ B ⟧-Coalg (M , desup-M) k
  M-is-terminal X =
    map-equiv
      (equiv-is-contr-equiv
        (equiv-tr
          (λ Z → hom-⟦ A ◁ B ⟧-Coalg Z (M , desup-M))
          (is-section-map-inv-equiv equiv-unit-indexed-Coalg X) ∘e
        equiv-hom-unit-indexed-Coalg-hom-Coalg
          (map-inv-equiv equiv-unit-indexed-Coalg X)
          (C.indexed-M-Coalg)))
      (C.indexed-M-is-terminal (map-inv-equiv equiv-unit-indexed-Coalg X))


-- The identity type on M is an indexed M-type
==-E-Coalg-M : ⦉ E (M , desup-M) ⦊-Coalg (la ⊔ lb)
==-E-Coalg-M = ==-E-Coalg (M , desup-M)

==-E-Coalg-is-terminal :
  ∀ {k} → is-terminal-⦉ E (M , desup-M) ⦊-Coalg ==-E-Coalg-M k
==-E-Coalg-is-terminal (R , m) =
  is-contr-equiv _
    (equiv-hom-E (R , m) ==-E-Coalg-M)
    (is-contr-Σ'
      (is-contr-equiv _
        (equiv-Σ
          (is-hom-⟦ A ◁ B ⟧-Coalg (tot-space (curry R) , h) (M , desup-M))
          (equiv-postcomp _ (equiv-pr1 is-torsorial-path ∘e associative-Σ _ _ _))
          (λ f →
            equiv-Π-equiv-family (λ ((x , y) , r) →
              equiv-ap
                (equiv-tot (λ a →
                  equiv-postcomp _ (equiv-pr1 is-torsorial-path ∘e associative-Σ _ _ _))) _ _)))
        (M-is-terminal (tot-space (curry R) , h)))
      (λ H →
        is-contr-prod
          (is-prop-is-contr (M-is-terminal _) _ _)
          (is-prop-is-contr (M-is-terminal _) _ _)))
  where
    h : tot-space (curry R) → ⟦ A ◁ B ⟧ (tot-space (curry R))
    h = pr1 (pr2 (map-equiv (equiv-E-coalgebra-bisimulation (M , desup-M)) (R , m)))

{-# OPTIONS --without-K --lossy-unification #-}

open import foundation.action-on-identifications-functions
open import foundation.binary-relations
  renaming (total-space-Relation to tot-space)
open import foundation.binary-transport
open import foundation.cartesian-product-types
open import foundation.contractible-types
open import foundation.dependent-pair-types
  renaming (ev-pair to curry; ind-Σ to uncurry)
open import foundation.equality-cartesian-product-types
open import foundation.equality-dependent-pair-types
open import foundation.equivalences
open import foundation.families-of-maps
open import foundation.fibers-of-maps
open import foundation.function-types
open import foundation.function-extensionality
open import foundation.functoriality-cartesian-product-types
open import foundation.functoriality-dependent-function-types
open import foundation.functoriality-dependent-pair-types
open import foundation.homotopies
open import foundation.homotopy-induction
open import foundation.identity-types
open import foundation.monomorphisms
open import foundation.postcomposition-functions
open import foundation.propositions
open import foundation.structure-identity-principle
open import foundation.transport-along-identifications
open import foundation.type-arithmetic-dependent-pair-types
open import foundation.type-duality
open import foundation.type-theoretic-principle-of-choice
open import foundation.universal-property-dependent-pair-types
  renaming (equiv-ind-Σ to equiv-uncurry)
open import foundation.universe-levels
open import foundation.whiskering-homotopies

open import container
open import container.coalgebra
open import container.indexed
open import container.indexed.coalgebra
open import notation

module container.bisimulation  where

module _ {la lb} {(A ◁ B) : Container la lb} where

  is-bisimulation : ∀ {k l} (X : ⟦ A ◁ B ⟧-Coalg k) (R : Relation l ¦ X ¦)
                  → UU (la ⊔ lb ⊔ k ⊔ l)
  is-bisimulation X R =
    Σ (tot-space R → ⟦ A ◁ B ⟧ (tot-space R))
      (λ f →
        is-hom-⟦ A ◁ B ⟧-Coalg (tot-space R , f) X (pr1 ∘ pr1) ×
        is-hom-⟦ A ◁ B ⟧-Coalg (tot-space R , f) X (pr2 ∘ pr1))

  Bisimulation : ∀ {k} l → ⟦ A ◁ B ⟧-Coalg k
               → UU (la ⊔ lb ⊔ k ⊔ lsuc l)
  Bisimulation l X = Σ (Relation l ¦ X ¦) (is-bisimulation X)

-- The identity type is a bisimulation on any ⟦ A ◁ B ⟧-coalgebra
module _ {la lb l} {(A ◁ B) : Container la lb}
  ((X , m) : ⟦ A ◁ B ⟧-Coalg l) where

  ==-↓ : tot-space (Id {A = X})
       → ⟦ A ◁ B ⟧ (tot-space (Id {A = X}))
  pr1 (==-↓ ((x , .x) , refl)) = pr1 (m x)
  pr2 (==-↓ ((x , .x) , refl)) b = ((pr2 (m x) b , pr2 (m x) b) , refl)

  is-bisimulation-== : is-bisimulation (X , m) (Id {A = X})
  pr1 is-bisimulation-== = ==-↓
  pr1 (pr2 is-bisimulation-==) ((x , .x) , refl) = refl
  pr2 (pr2 is-bisimulation-==) ((x , .x) , refl) = refl

  ==-Bisimulation : Bisimulation l (X , m)
  pr1 ==-Bisimulation = _==_
  pr2 ==-Bisimulation = is-bisimulation-==


module _ {la lb l} {(A ◁ B) : Container la lb}
  ((X , m) : ⟦ A ◁ B ⟧-Coalg l) where

  equiv-eq-span : (((Y , n) , ((f , H) , (g , K))) ((Y' , n') , ((f' , H') , (g' , K')))
                  : Σ (⟦ A ◁ B ⟧-Coalg l) λ (Y , n) →
                            hom-⟦ A ◁ B ⟧-Coalg (Y , n) (X , m) ×
                            hom-⟦ A ◁ B ⟧-Coalg (Y , n) (X , m))
                → Id {A = Σ (⟦ A ◁ B ⟧-Coalg l) λ (Y , n) →
                            hom-⟦ A ◁ B ⟧-Coalg (Y , n) (X , m) ×
                            hom-⟦ A ◁ B ⟧-Coalg (Y , n) (X , m)}
                     ((Y , n) , ((f , H) , (g , K)))
                     ((Y' , n') , ((f' , H') , (g' , K')))
                ≃ Σ (⟦ A ◁ B ⟧-Coalg-Eq (Y , n) (Y' , n')) λ (e , J) →
                    hom-⟦ A ◁ B ⟧-Coalg-Eq (f , H)
                      (comp-hom-⟦ A ◁ B ⟧-Coalg (Y , n) (Y' , n') (X , m) (f' , H') (map-equiv e , J)) ×
                    hom-⟦ A ◁ B ⟧-Coalg-Eq (g , K)
                      (comp-hom-⟦ A ◁ B ⟧-Coalg (Y , n) (Y' , n') (X , m) (g' , K') (map-equiv e , J))
  equiv-eq-span ((Y , n) , ((f , H) , (g , K))) =
    extensionality-Σ
      (λ {(Y' , n')} ((f' , H') , (g' , K')) (e , J) →
        hom-⟦ A ◁ B ⟧-Coalg-Eq (f , H)
          (comp-hom-⟦ A ◁ B ⟧-Coalg (Y , n) (Y' , n') (X , m) (f' , H') (map-equiv e , J)) ×
        hom-⟦ A ◁ B ⟧-Coalg-Eq (g , K)
          (comp-hom-⟦ A ◁ B ⟧-Coalg (Y , n) (Y' , n') (X , m) (g' , K') (map-equiv e , J)))
      (id-equiv , refl-htpy)
      ((refl-htpy , (λ x → ap (H x ∙_) (map-⟦ A ◁ B ⟧-~-refl (n x)))) ,
       (refl-htpy , (λ x → ap (K x ∙_) (map-⟦ A ◁ B ⟧-~-refl (n x)))))
      (λ (Y' , n') → equiv-⟦ A ◁ B ⟧-Coalg-Eq (Y , n) (Y' , n'))
      (λ ((f' , H') , (g' , K')) →
        equiv-prod
          (equiv-hom-⟦ A ◁ B ⟧-Coalg-Eq (f , H)
            (comp-hom-⟦ A ◁ B ⟧-Coalg (Y , n) (Y , n) (X , m) (f' , H') (id , refl-htpy)) ∘e
           equiv-concat' (f , H)
             (inv (comp-hom-⟦ A ◁ B ⟧-Coalg-id-hom-right (Y , n) (X , m) (f' , H'))))
          (equiv-hom-⟦ A ◁ B ⟧-Coalg-Eq (g , K)
            (comp-hom-⟦ A ◁ B ⟧-Coalg (Y , n) (Y , n) (X , m) (g' , K') (id , refl-htpy)) ∘e
          equiv-concat' (g , K)
            (inv (comp-hom-⟦ A ◁ B ⟧-Coalg-id-hom-right (Y , n) (X , m) (g' , K')))) ∘e
        inv-equiv (equiv-eq-pair _ _))

  eq-==-Bisimulation : Id {A = Σ (⟦ A ◁ B ⟧-Coalg l) λ (Y , n) →
                            hom-⟦ A ◁ B ⟧-Coalg (Y , n) (X , m) ×
                            hom-⟦ A ◁ B ⟧-Coalg (Y , n) (X , m)}
                          ((tot-space (Id {A = X}) , ==-↓ (X , m)) ,
                             ((pr1 ∘ pr1 , pr1 (pr2 (is-bisimulation-== (X , m)))) ,
                             (pr2 ∘ pr1 , pr2 (pr2 (is-bisimulation-== (X , m))))))
                          ((X , m) , ((id , refl-htpy) , (id , refl-htpy)))
  eq-==-Bisimulation =
    map-inv-equiv
      (equiv-eq-span
       ((tot-space (Id {A = X}) , ==-↓ (X , m)) ,
        (pr1 ∘ pr1 , pr1 (pr2 (is-bisimulation-== (X , m)))) ,
        pr2 ∘ pr1 , pr2 (pr2 (is-bisimulation-== (X , m))))
       ((X , m) , (id , refl-htpy) , id , refl-htpy))
      (((equiv-pr1 is-torsorial-path ∘e associative-Σ _ _ _) ,
        (λ { ((x , .x) , refl) → refl })) ,
        (refl-htpy ,
         λ { ((x , .x) , refl) → map-⟦ A ◁ B ⟧-~-refl (==-↓ (X , m) ((x , x) , refl)) }) ,
        ((λ ((x , x') , p) → inv p) ,
         λ { ((x , .x) , refl) →
           lemma1 (λ ((x , x') , p) → inv p) ∙
           ap (ap (pair (pr1 (m x)))) (eq-htpy-refl-htpy (pr2 (m x))) }))
    where
      lemma1 : ∀ {ℓ₁ ℓ₂} {X : UU ℓ₁} {Y : UU ℓ₂}
             → {f g : X → Y} (H : f ~ g)
             → {a : A} {φ : B a → X}
             → Id {A = Id {A = ⟦ A ◁ B ⟧ Y} (a , f ∘ φ) (a , g ∘ φ)}
                  (ap (λ h → (a , h ∘ φ)) (eq-htpy H))
                  (ap (pair a) (eq-htpy (H ·r φ)))
      lemma1 {Y = Y} {f = f} H {a} {φ} =
        ind-htpy f
          (λ g H →
            Id {A = Id {A = ⟦ A ◁ B ⟧ Y} (a , f ∘ φ) (a , g ∘ φ)}
                  (ap (λ h → (a , h ∘ φ)) (eq-htpy H))
                  (ap (pair a) (eq-htpy (H ·r φ))))
          (ap (ap (λ h → (a , h ∘ φ))) (eq-htpy-refl-htpy f) ∙
          ap (ap (pair a)) (inv (eq-htpy-refl-htpy (f ∘ φ))))
          H

-- Homomorphisms of bisimulations
module _ {la lb l k k'} {(A ◁ B) : Container la lb}
  ((X , m) : ⟦ A ◁ B ⟧-Coalg l)
  ((R , (α , (H , K))) : Bisimulation k (X , m))
  ((R' , (α' , (H' , K'))) : Bisimulation k' (X , m))
  where

  is-hom-Bisimulation : (tot-space R → tot-space R')
                      → UU (la ⊔ lb ⊔ l ⊔ k ⊔ k')
  is-hom-Bisimulation f =
    Σ (is-hom-⟦ A ◁ B ⟧-Coalg (tot-space R , α) (tot-space R' , α') f) (λ J →
      ((pr1 ∘ pr1 , H)
      == comp-hom-⟦ A ◁ B ⟧-Coalg
          (tot-space R , α)
          (tot-space R' , α')
          (X , m)
          (pr1 ∘ pr1 , H')
          (f , J)) ×
      (pr2 ∘ pr1 , K)
      == comp-hom-⟦ A ◁ B ⟧-Coalg
          (tot-space R , α)
          (tot-space R' , α')
          (X , m)
          (pr2 ∘ pr1 , K')
          (f , J))

  hom-Bisimulation : UU (la ⊔ lb ⊔ l ⊔ k ⊔ k')
  hom-Bisimulation =
    Σ (tot-space R → tot-space R') is-hom-Bisimulation


module _ {la lb k} {(A ◁ B) : Container la lb}
  ((X , m) : ⟦ A ◁ B ⟧-Coalg k) where

  equiv-bisim-pair-hom : Bisimulation k (X , m)
                       ≃ (Σ (⟦ A ◁ B ⟧-Coalg k) λ (Y , n) →
                            hom-⟦ A ◁ B ⟧-Coalg (Y , n) (X , m) ×
                            hom-⟦ A ◁ B ⟧-Coalg (Y , n) (X , m))
  equiv-bisim-pair-hom =
    equivalence-reasoning
      Bisimulation k (X , m)
      ≃ Σ (Σ (UU k) (λ Y → (Y → X) × (Y → X)))
          (λ (Y , (f , g)) →
            Σ (Y → ⟦ A ◁ B ⟧ Y) (λ n →
              is-hom-⟦ A ◁ B ⟧-Coalg (Y , n) (X , m) f ×
              is-hom-⟦ A ◁ B ⟧-Coalg (Y , n) (X , m) g)) by equiv-Σ-equiv-base _
                                                              (equiv-tot (λ Y → distributive-Π-Σ) ∘e
                                                              equiv-Pr1 k (X × X) ∘e
                                                              equiv-uncurry)
      ≃ Σ (⟦ A ◁ B ⟧-Coalg k) (λ (Y , n) →
          hom-⟦ A ◁ B ⟧-Coalg (Y , n) (X , m) ×
          hom-⟦ A ◁ B ⟧-Coalg (Y , n) (X , m))           by ((λ ((Y , (f , g)) , (n , (H , K))) →
                                                              ((Y , n) , ((f , H) , (g , K)))) ,
                                                            (is-equiv-is-invertible
                                                              (λ ((Y , n) , ((f , H) , (g , K))) →
                                                                ((Y , (f , g)) , (n , (H , K))))
                                                              refl-htpy
                                                              refl-htpy))

  equiv-hom-==-Bisimulation-eq-pair : ((R , (α , (H , K))) : Bisimulation k (X , m))
                                    → hom-Bisimulation (X , m) (R , (α , (H , K))) (==-Bisimulation (X , m))
                                    ≃ ((pr1 ∘ pr1 , H) == (pr2 ∘ pr1 , K))
  equiv-hom-==-Bisimulation-eq-pair (R , (α , (H , K))) =
    equivalence-reasoning
      hom-Bisimulation (X , m) (R , (α , (H , K))) (==-Bisimulation (X , m))
      ≃ Σ (tot-space R → X) (λ f →
          Σ (is-hom-⟦ A ◁ B ⟧-Coalg (tot-space R , α) (X , m) f) (λ J →
            (pr1 ∘ pr1 , H)
            == comp-hom-⟦ A ◁ B ⟧-Coalg (tot-space R , α) (X , m) (X , m)
                (id , refl-htpy) (f , J) ×
            (pr2 ∘ pr1 , K)
            == comp-hom-⟦ A ◁ B ⟧-Coalg (tot-space R , α) (X , m) (X , m)
                (id , refl-htpy) (f , J)))                                   by equiv-tr
                                                                                  (λ (((Y , n) , (f , H') , g , K')) →
                                                                                     Σ (tot-space R → Y)
                                                                                     (λ h →
                                                                                        Σ (is-hom-⟦ A ◁ B ⟧-Coalg (tot-space R , α) (Y , n) h)
                                                                                        (λ J →
                                                                                           (pr1 ∘ pr1 , H) ==
                                                                                           comp-hom-⟦ A ◁ B ⟧-Coalg (tot-space R , α) (Y , n) (X , m) (f , H')
                                                                                           (h , J)
                                                                                           ×
                                                                                           (pr2 ∘ pr1 , K) ==
                                                                                           comp-hom-⟦ A ◁ B ⟧-Coalg (tot-space R , α) (Y , n) (X , m) (g , K')
                                                                                           (h , J))))
                                                                                  (eq-==-Bisimulation (X , m))
      ≃ Σ (tot-space R → X) (λ f →
          Σ (is-hom-⟦ A ◁ B ⟧-Coalg (tot-space R , α) (X , m) f) (λ J →
            (pr1 ∘ pr1 , H) == (f , J) ×
            (pr2 ∘ pr1 , K) == (f , J)))                                     by equiv-tot (λ f →
                                                                                  equiv-tot (λ J →
                                                                                    equiv-prod
                                                                                      (equiv-concat' _
                                                                                        (eq-pair-eq-pr2
                                                                                          (eq-htpy (ap-id ∘ J))))
                                                                                      (equiv-concat' _
                                                                                        (eq-pair-eq-pr2
                                                                                          (eq-htpy (ap-id ∘ J))))))
      ≃ Σ (Σ (hom-⟦ A ◁ B ⟧-Coalg (tot-space R , α) (X , m)) (λ (f , J) →
             (pr2 ∘ pr1 , K) == (f , J)))
          (λ ((f , J) , p) → (pr1 ∘ pr1 , H) == (f , J))                     by ((λ (f , J , (p , q)) → ((((f , J) , q)) , p)) ,
                                                                                is-equiv-is-invertible
                                                                                  (λ ((((f , J) , q)) , p) → (f , J , (p , q)))
                                                                                  refl-htpy
                                                                                  refl-htpy)
      ≃ ((pr1 ∘ pr1 , H) == (pr2 ∘ pr1 , K))                                 by left-unit-law-Σ-is-contr
                                                                                  (is-torsorial-path (pr2 ∘ pr1 , K))
                                                                                  ((pr2 ∘ pr1 , K) , refl)


module _ {la lb k} {(A ◁ B) : Container la lb}
  ((X , m) : ⟦ A ◁ B ⟧-Coalg k) where

  simple-to-bisim-simple : (((Y , n) : ⟦ A ◁ B ⟧-Coalg k)
                           → is-prop (hom-⟦ A ◁ B ⟧-Coalg (Y , n) (X , m)))
                         → ((R , (α , (H , K))) : Bisimulation k (X , m))
                           → is-contr (hom-Bisimulation (X , m) (R , (α , (H , K))) (==-Bisimulation (X , m)))
  simple-to-bisim-simple T (R , (α , (H , K))) =
    is-contr-equiv _
      (equiv-hom-==-Bisimulation-eq-pair (X , m) (R , (α , (H , K))))
      (T (tot-space R , α) (pr1 ∘ pr1 , H) (pr2 ∘ pr1 , K))

  bisim-simple-to-simple : (((R , (α , (H , K))) : Bisimulation k (X , m))
                           → is-contr (hom-Bisimulation (X , m) (R , (α , (H , K))) (==-Bisimulation (X , m))))
                         → (((Y , n) : ⟦ A ◁ B ⟧-Coalg k)
                           → is-prop (hom-⟦ A ◁ B ⟧-Coalg (Y , n) (X , m)))
  bisim-simple-to-simple T (Y , n) (f , H) (g , K) =
    is-contr-equiv _
      (inv-equiv
        (equiv-hom-==-Bisimulation-eq-pair
          (X , m)
          (map-inv-equiv (equiv-bisim-pair-hom (X , m)) ((Y , n) , ((f , H) , (g , K))))) ∘e
      equiv-tr
        (λ ((s , t , u)) → t == u)
        (inv (is-section-map-inv-equiv
               (equiv-bisim-pair-hom (X , m))
               ((Y , n) , ((f , H) , (g , K))))))
      (T (map-inv-equiv (equiv-bisim-pair-hom (X , m)) ((Y , n) , ((f , H) , (g , K)))))
  

-- Utilities
tr-function : ∀ {i j k} {A : UU i} {B : A → UU j} {C : UU k}
            → {x y : A} (p : x == y) (f : B y → C)
            → tr (λ a → B a → C) (inv p) f == (f ∘ tr B p)
tr-function refl f = refl


{- Being a bisimulation on an ⟦ A ◁ B ⟧-coalgebra X
is equivalent to being a coalgebra
to the (X × X)-indexed polynomial functor
R (x , y)
  ↦ Σ (p : pr1 (m x) == pr1 (m y))
       Π (b : B (pr1 (m x))) (R (pr2 (m x) b , pr2 (m y) (tr B p b)))
-}
module _ {la lb l} {(A ◁ B) : Container la lb}
  ((X , m) : ⟦ A ◁ B ⟧-Coalg l) where

  E : IndexedContainer la lb (X × X)
  pr1 E (x , y) = pr1 (m x) == pr1 (m y)
  pr1 (pr2 E) {(x , y)} _ = B (pr1 (m x))
  pr2 (pr2 E) {(x , y)} {p} b =
    ((pr2 (m x) b) , (pr2 (m y) (tr B p b)))

  ==-E-Coalg : ⦉ E ⦊-Coalg l
  pr1 ==-E-Coalg = uncurry _==_
  pr2 ==-E-Coalg (x , .x) refl = (refl , refl-htpy)


module _ {la lb l lr} {(A ◁ B) : Container la lb}
  ((X , m) : ⟦ A ◁ B ⟧-Coalg l) (R : Relation lr X) where

  equiv-E : {(x , y) : X × X}
    → ⦉ E (X , m) ⦊ (uncurry R) (x , y)
    ≃ Σ (⟦ A ◁ B ⟧ (tot-space R)) (λ (a , φ) →
         m x == (a , pr1 ∘ pr1 ∘ φ) ×
         m y == (a , pr2 ∘ pr1 ∘ φ))
  equiv-E {x , y} =
    equivalence-reasoning

      ⦉ E (X , m) ⦊ (uncurry R) (x , y)

      ≃ Σ (pr1 (m x) == pr1 (m y)) (λ p →
          Σ (Σ (B (pr1 (m x)) → X) (λ φ₂ →
               (pr2 (m y) ∘ tr B p) == φ₂))
            λ (φ₂ , _) → (b : B (pr1 (m x)))
            → R (pr2 (m x) b) (φ₂ b))                         by equiv-tot (λ p →
                                                                   inv-left-unit-law-Σ-is-contr
                                                                     (is-torsorial-path (pr2 (m y) ∘ tr B p))
                                                                     (pr2 (m y) ∘ tr B p , refl))
      ≃ Σ (Σ (B (pr1 (m x)) → X)
             (λ φ₂ → (b : B (pr1 (m x)))
            → R (pr2 (m x) b) (φ₂ b)))
          (λ (φ₂ , _) →
            Σ (pr1 (m x) == pr1 (m y))
              λ p → (pr2 (m y) ∘ tr B p) == φ₂)               by (λ (p , ((φ₂ , q) , σ)) → ((φ₂ , σ) , (p , q))) ,
                                                                 is-equiv-is-invertible
                                                                   (λ ((φ₂ , σ) , (p , q)) → (p , ((φ₂ , q) , σ)))
                                                                   refl-htpy
                                                                   refl-htpy
      ≃ Σ (Σ (B (pr1 (m x)) → X)
             (λ φ₂ → (b : B (pr1 (m x)))
            → R (pr2 (m x) b) (φ₂ b)))
          (λ (φ₂ , _) →
            m y == (pr1 (m x) , φ₂))                          by equiv-tot (λ (φ₂ , _) →
                                                                   equiv-eq-pair-Σ _ _ ∘e
                                                                   equiv-Σ _
                                                                     (equiv-inv _ _)
                                                                     (λ p → equiv-concat
                                                                       (tr-function p (pr2 (m y))) _))
      ≃ Σ (Σ (⟦ A ◁ B ⟧ X)
             (λ (a , φ₁) → m x == (a , φ₁)))
          (λ ((a , φ₁) , p) →
            Σ (Σ (B a → X)
               (λ φ₂ → (b : B a)
              → R (φ₁ b) (φ₂ b)))
            (λ (φ₂ , _) →
              m y == (a , φ₂)))                               by inv-left-unit-law-Σ-is-contr
                                                                   (is-torsorial-path (m x))
                                                                   (m x , refl)
      ≃ Σ (Σ A (λ a →
             Σ ((B a → X) × (B a → X))
               λ (φ₁ , φ₂) →
                 (b : B a) → R (φ₁ b) (φ₂ b)))
          (λ (a , ((φ₁ , φ₂) , σ)) →
            m x == (a , φ₁) ×
            m y == (a , φ₂))                                  by (λ (((a , φ₁) , p) , ((φ₂ , σ) , q)) →
                                                                    ((a , ((φ₁ , φ₂) , σ)) , (p , q))) ,
                                                                 is-equiv-is-invertible
                                                                   (λ ((a , ((φ₁ , φ₂) , σ)) , (p , q)) →
                                                                     (((a , φ₁) , p) , ((φ₂ , σ) , q)))
                                                                   refl-htpy
                                                                   refl-htpy
      ≃ Σ (⟦ A ◁ B ⟧ (tot-space R)) (λ (a , φ) →
         m x == (a , pr1 ∘ pr1 ∘ φ) ×
         m y == (a , pr2 ∘ pr1 ∘ φ))                          by equiv-Σ-equiv-base _
                                                                   (equiv-tot (λ a →
                                                                     inv-distributive-Π-Σ ∘e
                                                                     equiv-Σ-equiv-base _
                                                                       inv-distributive-Π-Σ))


  compute-map-equiv-E : {(x , y) : X × X}
    → ((p , σ) : ⦉ E (X , m) ⦊ (uncurry R) (x , y))
    → map-equiv equiv-E (p , σ)
    == ((pr1 (m x) , λ b → (pr2 (m x) b , (pr2 (m y) (tr B p b))) , σ b) ,
       refl ,
       (eq-pair-Σ (inv p) (tr-function p (pr2 (m y)) ∙ refl)))
  compute-map-equiv-E {(x , y)} (p , σ) = refl

  equiv'-E : {(x , y) : X × X}
    → ⦉ E (X , m) ⦊ (uncurry R) (x , y)
    ≃ Σ (⟦ A ◁ B ⟧ (tot-space R)) (λ (a , φ) →
         m x == (a , pr1 ∘ pr1 ∘ φ) ×
         m y == (a , pr2 ∘ pr1 ∘ φ))
  pr1 (equiv'-E {x , y}) (p , σ) =
    ((pr1 (m x) , λ b → (pr2 (m x) b , (pr2 (m y) (tr B p b))) , σ b) ,
     refl ,
     (eq-pair-Σ (inv p) (tr-function p (pr2 (m y)))))
  pr2 (equiv'-E {x , y}) =
    is-equiv-htpy
      (map-equiv equiv-E)
      (λ (p , σ) → eq-pair-Σ refl (eq-pair refl (ap (eq-pair-Σ (inv p)) (inv right-unit))))
      (is-equiv-map-equiv equiv-E)

  equiv-is-E-coalgebra-is-bisimulation :
    (((x , y) : X × X) → R x y → ⦉ E (X , m) ⦊ (uncurry R) (x , y))
    ≃ is-bisimulation (X , m) R
  equiv-is-E-coalgebra-is-bisimulation =
    equiv-tot (λ f →
      distributive-Π-Σ) ∘e
    distributive-Π-Σ ∘e
    equiv-uncurry ∘e
    equiv-Π-equiv-family (λ (x , y) →
      equiv-Π-equiv-family (λ r →
        equiv'-E))

module _ {la lb l} {(A ◁ B) : Container la lb}
  ((X , m) : ⟦ A ◁ B ⟧-Coalg l) where

  equiv-E-coalgebra-bisimulation : ∀ {l'}
    → ⦉ E (X , m) ⦊-Coalg l' ≃ Bisimulation l' (X , m)
  equiv-E-coalgebra-bisimulation =
    equiv-Σ _
      equiv-ev-pair
      (λ R → equiv-is-E-coalgebra-is-bisimulation (X , m) (curry R))


-- Coalgebra homomorphisms
module _ {la lb l lr lr'} {(A ◁ B) : Container la lb}
  {(X , m) : ⟦ A ◁ B ⟧-Coalg l}
  ((R , k) : ⦉ E (X , m) ⦊-Coalg lr)
  ((R' , k') : ⦉ E (X , m) ⦊-Coalg lr')
  where

  private
    h : tot-space (curry R) → ⟦ A ◁ B ⟧ (tot-space (curry R))
    h = pr1 (pr2 (map-equiv (equiv-E-coalgebra-bisimulation (X , m)) (R , k)))

    α : m ∘ pr2 ∘ pr1 ~ map-⟦ A ◁ B ⟧ (pr2 ∘ pr1) ∘ h
    α = pr2 (pr2 (pr2 (map-equiv (equiv-E-coalgebra-bisimulation (X , m)) (R , k))))

    h' : tot-space (curry R') → ⟦ A ◁ B ⟧ (tot-space (curry R'))
    h' = pr1 (pr2 (map-equiv (equiv-E-coalgebra-bisimulation (X , m)) (R' , k')))

    α' : m ∘ pr2 ∘ pr1 ~ map-⟦ A ◁ B ⟧ (pr2 ∘ pr1) ∘ h'
    α' = pr2 (pr2 (pr2 (map-equiv (equiv-E-coalgebra-bisimulation (X , m)) (R' , k'))))


  equiv-hom-E :
    hom-⦉ E (X , m) ⦊-Coalg (R , k) (R' , k')
    ≃ Σ (hom-⟦ A ◁ B ⟧-Coalg (tot-space (curry R) , h) (tot-space (curry R') , h'))
        (λ (f , H) →
          Id {A = hom-⟦ A ◁ B ⟧-Coalg (tot-space (curry R) , h) (X , m)}
             (pr1 ∘ pr1 , refl-htpy)
             (pr1 ∘ pr1 ∘ f , map-⟦ A ◁ B ⟧ (pr1 ∘ pr1) ·l H) ×
          Id {A = hom-⟦ A ◁ B ⟧-Coalg (tot-space (curry R) , h) (X , m)}
             (pr2 ∘ pr1 , α)
             (pr2 ∘ pr1 ∘ f , (α' ·r f) ∙h (map-⟦ A ◁ B ⟧ (pr2 ∘ pr1) ·l H)))
  equiv-hom-E =
    equivalence-reasoning

      hom-⦉ E (X , m) ⦊-Coalg (R , k) (R' , k')

      ≃ Σ (((x , y) : X × X) → R (x , y) → R' (x , y)) (λ f →
          ((x , y) : X × X) (r : R (x , y)) →
            map-equiv
              (equiv'-E (X , m) (curry R'))
              (k' (x , y) (f (x , y) r))
            == map-equiv
                 (equiv'-E (X , m) (curry R'))
                 (map-⦉ E (X , m) ⦊ f (x , y) (k (x , y) r)))       by equiv-tot (λ f →
                                                                                  equiv-Π-equiv-family (λ (x , y) →
                                                                                    equiv-Π-equiv-family (λ r →
                                                                                      equiv-ap (equiv'-E (X , m) (curry R')) _ _)))
      ≃ Σ (((x , y) : X × X) → R (x , y) → R' (x , y)) (λ f →
          ((x , y) : X × X) (r : R (x , y)) →
            Σ ((h' (tot f ((x , y) , r)))
              == (map-⟦ A ◁ B ⟧ (tot f) (h ((x , y) , r))))
              (λ p →
                ap (map-⟦ A ◁ B ⟧ (pr1 ∘ pr1)) p
                == refl ×
                (α' (tot f ((x , y) , r))
                ∙ ap (map-⟦ A ◁ B ⟧ (pr2 ∘ pr1)) p)
                == α ((x , y) , r)))                                         by equiv-tot (λ f →
                                                                                  equiv-Π-equiv-family (λ (x , y) →
                                                                                    equiv-Π-equiv-family (λ r →
                                                                                      equiv-tot (λ p →
                                                                                        equiv-prod
                                                                                          (equiv-concat
                                                                                            (tr-lem1 (m x) p _) _)
                                                                                          (equiv-concat
                                                                                            (tr-lem2 (m y) p _) _) ∘e
                                                                                        equiv-pair-eq _ _ ∘e
                                                                                        equiv-concat
                                                                                          (inv (tr-prod
                                                                                            (λ (a , φ) → m x == (a , pr1 ∘ pr1 ∘ φ))
                                                                                            (λ (a , φ) → m y == (a , pr2 ∘ pr1 ∘ φ))
                                                                                            p
                                                                                            _)) _) ∘e
                                                                                      equiv-pair-eq-Σ _ _)))
      ≃ Σ (((x , y) : X × X) → R (x , y) → R' (x , y)) (λ f →
          Σ ((h' ∘ tot f) ~ map-⟦ A ◁ B ⟧ (tot f) ∘ h) λ H →
            ((map-⟦ A ◁ B ⟧ (pr1 ∘ pr1) ·l H) ~ refl-htpy) ×
            (((α' ·r (tot f)) ∙h (map-⟦ A ◁ B ⟧ (pr2 ∘ pr1) ·l H)) ~ α))     by equiv-tot (λ f →
                                                                                  equiv-tot (λ H →
                                                                                    distributive-Π-Σ) ∘e
                                                                                  distributive-Π-Σ ∘e
                                                                                  equiv-uncurry)
      ≃ Σ (((x , y) : X × X) → R (x , y) → R' (x , y)) (λ f →
          Σ ((h' ∘ tot f) ~ map-⟦ A ◁ B ⟧ (tot f) ∘ h) λ H →
            refl-htpy == (map-⟦ A ◁ B ⟧ (pr1 ∘ pr1) ·l H) ×
            α == ((α' ·r (tot f)) ∙h (map-⟦ A ◁ B ⟧ (pr2 ∘ pr1) ·l H)))      by equiv-tot (λ f →
                                                                                  equiv-tot (λ H →
                                                                                    equiv-prod
                                                                                      (equiv-inv _ _ ∘e
                                                                                      equiv-eq-htpy)
                                                                                      (equiv-inv _ _ ∘e
                                                                                      equiv-eq-htpy)))
      ≃ Σ (((x , y) : X × X) → R (x , y) → R' (x , y)) (λ f →
          Σ ((h' ∘ tot f) ~ map-⟦ A ◁ B ⟧ (tot f) ∘ h) λ H →
            tr (is-hom-⟦ A ◁ B ⟧-Coalg (tot-space (curry R) , h) (X , m))
               refl
               refl-htpy
            == (map-⟦ A ◁ B ⟧ (pr1 ∘ pr1) ·l H) ×
            tr (is-hom-⟦ A ◁ B ⟧-Coalg (tot-space (curry R) , h) (X , m))
               refl
               α
            == ((α' ·r (tot f)) ∙h (map-⟦ A ◁ B ⟧ (pr2 ∘ pr1) ·l H)))        by id-equiv

      ≃ Σ (Σ (tot-space (curry R) → tot-space (curry R')) (λ f →
             ((pr1 ∘ pr1) == (pr1 ∘ pr1 ∘ f)) ×
             ((pr2 ∘ pr1) == (pr2 ∘ pr1 ∘ f))))
          (λ (f , (p , q)) →
            Σ (h' ∘ f ~ map-⟦ A ◁ B ⟧ f ∘ h) (λ H →
              tr (is-hom-⟦ A ◁ B ⟧-Coalg (tot-space (curry R) , h) (X , m))
                 p
                 refl-htpy
              == ((map-⟦ A ◁ B ⟧ (pr1 ∘ pr1) ·l H)) ×
              tr (is-hom-⟦ A ◁ B ⟧-Coalg (tot-space (curry R) , h) (X , m))
                 q
                 α
              == (((α' ·r f) ∙h (map-⟦ A ◁ B ⟧ (pr2 ∘ pr1) ·l H)))))         by equiv-Σ _
                                                                                  (equiv-tot (λ f →
                                                                                    equiv-prod
                                                                                      (inv-equiv equiv-funext)
                                                                                      (inv-equiv equiv-funext) ∘e
                                                                                    distributive-Π-Σ ∘e
                                                                                    equiv-Π-equiv-family (λ ((x , y) , r) →
                                                                                      equiv-pair-eq _ _)) ∘e
                                                                                  equiv-fam-map-map-tot-space _ _)
                                                                                  (λ f →
                                                                                    equiv-tot (λ H →
                                                                                      equiv-prod
                                                                                        (equiv-concat
                                                                                          (ap (λ p →
                                                                                            tr (is-hom-⟦ A ◁ B ⟧-Coalg (tot-space (curry R) , h) (X , m))
                                                                                                p
                                                                                                refl-htpy)
                                                                                          (eq-htpy-refl-htpy (pr1 ∘ pr1))) _)
                                                                                        (equiv-concat
                                                                                          (ap (λ p →
                                                                                            tr (is-hom-⟦ A ◁ B ⟧-Coalg (tot-space (curry R) , h) (X , m))
                                                                                                p
                                                                                                α)
                                                                                          (eq-htpy-refl-htpy (pr2 ∘ pr1))) _)))
      ≃ Σ (hom-⟦ A ◁ B ⟧-Coalg (tot-space (curry R) , h) (tot-space (curry R') , h'))
          (λ (f , H) →
            Σ ((pr1 ∘ pr1) == (pr1 ∘ pr1 ∘ f)) (λ p →
              tr (is-hom-⟦ A ◁ B ⟧-Coalg (tot-space (curry R) , h) (X , m))
                 p
                 refl-htpy
              == ((map-⟦ A ◁ B ⟧ (pr1 ∘ pr1) ·l H))) ×
            Σ ((pr2 ∘ pr1) == (pr2 ∘ pr1 ∘ f)) (λ q →
              tr (is-hom-⟦ A ◁ B ⟧-Coalg (tot-space (curry R) , h) (X , m))
                 q
                 α
              == (((α' ·r f) ∙h (map-⟦ A ◁ B ⟧ (pr2 ∘ pr1) ·l H)))))         by ((λ ((f , (p , q)) , (H , (p' , q'))) →
                                                                                  ((f , H) , ((p , p') , (q , q')))) ,
                                                                                is-equiv-is-invertible
                                                                                  (λ ((f , H) , ((p , p') , (q , q'))) →
                                                                                    ((f , (p , q)) , (H , (p' , q'))))
                                                                                  refl-htpy
                                                                                  refl-htpy)
      ≃ Σ (hom-⟦ A ◁ B ⟧-Coalg (tot-space (curry R) , h) (tot-space (curry R') , h'))
          (λ (f , H) →
            Id {A = hom-⟦ A ◁ B ⟧-Coalg (tot-space (curry R) , h) (X , m)}
               (pr1 ∘ pr1 , refl-htpy)
               (pr1 ∘ pr1 ∘ f , map-⟦ A ◁ B ⟧ (pr1 ∘ pr1) ·l H) ×
            Id {A = hom-⟦ A ◁ B ⟧-Coalg (tot-space (curry R) , h) (X , m)}
               (pr2 ∘ pr1 , α)
               (pr2 ∘ pr1 ∘ f , (α' ·r f) ∙h (map-⟦ A ◁ B ⟧ (pr2 ∘ pr1) ·l H))) by equiv-tot (λ (f , H) →
                                                                                     equiv-prod
                                                                                       (equiv-eq-pair-Σ _ _)
                                                                                       (equiv-eq-pair-Σ _ _))
      where
        tr-lem1 : (s : ⟦ A ◁ B ⟧ X)
                → {u v : ⟦ A ◁ B ⟧ (tot-space (curry R'))}
                → (p : u == v)
                → (q : s == (pr1 u , pr1 ∘ pr1 ∘ pr2 u))
                → (q ∙ ap (λ (a , φ) → (a , pr1 ∘ pr1 ∘ φ)) p)
                == tr (λ (a , φ) → s == (a , pr1 ∘ pr1 ∘ φ)) p q
        tr-lem1 s refl refl = refl

        tr-lem2 : (s : ⟦ A ◁ B ⟧ X)
                → {u v : ⟦ A ◁ B ⟧ (tot-space (curry R'))}
                → (p : u == v)
                → (q : s == (pr1 u , pr2 ∘ pr1 ∘ pr2 u))
                → (q ∙ ap (λ (a , φ) → (a , pr2 ∘ pr1 ∘ φ)) p)
                == tr (λ (a , φ) → s == (a , pr2 ∘ pr1 ∘ φ)) p q
        tr-lem2 s refl refl = refl


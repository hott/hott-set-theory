{-# OPTIONS --without-K --lossy-unification #-}

open import elementary-number-theory.addition-natural-numbers
open import elementary-number-theory.natural-numbers

open import foundation.action-on-identifications-functions
open import foundation.cartesian-product-types
open import foundation.contractible-types
open import foundation.dependent-pair-types
open import foundation.embeddings
open import foundation.equality-dependent-pair-types
open import foundation.equivalences
open import foundation.equivalence-extensionality
open import foundation.fibers-of-maps
open import foundation.function-extensionality
open import foundation.function-types
open import foundation.functoriality-dependent-function-types
open import foundation.functoriality-dependent-pair-types
open import foundation.functoriality-function-types
open import foundation.homotopies
open import foundation.identity-types
open import foundation.locally-small-types
open import foundation.postcomposition-functions
open import foundation.propositional-maps
open import foundation.propositions
open import foundation.sets
open import foundation.transport-along-identifications
open import foundation.truncated-maps
open import foundation.truncated-types
open import foundation.truncation-levels
  renaming (truncation-level-minus-one-ℕ to minus-one;
    truncation-level-ℕ to to-𝕋)
open import foundation.type-arithmetic-dependent-pair-types
open import foundation.type-theoretic-principle-of-choice
open import foundation.univalence
open import foundation.universal-property-equivalences
open import foundation.universe-levels

open import functor.n-slice
open import image-factorisation
open import notation hiding (sup)

module fixed-point.exponentiation {i j} (n : ℕ)
  (V : UU j) (fixed-point-V : V ≃ P-[ n ] i V)
  (ordered-pairs : (V × V) ↪ V) where

-- Construction of ∈-structure on V
open import fixed-point.core n V fixed-point-V

-- Property of having exponentiation
open import e-structure.property.exponentiation ∈-structure-V ordered-pairs

-- Notation for ordered pairs
import e-structure.core
open e-structure.core.ordered-pairing-structure.notation V ordered-pairs

module _ {A : UU i} {B : A → UU i}
  (f : trunc-map (minus-one n) A V)
  (g : ∀ a → trunc-map (minus-one n) (B a) V)
  where

  trunc-map-graph : (∀ a → B a)
                  → trunc-map (minus-one n) A V
  trunc-map-graph ϕ =
    comp-trunc-map _
      ((λ (x , y) → ⟨ x , y ⟩) , is-trunc-map-is-prop-map _ (is-prop-map-is-emb (pr2 ordered-pairs)))
      (comp-trunc-map _
        (map-Σ _ (pr1 f) (λ a → pr1 (g a)) , is-trunc-map-map-Σ _ _ (pr2 f) (λ a → pr2 (g a)))
        graph-on-types)
    where
      equiv-fiber-graph-on-types : ((a , b) : Σ A B)
                               → Σ A (λ a' → (a' , ϕ a') == (a , b))
                               ≃ (ϕ a == b)
      equiv-fiber-graph-on-types (a , b) =
        equivalence-reasoning
          Σ A (λ a' → (a' , ϕ a') == (a , b))
            ≃ Σ A (λ a' → Σ (a' == a) (λ α → tr B α (ϕ a') == b))        by equiv-tot (λ a' → inv-equiv (equiv-eq-pair-Σ _ _))
            ≃ Σ (Σ A (λ a' → a' == a)) (λ (a' , α) → tr B α (ϕ a') == b) by inv-associative-Σ _ _ _
            ≃ (ϕ a == b)                                                 by left-unit-law-Σ-is-contr
                                                                              (is-torsorial-path' a) (a , refl)

      graph-on-types : trunc-map (minus-one n) A (Σ A B)
      pr1 graph-on-types a = (a , ϕ a)
      pr2 graph-on-types (a , b) =
        is-trunc-equiv _ _
          (equiv-fiber-graph-on-types (a , b))
          (is-trunc-is-trunc-map-into-is-trunc
            (minus-one n)
            (pr1 (g a))
            is-trunc-V
            (pr2 (g a))
            (ϕ a)
            b)

  graph : (∀ a → B a) → V
  graph ϕ = sup (A , trunc-map-graph ϕ)

  is-trunc-map-graph : is-trunc-map (minus-one n) graph
  is-trunc-map-graph =
    is-trunc-map-is-trunc-map-ap _ graph (λ ϕ ψ →
      is-trunc-map-htpy _
        {g = ap sup ∘
             (map-inv-equiv (equiv-eq-P-[ n ] _ _) ∘
             (tot (λ e → ap (pr1 ordered-pairs ∘ map-Σ _ (pr1 f) (pr1 ∘ g)) ∘_) ∘
             map-equiv (e ϕ ψ)))}
        (λ {refl → ap (ap sup) (inv (is-retraction-map-inv-equiv (equiv-eq-P-[ n ] _ _) refl))})
        (is-trunc-map-comp _
          (ap sup)
          _
          (is-trunc-map-is-equiv _
            (is-equiv-map-equiv (equiv-ap (inv-equiv fixed-point-V) _ _)))
          (is-trunc-map-comp _
            (map-inv-equiv (equiv-eq-P-[ n ] _ _))
            _
            (is-trunc-map-is-equiv _ (is-equiv-map-inv-equiv (equiv-eq-P-[ n ] _ _)))
            (is-trunc-map-comp _
              (tot (λ e → ap (pr1 ordered-pairs ∘ map-Σ _ (pr1 f) (pr1 ∘ g)) ∘_))
              (map-equiv (e ϕ ψ))
              (is-trunc-map-tot _ (λ e →
                is-trunc-map-map-Π _
                  (λ a → ap (pr1 ordered-pairs ∘ map-Σ _ (pr1 f) (pr1 ∘ g)))
                  (λ a → is-trunc-map-ap-is-trunc-map _
                           (pr1 ordered-pairs ∘ map-Σ _ (pr1 f) (pr1 ∘ g))
                           (is-trunc-map-comp _
                             (pr1 ordered-pairs)
                             (map-Σ _ (pr1 f) (pr1 ∘ g))
                             (is-trunc-map-is-prop-map _
                               (is-prop-map-is-emb (pr2 ordered-pairs)))
                             (is-trunc-map-map-Σ _ _
                               (pr2 f)
                               (pr2 ∘ g)))
                           (a , ϕ a)
                           (map-equiv e a , ψ (map-equiv e a)))))
              (is-trunc-map-is-equiv _ (is-equiv-map-equiv (e ϕ ψ)))))))
    where
      e : (ϕ ψ : ∀ a → B a)
        → (ϕ == ψ)
        ≃ Σ (A ≃ A) (λ e →
            (a : A) → Id {A = Σ A B} (a , ϕ a) (map-equiv e a , ψ (map-equiv e a)))
      e ϕ ψ =
        equivalence-reasoning
          (ϕ == ψ)

          ≃ (ϕ ~ ψ)                                    by equiv-funext

          ≃ Σ (Σ (A ≃ A) (λ e → id ~ map-equiv e))
              (λ (e , r) → ∀ a → tr B (r a) (ϕ a)
                                 == ψ (map-equiv e a)) by inv-left-unit-law-Σ-is-contr
                                                            (is-torsorial-htpy-equiv id-equiv)
                                                            (id-equiv , refl-htpy)
          ≃ Σ (A ≃ A) (λ e → Σ (id ~ map-equiv e)
              λ r → ∀ a → tr B (r a) (ϕ a)
                          == ψ (map-equiv e a))        by associative-Σ _ _ _

          ≃ Σ (A ≃ A) (λ e → (a : A) →
            Σ (a == map-equiv e a) λ α →
              tr B α (ϕ a)
              == ψ (map-equiv e a))                    by equiv-tot (λ e → inv-distributive-Π-Σ)

          ≃ Σ (A ≃ A) (λ e → (a : A) →
              (a , ϕ a) ==
              (map-equiv e a , ψ (map-equiv e a)))     by equiv-tot (λ e → equiv-Π-equiv-family (λ a →
                                                            equiv-eq-pair-Σ (a , ϕ a) (map-equiv e a , ψ (map-equiv e a))))

infix 70 _⇒_
_⇒_ : V → V → V
x ⇒ y = sup ((A → B) , graph f (λ _ → g) , is-trunc-map-graph f (λ _ → g))
  where
    A = pr1 (desup x)
    B = pr1 (desup y)
    f = pr2 (desup x)
    g = pr2 (desup y)


∈-⇒ : (a b z : V)
    → (z ∈ a ⇒ b) ≃ Operation' a b z
∈-⇒ a b z =
  equivalence-reasoning
    (z ∈ a ⇒ b)

      ≃ Σ (A → B) (λ ϕ → graph f (λ _ → g) ϕ == z)     by equiv-eq (∈-sup z)

      ≃ Σ (A → B) (λ ϕ → z == graph f (λ _ → g) ϕ)     by equiv-tot (λ ϕ → equiv-inv (graph f (λ _ → g) ϕ) z)

      ≃ Σ (A → B) (λ ϕ → (u : V) →
          (u ∈ z) ≃ (u ∈ graph f (λ _ → g) ϕ))         by equiv-tot (λ ϕ → equiv-extensionality)

      ≃ Σ (A → B) (λ ϕ → (u : V) →
          (u ∈ z) ≃
          Σ A (λ x → ⟨ pr1 f x , pr1 g (ϕ x) ⟩ == u))  by equiv-tot (λ ϕ → equiv-Π-equiv-family (λ u →
                                                            equiv-postcomp-equiv (equiv-eq (∈-sup u)) (u ∈ z)))
      ≃ Σ (El a → El b) (λ ϕ → (u : V) →
          (u ∈ z) ≃
          Σ (El a) (λ x → ⟨ pr1 x , pr1 (ϕ x) ⟩ == u)) by equiv-Σ _
                                                            (equiv-postcomp (El a) (inv-equiv-total-fiber (pr1 g)) ∘e
                                                             equiv-precomp (equiv-total-fiber (pr1 f)) B)
                                                            (λ ϕ → equiv-Π-equiv-family (λ u →
                                                              equiv-postcomp-equiv
                                                                (equiv-Σ-equiv-base
                                                                  (λ x → ⟨ pr1 x , pr1 g (ϕ (pr1 (pr2 x))) ⟩ == u)
                                                                  (inv-equiv-total-fiber (pr1 f)))
                                                                (u ∈ z)))
      ≃ Operation' a b z                               by id-equiv

  where
    A = pr1 (desup a)
    B = pr1 (desup b)
    C = pr1 (desup z)
    f = pr2 (desup a)
    g = pr2 (desup b)
    h = pr2 (desup z)

exponentiation' : Exponentiation'
exponentiation' a b = (a ⇒ b , ∈-⇒ a b)

exponentiation : Exponentiation
exponentiation =
  map-inv-equiv Exponentiation≃Exponentiation' exponentiation'


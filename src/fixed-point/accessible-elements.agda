{-# OPTIONS --without-K #-}

open import elementary-number-theory.natural-numbers

open import foundation.action-on-identifications-functions
open import foundation.dependent-pair-types
open import foundation.equality-dependent-pair-types
open import foundation.equivalences
open import foundation.fibers-of-maps
open import foundation.function-types
open import foundation.function-extensionality
open import foundation.functoriality-dependent-pair-types
open import foundation.homotopies
open import foundation.identity-types
open import foundation.locally-small-types
open import foundation.logical-equivalences
open import foundation.propositions
open import foundation.small-types
open import foundation.subtypes
open import foundation.transport-along-identifications
open import foundation.truncated-maps
open import foundation.truncated-types
open import foundation.truncation-levels
  renaming (truncation-level-minus-one-ℕ to minus-one;
    truncation-level-minus-two-ℕ to minus-two)
open import foundation.type-arithmetic-dependent-pair-types
open import foundation.type-theoretic-principle-of-choice
open import foundation.universe-levels

open import order-theory.accessible-elements-relations

open import functor.n-slice
open import notation hiding (sup)

module fixed-point.accessible-elements {i j} (n : ℕ)
  (V : UU j) (fixed-point-V : V ≃ P-[ n ] i V) where

-- Construction of ∈-structure on V
open import fixed-point.core n V fixed-point-V

data is-accessible-element-Relation' (x : V) : UU i where
  accessible' : ((a : pr1 (desup x)) → is-accessible-element-Relation' (pr2 (desup x) 〈 a 〉)) → is-accessible-element-Relation' x

all-elements-equal-is-accessible-element-Relation' : (x : V) → all-elements-equal (is-accessible-element-Relation' x)
all-elements-equal-is-accessible-element-Relation' x (accessible' f) (accessible' g) =
  ap accessible' (eq-htpy (λ a → all-elements-equal-is-accessible-element-Relation' (pr2 (desup x) 〈 a 〉) (f a) (g a)))

is-prop-is-accessible-element-Relation' : (x : V) → is-prop (is-accessible-element-Relation' x)
is-prop-is-accessible-element-Relation' x = is-prop-all-elements-equal (all-elements-equal-is-accessible-element-Relation' x)

is-accessible-element-Relation≃is-accessible-element-Relation' : (x : V) → is-accessible-element-Relation _∈_ x ≃ is-accessible-element-Relation' x
is-accessible-element-Relation≃is-accessible-element-Relation' x = equiv-prop (is-prop-is-accessible-element-Relation _∈_ x) (is-prop-is-accessible-element-Relation' x) (there x) (back x)
  where
    there : ∀ x → is-accessible-element-Relation _∈_ x → is-accessible-element-Relation' x
    there x (access f) =
      accessible' (λ a → there (pr2 (desup x) 〈 a 〉) (f {pr2 (desup x) 〈 a 〉} (a , refl)))

    back : ∀ x → is-accessible-element-Relation' x → is-accessible-element-Relation _∈_ x
    back x (accessible' f) =
      access (λ { .{pr2 (desup x) 〈 a 〉} (a , refl) → back _ (f a) })

-- is-accessible-element-Relation is small
is-small-is-accessible-element-Relation : (x : V) → is-small i (is-accessible-element-Relation _∈_ x)
pr1 (is-small-is-accessible-element-Relation x) = is-accessible-element-Relation' x
pr2 (is-small-is-accessible-element-Relation x) = is-accessible-element-Relation≃is-accessible-element-Relation' x

-- The subtype of accessible elements is locally small
is-locally-small-is-accessible-element-Relation-subtype : is-locally-small i (Σ V (is-accessible-element-Relation _∈_))
is-locally-small-is-accessible-element-Relation-subtype (x , accessible-x) (y , accessible-y) =
  is-small-equiv _
    (extensionality-type-subtype' (is-accessible-element-prop-Relation _∈_) _ _)
    (H x y accessible-x accessible-y)
  where
    H : (x y : V) → is-accessible-element-Relation _∈_ x → is-accessible-element-Relation _∈_ y → is-small i (x == y)
    H x y (access f) (access g) =
      is-small-equiv _
        (equiv-eq-P-[ n ] _ _ ∘e equiv-ap fixed-point-V x y)
        (is-small-Σ is-small'
          (λ α → is-small-Π is-small'
            (λ a → H (pr1 (desup x ↓) a)
                     (pr1 (desup y ↓) (map-equiv α a))
                     (f (a , refl))
                     (g (map-equiv α a , refl)))))

is-accessible-sup : ∀ A f → ((a : A) → is-accessible-element-Relation _∈_ (f 〈 a 〉))
           → is-accessible-element-Relation _∈_ (sup (A , f))
is-accessible-sup A f H =
  access (λ {y} p → tr (is-accessible-element-Relation _∈_) (pr2 (tr id (∈-sup y) p)) (H _))


-- The subtype of accessible elements is also a fixpoint for P-[ n ] i
fixed-point-is-accessible-element-Relation-V : Σ V (is-accessible-element-Relation _∈_) ≃ P-[ n ] i (Σ V (is-accessible-element-Relation _∈_))
fixed-point-is-accessible-element-Relation-V =
  equivalence-reasoning
    Σ V (is-accessible-element-Relation _∈_)
      ≃ Σ (P-[ n ] i V) (λ (A , f) → (a : A) → is-accessible-element-Relation _∈_ (f 〈 a 〉)) by equiv-subtype-equiv
                                                                        fixed-point-V
                                                                        (is-accessible-element-prop-Relation _∈_)
                                                                        (λ (A , f) → Π-Prop A (λ a → is-accessible-element-prop-Relation _∈_ (f 〈 a 〉)))
                                                                        (λ x →
                                                                          (λ accessible-x a → is-accessible-element-is-related-to-accessible-element-Relation _∈_ accessible-x (a , refl)) ,
                                                                          (λ p → map-inv-equiv
                                                                            (is-accessible-element-Relation≃is-accessible-element-Relation' x)
                                                                            (accessible' (λ a → map-equiv (is-accessible-element-Relation≃is-accessible-element-Relation' ((desup x ↓) 〈 a 〉)) (p a)))))
      ≃ Σ (UU i) (λ A →
          Σ (Σ (A → V) (λ f → (a : A) → is-accessible-element-Relation _∈_ (f a)))
            λ (f , _) → is-trunc-map (minus-one n) f)               by (equiv-tot (λ A → equiv-right-swap-Σ) ∘e associative-Σ _ _ _)
      ≃ Σ (UU i) (λ A →
        Σ (A → Σ V (is-accessible-element-Relation _∈_)) (λ f →
          is-trunc-map (minus-one n) (pr1 ∘ f)))                    by equiv-tot (λ A → equiv-Σ-equiv-base _ inv-distributive-Π-Σ)
      ≃ P-[ n ] i (Σ V (is-accessible-element-Relation _∈_))                                   by equiv-tot (λ A →
                                                                        equiv-tot (λ f →
                                                                          equiv-iff
                                                                            (is-trunc-map-Prop (minus-one n) (pr1 ∘ f))
                                                                            (is-trunc-map-Prop (minus-one n) f)
                                                                            (is-trunc-map-right-factor _ _ _ H)
                                                                            (is-trunc-map-comp _ _ _ H)))
  where
    H : is-trunc-map (minus-one n) (pr1 {B = is-accessible-element-Relation _∈_})
    H = is-trunc-map-pr1 _ (λ x → is-trunc-is-prop _ (is-prop-is-accessible-element-Relation _∈_ x))

-- The ∈-relation on the subtype of accessible elements
-- is the same as the original ∈-relation

open import fixed-point.core n (Σ V (is-accessible-element-Relation _∈_)) fixed-point-is-accessible-element-Relation-V
  renaming (_∈_ to _∈'_)

equiv-∈-accessible : (x y : Σ V (is-accessible-element-Relation _∈_))
            → (y ∈' x) ≃ (pr1 y ∈ pr1 x)
equiv-∈-accessible x y = equiv-tot (λ a → extensionality-type-subtype' (is-accessible-element-prop-Relation _∈_) _ _)

{-# OPTIONS --without-K #-}

open import elementary-number-theory.addition-natural-numbers
open import elementary-number-theory.natural-numbers

open import foundation.action-on-identifications-functions
open import foundation.dependent-pair-types
open import foundation.embeddings
open import foundation.equivalences
open import foundation.existential-quantification
open import foundation.fibers-of-maps
open import foundation.functoriality-dependent-pair-types
open import foundation.functoriality-truncation
open import foundation.identity-types
open import foundation.locally-small-types
open import foundation.propositional-truncations
open import foundation.truncated-maps
open import foundation.truncations
open import foundation.truncation-levels
  renaming (truncation-level-minus-one-ℕ to minus-one)
open import foundation.univalence
open import foundation.universe-levels

open import functor.n-slice
open import image-factorisation
open import notation hiding (sup)

module fixed-point.replacement {i j}
  (m n k : ℕ) (p : m == add-ℕ' n k)
  (V : UU j) (fixed-point-V : V ≃ P-[ m ] i V)
  (is-n+1-small-V : is-[ succ-ℕ n ]-small i V) where

-- Construction of ∈-structure on V
open import fixed-point.core m V fixed-point-V

-- Property of having replacement
open import e-structure.property.replacement ∈-structure-V

replacement-set : (x : V) → (El x → V) → V
replacement-set x F =
  let (A , f) = desup x in
  sup
    (trunc-Image is-n+1-small-V (λ a → F (f 〈 a 〉 , (a , refl))) ,
    trunc-image-inclusion-eq-add-ℕ' k (succ-ℕ m) (ap succ-ℕ p) is-n+1-small-V _)

∈-replacement-set : (x : V)
                  → (F : El x → V)
                  → (z : V)
                  → (z ∈ replacement-set x F) ≃ type-trunc (minus-one n) (Σ (El x) λ x → F x == z)
∈-replacement-set x F z =
  let (A , f) = desup x in
  equivalence-reasoning
    z ∈ replacement-set x F
      ≃ fiber (map-trunc-map
              (trunc-image-inclusion
              is-n+1-small-V
              (λ a → F (f 〈 a 〉 , (a , refl)))))
            z                                              by equiv-eq (∈-sup z)
      ≃ type-trunc (minus-one n)
          (fiber (λ a → F (f 〈 a 〉 , (a , refl))) z)        by equiv-fiber-trunc-Image-im is-n+1-small-V _ z
      ≃ type-trunc (minus-one n) (Σ (El x) λ x → F x == z) by equiv-trunc (minus-one n)
                                                                (equiv-Σ-equiv-base
                                                                  (λ (a , p) → F (a , p) == z)
                                                                  (inv-equiv-total-fiber (map-trunc-map f)))

n-replacement : [ n ]-Replacement
n-replacement x F = (replacement-set x F , ∈-replacement-set x F)


{-# OPTIONS --without-K #-}

open import elementary-number-theory.natural-numbers

open import foundation.action-on-identifications-functions
open import foundation.cartesian-product-types
open import foundation.contractible-types
open import foundation.dependent-pair-types
open import foundation.embeddings
open import foundation.equivalences
open import foundation.fibers-of-maps
open import foundation.function-types
open import foundation.functoriality-dependent-pair-types
open import foundation.homotopies
open import foundation.identity-types
open import foundation.locally-small-types
open import foundation.propositions
open import foundation.subtypes
open import foundation.truncated-maps
open import foundation.truncated-types
open import foundation.truncation-levels
  renaming (truncation-level-minus-one-ℕ to minus-one)
open import foundation.type-arithmetic-dependent-pair-types
open import foundation.univalence
open import foundation.universe-levels

open import functor.n-slice
open import notation hiding (sup)

module fixed-point.restricted-separation {i j} (n : ℕ)
  (V : UU j) (fixed-point-V : V ≃ P-[ n ] i V) where

-- Construction of ∈-structure on V
open import fixed-point.core n V fixed-point-V

-- Property of having restricted n-separation
open import e-structure.property.restricted-separation ∈-structure-V

separation-set : (x : V) → (El x → Truncated-Type i (minus-one n)) → V
separation-set x C =
  let (A , f) = desup x in
  sup
    (Σ A (λ a → type-Truncated-Type (C (f 〈 a 〉 , (a , refl)))) ,
    comp-trunc-map (minus-one n) f (pr1-trunc-map (minus-one n) (λ a → C (f 〈 a 〉 , (a , refl)))))

syntax separation-set x (λ z → C) = [ z ∈ x ¦ C ]

∈-separation-set : (x : V) (C : El x → Truncated-Type i (minus-one n))
                 → (z : V)
                 → (z ∈ [ y ∈ x ¦ C y ]) ≃ (Σ (z ∈ x) λ p → type-Truncated-Type (C (z , p)))
∈-separation-set x C z =
  let (A , f) = desup x in
  equivalence-reasoning
    z ∈ [ y ∈ x ¦ C y ]
      ≃ Σ (Σ A (λ a → type-Truncated-Type (C (f 〈 a 〉 , (a , refl)))))
          (λ (a , _) → f 〈 a 〉 == z)                                                   by equiv-eq (∈-sup z)
      ≃ Σ (Σ (El x) (λ (z' , p) → type-Truncated-Type (C (z' , p))))
          (λ ((z' , _) , _) → z' == z)                                                by equiv-Σ-equiv-base _
                                                                                           (equiv-Σ-equiv-base _
                                                                                             (inv-equiv-total-fiber (pr1 f)))
      ≃ Σ (Σ V (λ z' → z' == z))
          (λ (z' , _) → Σ (z' ∈ x) λ p → type-Truncated-Type (C (z' , p)))            by ((λ (((z' , p) , c) , q) →
                                                                                           ((z' , q) , (p , c))) ,
                                                                                         is-equiv-is-invertible
                                                                                           (λ ((z' , q) , (p , c)) →
                                                                                             (((z' , p) , c) , q))
                                                                                           refl-htpy
                                                                                           refl-htpy)
      ≃ Σ (z ∈ x) (λ p → type-Truncated-Type (C (z , p)))                             by left-unit-law-Σ-is-contr
                                                                                           (is-torsorial-path' z)
                                                                                           (z , refl)

restricted-n-separation : Restricted[ n ]-Separation i
restricted-n-separation C x = (separation-set C x , ∈-separation-set C x)


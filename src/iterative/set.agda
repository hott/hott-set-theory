{-# OPTIONS --without-K --rewriting --lossy-unification #-}

open import elementary-number-theory.addition-natural-numbers
open import elementary-number-theory.inequality-natural-numbers
open import elementary-number-theory.natural-numbers

open import foundation.action-on-identifications-functions
open import foundation.booleans
open import foundation.cartesian-product-types
open import foundation.coproduct-types
open import foundation.coherently-invertible-maps
open import foundation.contractible-types
open import foundation.dependent-pair-types
open import foundation.dependent-universal-property-equivalences
open import foundation.embeddings
open import foundation.empty-types
open import foundation.equality-cartesian-product-types
open import foundation.equality-coproduct-types
open import foundation.equality-dependent-pair-types
open import foundation.equivalences
open import foundation.equivalence-induction
open import foundation.families-of-maps
open import foundation.fibers-of-maps
open import foundation.function-extensionality
open import foundation.function-types
open import foundation.functoriality-dependent-function-types
open import foundation.functoriality-dependent-pair-types
open import foundation.homotopies
open import foundation.identity-types
open import foundation.images
open import foundation.injective-maps
open import foundation.logical-equivalences
open import foundation.monomorphisms
open import foundation.negated-equality
open import foundation.propositional-maps
open import foundation.propositions
open import foundation.raising-universe-levels
open import foundation.sets
open import foundation.subtypes
open import foundation.transport-along-identifications
open import foundation.truncated-maps
open import foundation.truncated-types
open import foundation.truncation-levels
  renaming (truncation-level-minus-one-ℕ to minus-one;
    truncation-level-minus-two-ℕ to minus-two;
    truncation-level-ℕ to to-𝕋)
open import foundation.type-arithmetic-booleans
open import foundation.type-arithmetic-coproduct-types
open import foundation.type-arithmetic-empty-type
open import foundation.type-arithmetic-unit-type
open import foundation.unit-type
open import foundation.univalence
open import foundation.universe-levels
open import foundation.locally-small-types

open import e-structure.core
open import functor.n-slice
open import functor.slice
open import image-factorisation
open import notation
open import set-quotient


module iterative.set where

private
  variable
    i : Level

{- V∞ is the unrestricted iterative hiearchy -}
V∞ : ∀ i → UU (lsuc i)
V∞ i = W (UU i) id

desup∞ : V∞ i → P∞ i (V∞ i)
desup∞ (sup A f) = (A , f)

{- The underlying type of an element in V∞ -}
El∞ : V∞ i → UU i
El∞ x = ¦ desup∞ x ¦

{- Equality on V∞ are equivalences
   of the underlying types which commute with
   the indexing function -}

infix 100 _==V∞_

_==V∞_ : V∞ i → V∞ i → UU i
(sup A f) ==V∞ (sup B g) = Σ (A ≃ B) λ α → ∀ x → f x ==V∞ (g (α 〈 x 〉))

equiv-Eq-V∞ : (x y : V∞ i)
            → (x ==W y) ≃ (x ==V∞ y)
equiv-Eq-V∞ (sup A f) (sup B g)
   = equiv-Σ _
         equiv-univalence
         (λ {refl → equiv-Π-equiv-family (λ a → equiv-Eq-V∞ _ _) })

equiv-eq-V∞ : (x y : V∞ i)
            → (x == y) ≃ (x ==V∞ y)
equiv-eq-V∞ x y = (equiv-Eq-V∞ _ _) ∘e  (equiv-Eq-𝕎-eq _ _)


{- The iterative n-types in V∞ are those for which
   the indexing function is an n-truncated map on all levels. -}

is-iterative-[_]-type : 𝕋 → V∞ i → UU (lsuc i)
is-iterative-[ n ]-type (sup A f) =
  is-trunc-map n f × ((a : A) → is-iterative-[ n ]-type (f a))

is-prop-is-iterative-[_]-type : (n : 𝕋) (x : V∞ i) → is-prop (is-iterative-[ n ]-type x)
is-prop-is-iterative-[ n ]-type (sup A f)
  = is-prop-prod (is-prop-is-trunc-map n f) (is-prop-Π λ x → is-prop-is-iterative-[ n ]-type _)

is-iterative-[_]-type-Prop : 𝕋 → V∞ i → Prop (lsuc i)
pr1 (is-iterative-[ n ]-type-Prop x) = is-iterative-[ n ]-type x
pr2 (is-iterative-[ n ]-type-Prop x) = is-prop-is-iterative-[ n ]-type x

is-iterative-[succ-𝕋_]-type : (n : 𝕋) (x : V∞ i)
                            → is-iterative-[ n ]-type x
                            → is-iterative-[ succ-𝕋 n ]-type x
pr1 (is-iterative-[succ-𝕋 n ]-type (sup A f) (H , K)) =
  is-trunc-map-succ-is-trunc-map n H
pr2 (is-iterative-[succ-𝕋 n ]-type (sup A f) (H , K)) a =
  is-iterative-[succ-𝕋 n ]-type (f a) (K a)


{- V-[n] is the subtype of V∞ of iterative (n-1)-types. -}
V-[_] : ℕ → (i : Level) → UU (lsuc i)
V-[ n ] i = Σ (V∞ i) is-iterative-[ minus-one n ]-type

{- Some special cases -}

V⁰ : ∀ i → UU (lsuc i)
V⁰ = V-[ 0 ]

V¹ : ∀ i → UU (lsuc i)
V¹ = V-[ 1 ]

{- Inclusion of Vⁿ into V∞ -}

V-[_]↪V∞ : (n : ℕ) → V-[ n ] i ↪ V∞ i
V-[ n ]↪V∞ = emb-subtype is-iterative-[ minus-one n ]-type-Prop

V⁰↪V∞ : V⁰ i ↪ V∞ i
V⁰↪V∞ = V-[ 0 ]↪V∞

is-trunc-map-V-[_]↪V∞ : (n : ℕ)
                      → is-trunc-map (minus-one n) (map-emb (V-[_]↪V∞ {i} n))
is-trunc-map-V-[ n ]↪V∞ =
  is-trunc-map-is-prop-map
    (minus-two n)
    (is-prop-map-is-emb
      (is-emb-map-emb V-[ n ]↪V∞))

{- Inclusion of Vⁿ into Vⁿ⁺¹ -}

V-[_]↪V-[succ-ℕ] : (n : ℕ) → V-[ n ] i ↪ V-[ succ-ℕ n ] i
V-[ n ]↪V-[succ-ℕ] =
  emb-into-subtype
    is-iterative-[ to-𝕋 n ]-type-Prop
    V-[ n ]↪V∞
    (λ (x , p) → is-iterative-[succ-𝕋 (minus-one n) ]-type x p)

Eq-V : (n : ℕ) → V-[ n ] i → V-[ n ] i → UU i
Eq-V n x y = V-[ n ]↪V∞ 〈 x 〉 ==V∞ V-[ n ]↪V∞ 〈 y 〉

syntax Eq-V n x y = x ==V-[ n ] y

equiv-eq-Eq-V : {n : ℕ} (x y : V-[ n ] i)
              → (x == y) ≃ (x ==V-[ n ] y)
equiv-eq-Eq-V {n = n} x y =
  equiv-eq-V∞ (pr1 x) (pr1 y) ∘e
  extensionality-type-subtype' is-iterative-[ minus-one n ]-type-Prop x y

is-locally-small-V : {n : ℕ} → is-locally-small i (V-[ n ] i)
pr1 (is-locally-small-V {n = n} x y) = x ==V-[ n ] y
pr2 (is-locally-small-V x y) = equiv-eq-Eq-V x y

is-locally-small-V⁰ : is-locally-small i (V⁰ i)
is-locally-small-V⁰ = is-locally-small-V


is-[succ-ℕ_]-small-V : {n : ℕ} (k : ℕ) → is-[ succ-ℕ k ]-small i (V-[ n ] i)
is-[succ-ℕ k ]-small-V =
  is-[succ-ℕ k ]-small-is-locally-small is-locally-small-V

{- sup-[ n ] and desup-[ n ] defined below make V-[ n ] into a fixed
   point for P-[ n ] i -}

sup-[_] : (n : ℕ) → P-[ n ] i (V-[ n ] i) → V-[ n ] i
pr1 (sup-[ n ] (A , f)) = sup A (map-emb V-[ n ]↪V∞ ∘ map-trunc-map f)
pr1 (pr2 (sup-[ n ] (A , f))) =
  is-trunc-map-comp
    (minus-one n)
    (map-emb V-[ n ]↪V∞)
    (map-trunc-map f)
    (is-trunc-map-V-[ n ]↪V∞)
    (pr2 f)
pr2 (pr2 (sup-[ n ] (A , f))) a = pr2 (f 〈 a 〉)

-- Some special cases
sup⁰ : P⁰ i (V⁰ i) → V⁰ i
sup⁰ = sup-[ 0 ]

-- For V⁰ we may equivalently construct an element from
-- a small type and an embedding into V⁰
sup⁰' : Σ (UU i) (λ A → A ↪ V⁰ i) → V⁰ i
sup⁰' (A , f) = sup⁰ (A , (map-emb f , is-prop-map-is-emb (is-emb-map-emb f)))

sup¹ : P¹ i (V¹ i) → V¹ i
sup¹ = sup-[ 1 ]

desup-[_] : (n : ℕ) → V-[ n ] i → P-[ n ] i (V-[ n ] i)
pr1 (desup-[ n ] (sup A f , p)) = A
pr1 (pr2 (desup-[ n ] (sup A f , p))) a = (f a , pr2 p a)
pr2 (pr2 (desup-[ n ] (sup A f , p))) =
  is-trunc-map-right-factor
    (minus-one n)
    (map-emb V-[ n ]↪V∞)
    (λ a → f a , pr2 p a)
    (is-trunc-map-V-[ n ]↪V∞)
    (pr1 p)

-- Some special cases
desup⁰ : V⁰ i → P⁰ i (V⁰ i)
desup⁰ = desup-[ 0 ]

desup¹ : V¹ i → P¹ i (V¹ i)
desup¹ = desup-[ 1 ]

{- The underlying type of an element in Vⁿ -}

El-[_] : (n : ℕ) → V-[ n ] i → UU i
El-[ n ] x = ¦ desup-[ n ] x ¦

El⁰ : V⁰ i → UU i
El⁰ = El-[ 0 ]

El¹ : V¹ i → UU i
El¹ = El-[ 1 ]


V-P-[_]-Coalg : (n : ℕ) → (i : Level) → P-[ n ]-Coalg i (lsuc i)
pr1 (V-P-[ n ]-Coalg i) = V-[ n ] i
pr2 (V-P-[ n ]-Coalg i) = desup-[ n ]

-- Some special cases
V⁰-P⁰-Coalg : ∀ i → P⁰-Coalg i (lsuc i)
V⁰-P⁰-Coalg = V-P-[ 0 ]-Coalg

V¹-P¹-Coalg : ∀ i → P¹-Coalg i (lsuc i)
V¹-P¹-Coalg = V-P-[ 1 ]-Coalg

V-P-[_]-Alg : (n : ℕ) → (i : Level) → P-[ n ]-Alg i (lsuc i)
pr1 (V-P-[ n ]-Alg i) = V-[ n ] i
pr2 (V-P-[ n ]-Alg i) = sup-[ n ]

-- Some special cases
V⁰-P⁰-Alg : ∀ i → P⁰-Alg i (lsuc i)
V⁰-P⁰-Alg = V-P-[ 0 ]-Alg

V¹-P¹-Alg : ∀ i → P¹-Alg i (lsuc i)
V¹-P¹-Alg = V-P-[ 1 ]-Alg

sup-desup-[_] : (n : ℕ) (x : V-[ n ] i)
              → sup-[ n ] (desup-[ n ] x) == x
sup-desup-[ n ] (sup A f , p) =
  eq-type-subtype
    is-iterative-[ minus-one n ]-type-Prop
    refl

desup-sup-[_] : (n : ℕ) (x : P-[ n ] i (V-[ n ] i))
              → desup-[ n ] (sup-[ n ] x) == x
desup-sup-[ n ] (A , f) =
  eq-pair-Σ
    refl
    (eq-type-subtype
      (is-trunc-map-Prop (minus-one n))
      refl)

sup-[_]-equiv : (n : ℕ) {i : Level}
              → P-[ n ] i (V-[ n ] i) ≃ V-[ n ] i
pr1 sup-[ n ]-equiv = sup-[ n ]
pr2 sup-[ n ]-equiv =
  is-equiv-is-invertible desup-[ n ] sup-desup-[ n ] desup-sup-[ n ]

-- Some special cases
sup⁰-equiv : P⁰ i (V⁰ i) ≃ V⁰ i
sup⁰-equiv = sup-[ 0 ]-equiv

sup¹-equiv : P¹ i (V¹ i) ≃ V¹ i
sup¹-equiv = sup-[ 1 ]-equiv

desup-[_]-equiv : (n : ℕ) {i : Level}
                → V-[ n ] i ≃ P-[ n ] i (V-[ n ] i)
pr1 desup-[ n ]-equiv = desup-[ n ]
pr2 desup-[ n ]-equiv =
  is-equiv-is-invertible sup-[ n ] desup-sup-[ n ] sup-desup-[ n ]

-- Some special cases
desup⁰-equiv : V⁰ i ≃ P⁰ i (V⁰ i)
desup⁰-equiv = desup-[ 0 ]-equiv

desup¹-equiv : V¹ i ≃ P¹ i (V¹ i)
desup¹-equiv = desup-[ 1 ]-equiv

{- A few different elimination and recursion principles
   for V⁰ -}

{- V-[ n ]-elim is the expected elimination principle
   if we regard sup-[ n ] as a constructor -}

V-[_]-elim : ∀ {i j}
           → (n : ℕ)
           → (P : V-[ n ] i → UU j)
           → (∀ A f → ((a : A) → P (f 〈 a 〉)) → P (sup-[ n ] (A , f)))
           → (x : V-[ n ] i) → P x
V-[ n ]-elim P H x@(sup A f , (p₁ , p₂)) =
  let f' = pr2 (desup-[ n ] x) in
  tr P
     (sup-desup-[ n ] x)
     (H A f' (λ a → V-[ n ]-elim P H (f a , p₂ a)))

-- Some special cases
V⁰-elim : ∀ {i j}
        → (P : V⁰ i → UU j)
        → (∀ A f → (∀ a → P (f 〈 a 〉)) → P (sup⁰ (A , f)))
        → ∀ x → P x
V⁰-elim = V-[ 0 ]-elim

V¹-elim : ∀ {i j}
        → (P : V¹ i → UU j)
        → (∀ A f → (∀ a → P (f 〈 a 〉)) → P (sup¹ (A , f)))
        → ∀ x → P x
V¹-elim = V-[ 1 ]-elim

-- The computation rule for V-[ n ]-elim holds only up to homotopy
V-[_]-elim-comp : (n : ℕ)
           → {i j : Level}
           → (P : V-[ n ] i → UU j)
           → (H : ∀ A f → (∀ a → P (f 〈 a 〉)) → P (sup-[ n ] (A , f)))
           → ∀ A f
           → V-[ n ]-elim P H (sup-[ n ] (A , f)) == H A f (V-[ n ]-elim P H ∘ map-trunc-map f)
V-[ n ]-elim-comp {i} P H A f =
  ind-equiv
    (λ B e
       → (q : ∀ b → P (pr2 (pr2 e) 〈 b 〉))
       → (b : B)
       → tr P (pr2 (pr2 (pr2 e)) (pr2 (pr2 e) 〈 b 〉)) (q (pr1 e (pr2 (pr2 e) 〈 b 〉))) == q b)
    (λ q b → refl)
    (desup-[ n ]-equiv)
    (λ (A' , f') → H A' f' (V-[ n ]-elim P H ∘ map-trunc-map f'))
    (A , f)

-- Recursion principle into P∞-algebras, uncurried
V-[_]-P∞-rec : (n : ℕ)
             → {i j : Level}
             → (X : UU j)
             → ((A : UU i) → (A → X) → X)
             → V-[ n ] i → X
V-[ n ]-P∞-rec X m (sup A f , (p₁ , p₂)) =
  m A (λ a → V-[ n ]-P∞-rec X m (f a , p₂ a))

-- Recursion principle into Pⁿ-algebras, uncurried
V-[_]-P-rec : (n : ℕ)
            → {i j : Level}
            → (X : UU j)
            → is-[ succ-ℕ n ]-small i X
            → (P-[ n ] i X → X)
            → V-[ n ] i → X
V-[ n ]-P-rec X p m =
  V-[ n ]-P∞-rec X (λ A f → m (trunc-Image p f , trunc-image-inclusion p f))

-- Recursion principle into Pⁿ-algebras
V-[_]-P-Alg-rec : (n : ℕ)
                → {i j : Level}
                → (X : P-[ n ]-Alg i j)
                → (p : is-[ succ-ℕ n ]-small i ¦ X ¦)
                → hom-P-[ n ]-Alg (V-P-[ n ]-Alg i) X p
pr1 (V-[ n ]-P-Alg-rec X p) = V-[ n ]-P-rec ¦ X ¦ p (X ↓)
pr2 (V-[ n ]-P-Alg-rec X p) s = refl

-- The recursor is unique as a function
V-[_]-P-rec-unique : (n : ℕ)
                   → {i j : Level}
                   → (X : P-[ n ]-Alg i j)
                   → (p : is-[ succ-ℕ n ]-small i ¦ X ¦)
                   → (φ : hom-P-[ n ]-Alg (V-P-[ n ]-Alg i) X p)
                   → pr1 φ ~ V-[ n ]-P-rec ¦ X ¦ p (X ↓)
V-[ n ]-P-rec-unique X p φ =
  V-[ n ]-elim
    (λ x → φ 〈 x 〉 == V-[ n ]-P-rec ¦ X ¦ p (X ↓) x)
    (λ A f IH →
      pr2 φ (A , f) ∙
      ap (λ h →
            (X ↓)
            (trunc-Image p h ,
             trunc-image-inclusion p h))
         (eq-htpy IH))

-- The recursor is unique as a Pⁿ-Alg homomorphism
V-[_]-P-Alg-Eq-rec-unique : (n : ℕ)
                          → {i j : Level}
                          → (X : P-[ n ]-Alg i j)
                          → (p : is-[ succ-ℕ n ]-small i ¦ X ¦)
                          → (φ : hom-P-[ n ]-Alg (V-P-[ n ]-Alg i) X p)
                          → hom-P-[ n ]-Alg-Eq (V-P-[ n ]-Alg i) X p φ (V-[ n ]-P-Alg-rec X p)
pr1 (V-[ n ]-P-Alg-Eq-rec-unique X p φ) = V-[ n ]-P-rec-unique X p φ
pr2 (V-[ n ]-P-Alg-Eq-rec-unique X p (φ , α)) (A , f) =
  ap (λ q → α (A , f) ∙ ap (X ↓) q)
      (ap-comp _
         (_∘ map-trunc-map f)
         (eq-htpy (V-[ n ]-P-rec-unique X p (φ , α)))) ∙
  (ap (λ q → α (A , f) ∙
              ap (X ↓) (ap (λ h → trunc-Image p h , trunc-image-inclusion p h) q))
       (eq-htpy-comp (map-trunc-map f) (V-[ n ]-P-rec-unique X p (φ , α))) ∙
  (ap (α (A , f) ∙_) (inv (ap-comp (X ↓) _ _)) ∙
  (inv (V-[ n ]-elim-comp
         (λ x → φ x == V-[ n ]-P-rec ¦ X ¦ p (X ↓) x)
         (λ A f IH →
           α (A , f) ∙
           ap (λ h → (X ↓) (trunc-Image p h , trunc-image-inclusion p h)) (eq-htpy IH))
         A f) ∙
  inv right-unit)))

V-[_]-P-Alg-rec-unique : (n : ℕ)
                       → {i j : Level}
                       → (X : P-[ n ]-Alg i j)
                       → (p : is-[ succ-ℕ n ]-small i ¦ X ¦)
                       → (φ : hom-P-[ n ]-Alg (V-P-[ n ]-Alg i) X p)
                       → φ == V-[ n ]-P-Alg-rec X p
V-[ n ]-P-Alg-rec-unique X p φ =
  hom-P-[ n ]-Alg-eq {Y = X} p φ (V-[ n ]-P-Alg-rec X p) (V-[ n ]-P-Alg-Eq-rec-unique X p φ)

{- Cumulativity of Vⁿ -}

map-V∞-V∞⁺ : ∀ i → V∞ i → V∞ (lsuc i)
map-V∞-V∞⁺ i (sup A f) = sup (raise (lsuc i) A) (λ a → map-V∞-V∞⁺ i (f (map-inv-raise a)))

V∞-in-V∞⁺ : ∀ i → V∞ (lsuc i)
V∞-in-V∞⁺ i = sup (V∞ i) (map-V∞-V∞⁺ i)

is-emb-map-V∞-V∞⁺ : is-emb (map-V∞-V∞⁺ i)
is-emb-map-V∞-V∞⁺ {i} (sup A f) (sup B g) =
  is-equiv-equiv'
    {f =
      ap (map-Σ
           (λ A → A → V∞ (lsuc i))
           (raise (lsuc i))
           (λ A f a → map-V∞-V∞⁺ i (f (map-inv-raise a))))}
    (equiv-ap equiv-structure-𝕎-Alg (A , f) (B , g))
    (equiv-ap equiv-structure-𝕎-Alg _ _)
    (λ { refl → refl })
    (is-equiv-htpy
      (ap (tot (λ A' f' → map-V∞-V∞⁺ i ∘ f')) ∘
       ap (map-Σ (λ A → A → V∞ i) (raise (lsuc i)) (λ A f a → f (map-inv-raise a))))
      (λ { refl → refl })
      (is-equiv-comp
        (ap (tot (λ A' f' → map-V∞-V∞⁺ i ∘ f')))
        (ap (map-Σ (λ A → A → V∞ i) (raise (lsuc i)) (λ A f a → f (map-inv-raise a))))
        (is-emb-map-Σ _
          (is-emb-raise (lsuc i))
          (λ A' →
            is-emb-is-equiv
              (is-equiv-map-equiv
                (equiv-precomp-Π
                  (inv-equiv (compute-raise (lsuc i) A'))
                  (λ _ → V∞ i))))
          (A , f)
          (B , g))
        (is-equiv-equiv
          (equiv-eq-P∞
            (raise (lsuc i) A , f ∘ map-inv-raise)
            (raise (lsuc i) B , g ∘ map-inv-raise))
          (equiv-eq-P∞
            (raise (lsuc i) A , map-V∞-V∞⁺ i ∘ f ∘ map-inv-raise)
            (raise (lsuc i) B , map-V∞-V∞⁺ i ∘ g ∘ map-inv-raise))
          lem1
          (is-equiv-tot-is-fiberwise-equiv (λ e →
            is-equiv-map-Π-is-fiberwise-equiv (λ a →
              is-emb-map-V∞-V∞⁺
                (f (map-inv-raise a))
                (g (map-inv-raise (map-equiv e a)))))))))
  where
    lem1 : {(A' , f') (B' , g') : P∞ (lsuc i) (V∞ i)}
         → map-equiv (equiv-eq-P∞ (A' , map-V∞-V∞⁺ i ∘ f') (B' , map-V∞-V∞⁺ i ∘ g'))
           ∘ ap (tot (λ A'' f'' → map-V∞-V∞⁺ i ∘ f''))
         ~ tot (λ e H a → ap (map-V∞-V∞⁺ i) (H a))
           ∘ map-equiv (equiv-eq-P∞ (A' , f') (B' , g'))
    lem1 refl = refl

V∞↪V∞⁺ : ∀ i → V∞ i ↪ V∞ (lsuc i)
pr1 (V∞↪V∞⁺ i) = map-V∞-V∞⁺ i
pr2 (V∞↪V∞⁺ i) = is-emb-map-V∞-V∞⁺

is-iterative-[_]-type-V∞-V∞⁺ : (n : 𝕋) (x : V∞ i)
                            → is-iterative-[ succ-𝕋 n ]-type x
                            → is-iterative-[ succ-𝕋 n ]-type (map-V∞-V∞⁺ i x)
pr1 (is-iterative-[_]-type-V∞-V∞⁺ {i} n (sup A f) (p , q)) =
  is-trunc-map-comp (succ-𝕋 n)
    (map-V∞-V∞⁺ i ∘ f)
    map-inv-raise
    (is-trunc-map-comp (succ-𝕋 n)
      (map-V∞-V∞⁺ i)
      f
      (is-trunc-map-is-prop-map n
        (is-prop-map-is-emb is-emb-map-V∞-V∞⁺))
      p)
    (is-trunc-map-is-equiv (succ-𝕋 n)
      (is-equiv-map-inv-equiv (compute-raise _ A)))
pr2 (is-iterative-[ n ]-type-V∞-V∞⁺ (sup A f) (p , q)) (map-raise a) =
  is-iterative-[ n ]-type-V∞-V∞⁺ (f a) (q a)

V-[_]↪V⁺ : (n : ℕ) (i : Level) → V-[ n ] i ↪ V-[ n ] (lsuc i)
V-[ n ]↪V⁺ i =
  emb-into-subtype
    is-iterative-[ minus-one n ]-type-Prop
    (comp-emb (V∞↪V∞⁺ i) V-[ n ]↪V∞)
    (λ (x , p) → is-iterative-[ minus-two n ]-type-V∞-V∞⁺ x p)

V-[_]-in-V⁺ : (n : ℕ) (i : Level) → V-[ n ] (lsuc i)
V-[ n ]-in-V⁺ i =
  sup-[ n ]
    (V-[ n ] i ,
    pr1 (V-[ n ]↪V⁺ i) ,
    is-trunc-map-is-prop-map (minus-two n) (is-prop-map-is-emb (pr2 (V-[ n ]↪V⁺ i))))

{- W-types in V∞ -}

W∞ : (ordered-pairs : V∞ i × V∞ i ↪ V∞ i)
   → (A : V∞ i) → (El∞ A → V∞ i) → V∞ i
W∞ {i} ordered-pairs A B = sup (W A' B') map-W-A-B
  where
    import e-structure.core
    open e-structure.core.ordered-pairing-structure.notation (V∞ i) ordered-pairs

    A' : UU i
    A' = El∞ A

    B' : A' → UU i
    B' = El∞ ∘ B

    f : A' → (V∞ i)
    f = pr2 (desup∞ A)

    g : ∀ a → B' a → V∞ i
    g a = pr2 (desup∞ (B a))

    map-W-A-B : W A' B' → V∞ i
    map-W-A-B (sup a α) = ⟨ f a , sup (B' a) (λ b → ⟨ g a b , map-W-A-B (α b) ⟩) ⟩


{- Vⁿ as a universe -}
module _  (n : ℕ) {i : Level} where
  open import e-structure.from-P-n-coalgebra
    n
    (V-P-[ n ]-Coalg i)
    (is-emb-is-equiv (pr2 desup-[ n ]-equiv))

  is-trunc-El-[_] : (x : V-[ n ] i) → is-trunc (to-𝕋 n) (El-[ n ] x)
  is-trunc-El-[_] = is-trunc-index-type

  is-trunc-V-[_] : is-trunc (to-𝕋 n) (V-[ n ] i)
  is-trunc-V-[_] = is-trunc-carrier-P-Coalg

module _ (n : ℕ) (i : Level) where

  V-[_]-Truncated-Type : Truncated-Type (lsuc i) (to-𝕋 n)
  pr1 V-[_]-Truncated-Type = V-[ n ] i
  pr2 V-[_]-Truncated-Type = is-trunc-V-[ n ]

  El-[_]-Truncated-Type : V-[ n ] i → Truncated-Type i (to-𝕋 n)
  pr1 (El-[_]-Truncated-Type x) = El-[ n ] x
  pr2 (El-[_]-Truncated-Type x) = is-trunc-El-[ n ] x

module _  (n : ℕ) {i : Level} where
  open import e-structure.from-P-n-coalgebra
    n
    (V-P-[ n ]-Coalg i)
    (is-emb-is-equiv (pr2 desup-[ n ]-equiv))
  open import e-structure.internalisations ∈-structure-P

  internalisable-repr-iff-trunc-map : {A : UU i} → (f : Representation A)
                                   → is-trunc-map (minus-one n) f
                                   ↔ InternalisationOfRepr f
  pr1 (internalisable-repr-iff-trunc-map {A = A} f) H =
    (sup-[ n ] (A , (f , H)) , (λ z → id-equiv))
  pr2 (internalisable-repr-iff-trunc-map f) (a , H) z =
    is-trunc-equiv _ _
      (inv-equiv (H z))
      is-trunc-∈

  equiv-trunc-repr-internalisations : (A : UU i)
                                    → trunc-map (minus-one n) A (V-[ n ] i)
                                    ≃ InternalisationOfType A
  equiv-trunc-repr-internalisations A =
    equiv-IntOfRepr-IntOfType A ∘e
    equiv-tot (λ f →
      equiv-iff
        (is-trunc-map-Prop (minus-one n) f)
        (InternalisationOfRepr-Prop f)
        (pr1 (internalisable-repr-iff-trunc-map f))
        (pr2 (internalisable-repr-iff-trunc-map f)))

-- The empty set
module _  (n : ℕ) {i : Level} where
  open import e-structure.from-P-n-coalgebra
    n
    (V-P-[ n ]-Coalg i)
    (is-emb-is-equiv (pr2 desup-[ n ]-equiv))
  open e-structure.core.mere-sets ∈-structure-P

  empty-[_] : V-[ n ] i
  empty-[_] =
    sup-[ n ]
      (raise-empty i ,
      (raise-ex-falso i ,
      is-trunc-map-is-prop-map _
        (is-prop-map-is-emb
          (pr2 (raise-ex-falso-emb i)))))

  mere-set-empty-[_] : mere-set empty-[_]
  mere-set-empty-[_] x =
    is-trunc-equiv _ _
      (left-absorption-Σ
        (λ y → raise-ex-falso i (map-raise y) == x) ∘e
      inv-equiv
        (equiv-Σ-equiv-base
          (λ y → raise-ex-falso i y == x)
          (compute-raise i empty)))
      is-prop-empty

-- Any n-type is internalisable in Vⁿ⁺¹
module _  (n : ℕ) {i : Level} where
  open import e-structure.from-P-n-coalgebra
    (succ-ℕ n)
    (V-P-[ succ-ℕ n ]-Coalg i)
    (is-emb-is-equiv (pr2 desup-[ succ-ℕ n ]-equiv))
  open import e-structure.internalisations ∈-structure-P

  InternalisationOfType-V-[succ-ℕ_] : (A : Truncated-Type i (to-𝕋 n))
                                    → InternalisationOfType (pr1 A)
  pr1 (InternalisationOfType-V-[succ-ℕ_] (A , is-n-type-A)) =
    sup-[ succ-ℕ n ]
      (A ,
      ((λ _ → empty-[ succ-ℕ n ]) ,
      (λ z → is-trunc-Σ is-n-type-A (λ a → is-trunc-V-[ succ-ℕ n ] _ _))))
  pr2 (InternalisationOfType-V-[succ-ℕ_] (A , is-n-type-A)) =
    equiv-El-pr1-desup _

-- The unit type and booleans
module _  (n : ℕ) {i : Level} where
  open import e-structure.from-P-n-coalgebra
    n
    (V-P-[ n ]-Coalg i)
    (is-emb-is-equiv (pr2 desup-[ n ]-equiv))
  open e-structure.core.mere-sets ∈-structure-P

  unit-[_] : V-[ n ] i
  unit-[_] =
    sup-[ n ]
      (raise-unit i ,
      ((λ _ → empty-[ n ]) ,
      (λ z →
        is-trunc-Σ
          (is-trunc-is-prop _ is-prop-raise-unit)
          (λ x → is-trunc-is-prop _
            (is-prop-eq-mere-set mere-set-empty-[ n ])))))

  mere-set-unit-[_] : mere-set unit-[_]
  mere-set-unit-[_] x =
    is-trunc-equiv _ _
      (left-unit-law-Σ
        (λ y → empty-[ n ] == x) ∘e
      inv-equiv
        (equiv-Σ-equiv-base
          (λ y → empty-[ n ] == x)
          (compute-raise i unit)))
      (is-prop-eq-mere-set mere-set-empty-[ n ])

  empty≠unit-[_] : empty-[ n ] ≠ unit-[_]
  empty≠unit-[_] empty-n=unit-n =
    map-inv-raise
      (map-inv-equiv
        (pr1 (map-equiv (equiv-eq-Eq-V empty-[ n ] unit-[_]) empty-n=unit-n))
        (map-raise star))

  bool-[_] : V-[ n ] i
  bool-[_] = sup-[ n ] (raise-bool i , (ι , is-trunc-map-ι))
    where
      ι : raise-bool i → V-[ n ] i
      ι (map-raise true) = unit-[_]
      ι (map-raise false) = empty-[ n ]

      contr-fib-at-values-ι : (b : raise-bool i)
                            → is-contr (fiber ι (ι b))
      contr-fib-at-values-ι (map-raise true) =
        is-contr-equiv _
          (right-unit-law-coprod-is-empty _ _
            empty≠unit-[_] ∘e
          equiv-Σ-bool-coprod (λ y → ι (map-raise y) == unit-[_]) ∘e
          inv-equiv
            (equiv-Σ-equiv-base
              (λ y → ι y == unit-[_])
              (compute-raise i bool)))
          (is-proof-irrelevant-is-prop
            (is-prop-eq-mere-set mere-set-unit-[_])
            refl)
      contr-fib-at-values-ι (map-raise false) =
        is-contr-equiv _
          (left-unit-law-coprod-is-empty _ _
            (λ p → empty≠unit-[_] (inv p)) ∘e
          equiv-Σ-bool-coprod (λ y → ι (map-raise y) == empty-[ n ]) ∘e
          inv-equiv
            (equiv-Σ-equiv-base
              (λ y → ι y == empty-[ n ])
              (compute-raise i bool)))
          (is-proof-irrelevant-is-prop
            (is-prop-eq-mere-set mere-set-empty-[ n ])
            refl)

      is-trunc-map-ι : is-trunc-map (minus-one n) ι
      is-trunc-map-ι =
        is-trunc-map-is-prop-map _
          (is-prop-map-is-emb
            (is-emb-is-contr-fibers-values ι
              contr-fib-at-values-ι))

module _ (n : ℕ) {i : Level} where
  open import fixed-point.unordered-tupling
    n n 0 (commutative-add-ℕ n 0) (V-[ n ] i) desup-[ n ]-equiv is-[succ-ℕ n ]-small-V
  open ordered-pairs-from-k=0 refl
  open import fixed-point.exponentiation
    n (V-[ n ] i) desup-[ n ]-equiv ordered-pairs

  Π-[_] : (A : V-[ n ] i) → (El-[ n ] A → V-[ n ] i) → V-[ n ] i
  Π-[_] A B = sup-[ n ] ((∀ a → B' a) , graph f g , is-trunc-map-graph f g) where
      A' : UU i
      A' = El-[ n ] A

      B' : A' → UU i
      B' = El-[ n ] ∘ B

      f : trunc-map (minus-one n) A' (V-[ n ] i)
      f = pr2 (desup-[ n ] A)

      g : ∀ a → trunc-map (minus-one n) (B' a) (V-[ n ] i)
      g a = pr2 (desup-[ n ] (B a))

  Σ-[_] : (A : V-[ n ] i) → (El-[ n ] A → V-[ n ] i) → V-[ n ] i
  Σ-[_] A B =
    sup-[ n ]
      (Σ A' B' ,
      comp-trunc-map (minus-one n)
        (pr1 ordered-pairs , is-trunc-map-is-prop-map _ (is-prop-map-is-emb (pr2 ordered-pairs)))
        ((map-Σ _ (f 〈_〉) (_〈_〉 ∘ g)) , is-trunc-map-map-Σ _ _ (pr2 f) (pr2 ∘ g)))
    where
      A' : UU i
      A' = El-[ n ] A

      B' : A' → UU i
      B' = El-[ n ] ∘ B

      f : trunc-map (minus-one n) A' (V-[ n ] i)
      f = pr2 (desup-[ n ] A)

      g : ∀ a → trunc-map (minus-one n) (B' a) (V-[ n ] i)
      g a = pr2 (desup-[ n ] (B a))

  fun-[_] : V-[ n ] i → V-[ n ] i → V-[ n ] i
  fun-[_] A B = Π-[_] A (λ _ → B)

  prod-[_] : V-[ n ] i → V-[ n ] i → V-[ n ] i
  prod-[_] A B = Σ-[_] A (λ _ → B)

  coprod-[_] : V-[ n ] i → V-[ n ] i → V-[ n ] i
  coprod-[_] A B =
    sup-[ n ] (El-[ n ] A + El-[ n ] B , trunc-map-f-g)
    where
      f : trunc-map (minus-one n) (El-[ n ] A) (V-[ n ] i)
      f =
        comp-trunc-map _
          ((λ x → ⟨ empty-[ n ] , x ⟩) ,
          is-trunc-map-fix-pr1 _
            (is-trunc-map-is-prop-map _
              (is-prop-map-is-emb (pr2 ordered-pairs)))
            is-trunc-V-[ n ]
            empty-[ n ])
          (pr2 (desup-[ n ] A))

      g : trunc-map (minus-one n) (El-[ n ] B) (V-[ n ] i)
      g =
        comp-trunc-map _
          ((λ x → ⟨ unit-[ n ] , x ⟩) ,
          is-trunc-map-fix-pr1 _
            (is-trunc-map-is-prop-map _
              (is-prop-map-is-emb (pr2 ordered-pairs)))
            is-trunc-V-[ n ]
            unit-[ n ])
          (pr2 (desup-[ n ] B))

      no-overlap : (a : El-[ n ] A) (b : El-[ n ] B) → f 〈 a 〉 ≠ g 〈 b 〉
      no-overlap a b fa=gb =
        empty≠unit-[ n ]
          (ap pr1
              (map-inv-is-equiv
                (pr2 ordered-pairs
                  (empty-[ n ] , pr2 (desup-[ n ] A) 〈 a 〉)
                  (unit-[ n ] , pr2 (desup-[ n ] B) 〈 b 〉))
                fa=gb))

      trunc-map-f-g : trunc-map (minus-one n) (El-[ n ] A + El-[ n ] B) (V-[ n ] i)
      pr1 trunc-map-f-g = ind-coprod (λ _ → V-[ n ] i) (pr1 f) (pr1 g)
      pr2 trunc-map-f-g x =
        is-trunc-equiv _
          (fiber (pr1 f) x + fiber (pr1 g) x)
          (right-distributive-Σ-coprod _ _ _)
          (ind-ℕ
            {P = λ k → is-trunc-map (minus-one k) (pr1 f)
               → is-trunc-map (minus-one k) (pr1 g)
               → is-trunc (minus-one k) (fiber (pr1 f) x + fiber (pr1 g) x)}
            (λ H K →
              is-prop-coprod
                (λ (a , p) (b , q) → no-overlap a b (p ∙ inv q))
                (H x)
                (K x))
            (λ k _ H K →
              is-trunc-coprod (minus-two k)
                (H x)
                (K x))
            n
            (pr2 f)
            (pr2 g))

  open import fixed-point.natural-numbers n (V-[ n ] i) (desup-[ n ]-equiv)

  ℕ-[_] : V-[ n ] i
  ℕ-[_] = pr1 (natural-numbers-von-neumann is-locally-small-V)

  Id-[_] : (A : V-[ n ] i) → El-[ n ] A → El-[ n ] A → V-[ n ] i
  Id-[_] A a a' =
    sup-[ n ]
      (a == a' ,
      ((λ _ → empty-[ n ]) ,
      (λ x →
        is-trunc-Σ
          (is-trunc-El-[ n ] A a a')
          (λ _ → is-trunc-V-[ n ] empty-[ n ] x))))


{- The special case of V⁰ as a universe -}
module _ {i : Level} where

  is-set-El⁰ : (x : V⁰ i) → is-set (El⁰ x)
  is-set-El⁰ = is-trunc-El-[ zero-ℕ ]

  is-set-V⁰ : is-set (V⁰ i)
  is-set-V⁰ = is-trunc-V-[ zero-ℕ ]

V⁰-Set : ∀ i → Set (lsuc i)
pr1 (V⁰-Set i) = V⁰ i
pr2 (V⁰-Set i) = is-set-V⁰

El⁰-Set : ∀ i → V⁰ i → Set i
pr1 (El⁰-Set i x) = El⁰ x
pr2 (El⁰-Set i x) = is-set-El⁰ x

module _ {i : Level} where

  empty⁰ : V⁰ i
  empty⁰ = empty-[ zero-ℕ ]

  unit⁰ : V⁰ i
  unit⁰ = unit-[ zero-ℕ ]

  empty⁰≠unit⁰ : empty⁰ ≠ unit⁰
  empty⁰≠unit⁰ = empty≠unit-[ zero-ℕ ]

  bool⁰ : V⁰ i
  bool⁰ = bool-[ zero-ℕ ]


module _ {i : Level} where
  open import fixed-point.unordered-tupling
    0 0 0 refl (V⁰ i) (desup⁰-equiv) is-locally-small-V
  open ordered-pairs-from-k=0 refl
  open import fixed-point.exponentiation
    0 (V⁰ i) (desup⁰-equiv) ordered-pairs

  Π⁰ : (A : V⁰ i) → (El⁰ A → V⁰ i) → V⁰ i
  Π⁰ = Π-[ zero-ℕ ]

  Σ⁰ : (A : V⁰ i) → (El⁰ A → V⁰ i) → V⁰ i
  Σ⁰ = Σ-[ zero-ℕ ]

  _→⁰_ : V⁰ i → V⁰ i → V⁰ i
  A →⁰ B = Π⁰ A (λ _ → B)

  _×⁰_ : V⁰ i → V⁰ i → V⁰ i
  A ×⁰ B = Σ⁰ A (λ _ → B)

  _+⁰_ : V⁰ i → V⁰ i → V⁰ i
  A +⁰ B = coprod-[ zero-ℕ ] A B

  ℕ⁰ : V⁰ i
  ℕ⁰ = ℕ-[ zero-ℕ ]

  Id⁰ : (A : V⁰ i) → El⁰ A → El⁰ A → V⁰ i
  Id⁰ = Id-[ zero-ℕ ]

  fiber⁰ : (A B : V⁰ i) → (El⁰ A → El⁰ B) → El⁰ B → V⁰ i
  fiber⁰ A B f b = Σ⁰ A (λ a → Id⁰ B (f a) b)

  fiber'⁰ : (A B : V⁰ i) → (El⁰ A → El⁰ B) → El⁰ B → V⁰ i
  fiber'⁰ A B f b = Σ⁰ A (λ a → Id⁰ B b (f a))

  equiv-class : (A : V⁰ i) (R : El⁰ A → El⁰ A → Prop i) 
              → El⁰ A → V⁰ i
  equiv-class A R a = sup⁰ (type-subtype (R a)
                           , comp-prop-map (pr2 (desup⁰ A))
                                           (prop-map-subtype (R a)))


  equiv-class-≃
              : (A : V⁰ i)(R : El⁰ A → El⁰ A → Prop i)
              → (∀ a a' → (∀ b → ¦ R a b ¦ ≃ ¦ R a' b ¦ ) ≃ ¦ R a a' ¦)
              → ∀ a a' → (equiv-class A R a == equiv-class A R a')
                       ≃ ¦ R a a' ¦
  equiv-class-≃ A R P a a'
              = equivalence-reasoning
                     equiv-class A R a == equiv-class A R a'
                       ≃  Σ (Σ _ (¦R¦ a) ≃ Σ _ (¦R¦ a'))
                            (λ α → ∀ x → V⁰↪V∞ 〈 f 〈 pr1 x 〉 〉 ==V∞ V⁰↪V∞ 〈 f 〈 pr1 (α 〈 x 〉)〉 〉)
                                by equiv-eq-Eq-V _ _
                       ≃  (Σ (Σ _ (¦R¦ a) ≃ Σ _ (¦R¦ a')) λ α → ∀ x → pr1 x == pr1 (α 〈 x 〉))
                                by equiv-tot (λ α → equivalence-reasoning
                                                (∀ x → V⁰↪V∞ 〈 f 〈 pr1 x 〉 〉 ==V∞ V⁰↪V∞ 〈 f 〈 pr1 (α 〈 x 〉)〉 〉)
                                              ≃ (∀ x → V⁰↪V∞ 〈 f 〈 pr1 x 〉 〉 == V⁰↪V∞ 〈 f 〈 pr1 (α 〈 x 〉)〉 〉)
                                                      by equiv-Π-equiv-family (λ x → (equiv-eq-V∞ _ _)⁻¹)
                                              ≃ (∀ x → f 〈 pr1 x 〉 == f 〈 pr1 (α 〈 x 〉)〉)
                                                      by equiv-Π-equiv-family (λ x → equiv-ap-emb V⁰↪V∞ ⁻¹)
                                              ≃ (∀ x → pr1 x == pr1 (α 〈 x 〉))
                                                      by equiv-htpy-postcomp-emb (emb-prop-map f) _ _ ⁻¹)
                       ≃  (∀ b → ¦ R a b ¦ ≃ ¦ R a' b ¦)
                               by equiv-fam-equiv-equiv-tot-space _ _ ⁻¹
                       ≃  ¦ R a a' ¦
                               by P a a' where
                          f : prop-map (El⁰ A ) (V⁰ i)
                          f = pr2 (desup⁰ A)
                          ¦R¦ : El⁰ A → El⁰ A → UU i
                          ¦R¦ a b = ¦ R a b ¦

  equiv-class' : (A : V⁰ i) (R : El⁰ A → El⁰ A → UU i) 
              → El⁰ A → V⁰ i
  equiv-class' A R a = equiv-class A ∥ R ∥ a

  _/⁰_ : (A : V⁰ i) (R : El⁰ A → El⁰ A → UU i) → V⁰ i
  A /⁰ R = sup⁰ (set-quotient R , ( f
                                , is-prop-map-is-emb (is-emb-is-injective (is-set-V⁰)
                                                                          (λ {a₀} {a₁} → injective-f a₀ a₁)))) where
             f : set-quotient R → V⁰ i
             f = set-quotient-rec (R)
                                  (is-set-V⁰)
                                  (equiv-class A ∥ R ∥)
                                  (λ {a₀} {a₁} → (equiv-class-≃ A ∥ R ∥ (λ a a' → ∥ R ∥-equivalence-class-representation _ _) a₀ a₁ ⁻¹) 〈_〉 ∘ quot-rel)
             injective-f : ∀ a₀ a₁ → f a₀ == f a₁ → a₀ == a₁
             injective-f a₀ a₁ = set-quotient-prop-elim (R)
                                                        {P = λ a₀ → ∀ a₁ → f a₀ == f a₁ → a₀ == a₁}
                                                        (λ _ → is-prop-Π (λ _ → is-prop-function-type (set-quotient-is-set _ _)))
                                                        (λ a₀' → set-quotient-prop-elim (R)
                                                                                        {P = λ a₁ → _ → q[ a₀' ] == a₁}
                                                                                        (λ _ → is-prop-function-type (set-quotient-is-set _ _))
                                                        (λ a₁' → equiv-class-≃ A ∥ R ∥ (λ a a' → ∥ R ∥-equivalence-class-representation _ _) a₀' a₁' 〈_〉)) a₀ a₁



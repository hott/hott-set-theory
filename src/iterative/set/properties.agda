{-# OPTIONS --without-K --rewriting #-}

open import elementary-number-theory.addition-natural-numbers
open import elementary-number-theory.inequality-natural-numbers
open import elementary-number-theory.natural-numbers
open import elementary-number-theory.strict-inequality-natural-numbers

open import foundation.dependent-pair-types
open import foundation.identity-types
open import foundation.transport-along-identifications
open import foundation.truncated-maps
open import foundation.truncated-types
open import foundation.truncation-levels
  renaming (truncation-level-ℕ to to-𝕋;
    truncation-level-minus-one-ℕ to minus-one)
open import foundation.universe-levels

open import order-theory.accessible-elements-relations

open import e-structure.core
open import iterative.set
open import notation

module iterative.set.properties (i : Level) (n : ℕ)  where

-- Since Vⁿ is a fixpoint for Pⁿ, it has an ∈-structure
open import fixed-point.core n (V-[ n ] i) desup-[ n ]-equiv

-- The following properties hold for Vⁿ

-- Replacement
open import e-structure.property.replacement ∈-structure-V

replacement : (k : ℕ) → k ≤-ℕ n → [ k ]-Replacement
replacement k k≤n = n-replacement
  where
    H : Σ ℕ λ l → add-ℕ l k == n
    H = subtraction-leq-ℕ k n k≤n

    open import fixed-point.replacement
      n k (pr1 H) (inv (pr2 H)) (V-[ n ] i)
      desup-[ n ]-equiv is-[succ-ℕ k ]-small-V


-- Restricted separation
open import e-structure.property.restricted-separation ∈-structure-V

restricted-separation : Restricted[ n ]-Separation i
restricted-separation = restricted-n-separation
  where
    open import fixed-point.restricted-separation
      n (V-[ n ] i) desup-[ n ]-equiv

-- Union
open import e-structure.property.union ∈-structure-V

union : (k : ℕ) → k ≤-ℕ n → [ k ]-Union
union k k≤n = n-union
  where
    H : Σ ℕ λ l → add-ℕ l k == n
    H = subtraction-leq-ℕ k n k≤n

    open import fixed-point.union
      n k (pr1 H) (inv (pr2 H)) (V-[ n ] i)
      desup-[ n ]-equiv is-[succ-ℕ k ]-small-V

-- Unordered tupling
open import e-structure.property.unordered-tupling ∈-structure-V

unordered-tupling : (k : ℕ) → k ≤-ℕ n
                  → (I : UU i)
                  → [ k ]-UnorderedTupling I
unordered-tupling k k≤n = n-unordered-tupling
  where
    H : Σ ℕ λ l → add-ℕ l k == n
    H = subtraction-leq-ℕ k n k≤n

    open import fixed-point.unordered-tupling
      n k (pr1 H) (inv (pr2 H)) (V-[ n ] i)
      desup-[ n ]-equiv is-[succ-ℕ k ]-small-V

∞-unordered-tupling : (k : ℕ) → k <-ℕ n
                    → (I : Truncated-Type i (to-𝕋 k))
                    → ∞-UnorderedTupling (type-Truncated-Type I)
∞-unordered-tupling = ∞-unordered-tupling'
  where
    open import fixed-point.unordered-tupling
      n n 0 (commutative-add-ℕ n 0) (V-[ n ] i)
      desup-[ n ]-equiv is-[succ-ℕ n ]-small-V
    open ∞-unordered-tupling-from-k=0 refl
      renaming (∞-unordered-tupling to ∞-unordered-tupling')


-- Exponentiation
open ordered-pairing-structure (V-[ n ] i)

-- We can construct ordered pairs
ordered-pairs : OrderedPairs
ordered-pairs = ordered-pairs'
  where
    open import fixed-point.unordered-tupling
      n n 0 (commutative-add-ℕ n 0) (V-[ n ] i)
      desup-[ n ]-equiv is-[succ-ℕ n ]-small-V
    open ordered-pairs-from-k=0 refl
      renaming (ordered-pairs to ordered-pairs')

open import e-structure.property.exponentiation ∈-structure-V

exponentiation : (ordered-pairs' : OrderedPairs) → Exponentiation ordered-pairs'
exponentiation ordered-pairs' = exponentiation''
  where
    open import fixed-point.exponentiation
      n (V-[ n ] i) desup-[ n ]-equiv ordered-pairs'
      renaming (exponentiation to exponentiation'')


-- Natural numbers with respect to any truncated representation.
-- in particular with respect to the von Neumann encoding
open import e-structure.property.natural-numbers ∈-structure-V
open import e-structure.internalisations ∈-structure-V

natural-numbers-trunc-repr : (f : Representation ℕ)
                           → is-trunc-map (minus-one n) f
                           → NaturalNumbersRepresentedBy f
natural-numbers-trunc-repr = natural-numbers-trunc-repr'
  where
    open import fixed-point.natural-numbers
      n (V-[ n ] i) desup-[ n ]-equiv
      renaming (natural-numbers-trunc-repr to natural-numbers-trunc-repr')

open import fixed-point.empty-set n (V-[ n ] i) desup-[ n ]-equiv
open import fixed-point.unordered-tupling
  n zero-ℕ n refl (V-[ n ] i) desup-[ n ]-equiv is-locally-small-V
open import fixed-point.union
  n zero-ℕ n refl (V-[ n ] i) desup-[ n ]-equiv is-locally-small-V
open von-neumann empty-set singletons binary-union

natural-numbers-von-neumann : NaturalNumbersRepresentedBy von-neumann-repr
natural-numbers-von-neumann = natural-numbers-von-neumann' is-locally-small-V
  where
    open import fixed-point.natural-numbers
      n (V-[ n ] i) desup-[ n ]-equiv
      renaming (natural-numbers-von-neumann to natural-numbers-von-neumann')


-- Moreover, Vⁿ satisfies Foundation
open import e-structure.property.foundation ∈-structure-V

foundation : Foundation
foundation =
  V-[ n ]-elim
    (is-accessible-element-Relation _∈_)
    (λ A f p → access (λ (a , fa=y) → tr (is-accessible-element-Relation _∈_) fa=y (p a)))

{-# OPTIONS --without-K --lossy-unification #-}

open import elementary-number-theory.natural-numbers

open import foundation.action-on-identifications-functions
open import foundation.contractible-types
open import foundation.dependent-pair-types
open import foundation.embeddings
open import foundation.equivalences
open import foundation.fibers-of-maps
open import foundation.function-types
open import foundation.function-extensionality
open import foundation.functoriality-dependent-function-types
open import foundation.functoriality-dependent-pair-types
open import foundation.functoriality-truncation
open import foundation.homotopies
open import foundation.identity-types
open import foundation.locally-small-types
open import foundation.slice
open import foundation.small-types
open import foundation.structure-identity-principle
open import foundation.subtypes
open import foundation.truncated-maps
open import foundation.truncated-types
open import foundation.truncations
open import foundation.truncation-images-of-maps
open import foundation.truncation-levels
  renaming (truncation-level-minus-one-ℕ to minus-one;
    truncation-level-ℕ to to-𝕋)
open import foundation.type-arithmetic-dependent-pair-types
open import foundation.univalence
open import foundation.universe-levels
open import foundation.whiskering-homotopies

open import functor.slice
open import image-factorisation
open import notation

module functor.n-slice where

P-[_] : (n : ℕ) (i : Level) {j : Level} → UU j → UU (lsuc i ⊔ j)
P-[ n ] i X = Σ (UU i) (λ A → trunc-map (minus-one n) A X)

-- Some special cases
P⁰ : (i : Level) {j : Level} → UU j → UU (lsuc i ⊔ j)
P⁰ = P-[ 0 ]

P¹ : (i : Level) {j : Level} → UU j → UU (lsuc i ⊔ j)
P¹ = P-[ 1 ]

module _ {i j} {A : UU j} where

  Eq-P : (n : ℕ) → P-[ n ] i A → P-[ n ] i A → UU (i ⊔ j)
  Eq-P n (X , f) (Y , g) =
    Σ (X ≃ Y) (λ e → map-trunc-map f ~ (map-trunc-map g ∘ map-equiv e))

  syntax Eq-P n s t = s ==P-[ n ] t

  equiv-eq-P-[_] : (n : ℕ) (s t : P-[ n ] i A) → s == t ≃ (s ==P-[ n ] t)
  equiv-eq-P-[ n ] (X , f) =
    extensionality-Σ
      (λ g e → map-trunc-map f ~ (map-trunc-map g ∘ map-equiv e))
      (id-equiv)
      (refl-htpy)
      (λ Y → equiv-univalence)
      (λ f' → equiv-funext ∘e extensionality-type-subtype' (is-trunc-map-Prop (minus-one n)) f f')

  Eq'-P : (n : ℕ) → P-[ n ] i A → P-[ n ] i A → UU (i ⊔ j)
  Eq'-P n (X , f) (Y , g) =
    (z : A) → fiber (map-trunc-map f) z ≃ fiber (map-trunc-map g) z

  syntax Eq'-P n s t = s ==P-[ n ]' t

  equiv-eq-P-[_]' : (n : ℕ) (s t : P-[ n ] i A) → s == t ≃ (s ==P-[ n ]' t)
  equiv-eq-P-[ n ]' (X , f) (Y , g) =
    equiv-fam-equiv-equiv-slice (map-trunc-map f) (map-trunc-map g)
    ∘e equiv-eq-P-[ n ] (X , f) (Y , g)

  is-trunc-P-[_] : (n : ℕ) → is-trunc (to-𝕋 n) (P-[ n ] i A)
  is-trunc-P-[ n ] (X , f) (Y , g) =
    is-trunc-equiv (minus-one n) _
      (equiv-eq-P-[ n ]' (X , f) (Y , g))
      (is-trunc-Π _ (λ z → is-trunc-equiv-is-trunc _ (pr2 f z) (pr2 g z)))

is-[succ-ℕ]-small-P-[_] : (n : ℕ) {i j : Level} {X : UU j}
                        → is-[ succ-ℕ n ]-small i X
                        → is-[ succ-ℕ n ]-small i (P-[ n ] i X)
is-[succ-ℕ]-small-P-[ n ] p (A , f) (B , g) =
  is-[ n ]-small-equiv
    (inv-equiv (equiv-eq-P-[ n ] (A , f) (B , g)))
    (is-[ n ]-small-Σ
      (is-[ n ]-small-is-small is-small')
      (λ e →
        is-[ n ]-small-equiv
          (inv-equiv equiv-eq-htpy)
          (is-[succ-ℕ n ]-small-Π
            is-small'
            (λ x → p)
            (map-trunc-map f)
            (map-trunc-map g ∘ map-equiv e))))

P-[_]↪P∞ : (n : ℕ)
         → {i j : Level}
         → (X : UU j)
         → P-[ n ] i X ↪ P∞ i X
P-[ n ]↪P∞ X =
  comp-emb
    (emb-subtype (λ s → is-trunc-map-Prop (minus-one n) (pr2 s)))
    (emb-equiv (inv-associative-Σ _ _ _))

P-[_]-to-P∞ : (n : ℕ)
            → {i j : Level}
            → (X : UU j)
            → P-[ n ] i X → P∞ i X
P-[ n ]-to-P∞ X = map-emb (P-[ n ]↪P∞ X)

module _ (n : ℕ) {i j k : Level}
  {X : UU j} {Y : UU k}
  (p : is-[ succ-ℕ n ]-small i Y)
  where

  map-P-[_] : (X → Y) → (P-[ n ] i X → P-[ n ] i Y)
  pr1 (map-P-[_] f s) = trunc-Image p (f ∘ map-trunc-map (s ↓))
  pr2 (map-P-[_] f s) = trunc-image-inclusion p (f ∘ map-trunc-map (s ↓))

  map-P-[_]-~ : (φ ψ : X → Y)
              → (φ ~ ψ)
              → map-P-[_] φ ~ map-P-[_] ψ
  map-P-[_]-~ φ ψ σ (A , f) =
    ap (λ h → map-P-[_] h (A , f)) (eq-htpy σ)

  map-P-[_]-~-refl : (φ : X → Y)
                   → map-P-[_]-~ φ φ refl-htpy ~ refl-htpy
  map-P-[_]-~-refl φ (A , f) =
    ap (ap (λ h → map-P-[_] h (A , f))) (eq-htpy-refl-htpy φ)

  map-P-[_]-map-P-comm-is-trunc-map-~ : {f : X → Y}
                                      → is-trunc-map (minus-one n) f
                                      → (map-P∞ f ∘ P-[ n ]-to-P∞ X)
                                      ~ (map-emb (P-[ n ]↪P∞ Y) ∘ map-P-[_] f)
  map-P-[_]-map-P-comm-is-trunc-map-~ {f} is-trunc-map-f (A , g) =
    inv (eq-equiv-slice
      (trunc-Image p (f ∘ map-trunc-map g) ,
       map-trunc-map (trunc-image-inclusion p (f ∘ map-trunc-map g)))
      (A , f ∘ map-trunc-map g)
      (equiv-total-fiber (f ∘ map-trunc-map g) ∘e equiv-2 ∘e inv-equiv equiv-1 ,
      (λ s → inv (pr2 (pr2 (map-equiv equiv-2 (map-inv-equiv equiv-1 s)))))))
    where
      equiv-1 : trunc-im (minus-one n) (f ∘ map-trunc-map g) ≃ trunc-Image p (f ∘ pr1 g)
      equiv-1 = equiv-trunc-im-trunc-Image p (f ∘ map-trunc-map g)

      equiv-2 : trunc-im (minus-one n) (f ∘ map-trunc-map g) ≃ Σ Y (fiber (f ∘ map-trunc-map g))
      equiv-2 =
        equiv-tot (λ y →
          inv-equiv (equiv-unit-trunc
            (fiber (f ∘ map-trunc-map g) y ,
            is-trunc-map-comp _ f (map-trunc-map g) is-trunc-map-f (is-trunc-map-map-trunc-map g) y)))

  map-P-[_]-map-P-comm-trunc-map-~ : (f : trunc-map (minus-one n) X Y)
                                   → (map-P∞ (map-trunc-map f) ∘ P-[ n ]-to-P∞ X)
                                   ~ (P-[ n ]-to-P∞ Y ∘ map-P-[_] (map-trunc-map f))
  map-P-[_]-map-P-comm-trunc-map-~ f =
    map-P-[_]-map-P-comm-is-trunc-map-~ (is-trunc-map-map-trunc-map f)

-- Functoriality of map-P-[_]
map-P-[_]-id : (n : ℕ)
             → ∀ {i j} {X : UU j}
             → (p : is-[ succ-ℕ n ]-small i X)
             → map-P-[ n ] p id ~ id
map-P-[ n ]-id p (A , f) =
  map-inv-equiv
   (equiv-eq-P-[ n ]'
     (trunc-Image p (map-trunc-map f) , trunc-image-inclusion p (map-trunc-map f))
     (A , f))
   (λ z →
     inv-equiv (equiv-unit-trunc (fiber (map-trunc-map f) z , is-trunc-map-map-trunc-map f z)) ∘e
     equiv-fiber-trunc-Image-im p (map-trunc-map f) z)

map-P-[_]-comp : (n : ℕ)
               → ∀ {i j k l} {X : UU j} {Y : UU k} {Z : UU l}
               → (p : is-[ succ-ℕ n ]-small i Y)
               → (q : is-[ succ-ℕ n ]-small i Z)
               → (g : Y → Z) (f : X → Y)
               → map-P-[ n ] q (g ∘ f) ~ map-P-[ n ] q g ∘ map-P-[ n ] p f
map-P-[ n ]-comp p q g f (A , h) =
  map-inv-equiv
    (equiv-eq-P-[ n ]'
      (trunc-Image q (g ∘ f ∘ map-trunc-map h) , trunc-image-inclusion q (g ∘ f ∘ map-trunc-map h))
      (trunc-Image q (g ∘ map-trunc-map (trunc-image-inclusion p (f ∘ map-trunc-map h))) ,
      trunc-image-inclusion q (g ∘ map-trunc-map (trunc-image-inclusion p (f ∘ map-trunc-map h)))))
    (λ z →
      inv-equiv (equiv-fiber-trunc-Image-im q (g ∘ map-trunc-map (trunc-image-inclusion p (f ∘ map-trunc-map h))) z) ∘e
      equiv-trunc (minus-one n)
        (inv-compute-fiber-comp g (map-trunc-map (trunc-image-inclusion p (f ∘ map-trunc-map h))) z) ∘e
      equiv-trunc (minus-one n) (equiv-tot (λ t → inv-equiv (equiv-fiber-trunc-Image-im p (f ∘ map-trunc-map h) (pr1 t)))) ∘e
      equiv-trunc-Σ (minus-one n) ∘e
      equiv-trunc (minus-one n) (compute-fiber-comp g (f ∘ map-trunc-map h) z) ∘e
      equiv-fiber-trunc-Image-im q (g ∘ f ∘ map-trunc-map h) z)

-- Some special cases
map-P⁰ : ∀ {i j k}
       → {A : UU j}
       → {B : UU k}
       → is-locally-small i B
       → (A → B)
       → (P⁰ i A → P⁰ i B)
map-P⁰ = map-P-[ 0 ]

map-P¹ : ∀ {i j k}
       → {A : UU j}
       → {B : UU k}
       → is-[ 2 ]-small i B
       → (A → B)
       → (P¹ i A → P¹ i B)
map-P¹ = map-P-[ 1 ]

-- P-[ n ]-coalgebras
P-[_]-Coalg : ℕ → (i j :  Level) → UU (lsuc i ⊔ lsuc j)
P-[ n ]-Coalg i j = Σ (UU j) (λ X → X → P-[ n ] i X)

-- P-[ n ]-coalgebra homomorphisms
module _ (n : ℕ) {i j j' : Level}
  (X : P-[ n ]-Coalg i j) (Y : P-[ n ]-Coalg i j')
  (p : is-[ succ-ℕ n ]-small i ¦ Y ¦)
  where

  is-hom-P-[_]-Coalg : (¦ X ¦ → ¦ Y ¦)
                     → UU (lsuc i ⊔ j ⊔ j')
  is-hom-P-[_]-Coalg f = ((Y ↓) ∘ f) ~ (map-P-[ n ] p f ∘ (X ↓))

  hom-P-[_]-Coalg : UU (lsuc i ⊔ j ⊔ j')
  hom-P-[_]-Coalg = Σ (¦ X ¦ → ¦ Y ¦) is-hom-P-[_]-Coalg

id-hom-P-[_]-Coalg : (n : ℕ) {i j : Level}
                   → ((X , m) : P-[ n ]-Coalg i j)
                   → (p : is-[ succ-ℕ n ]-small i X)
                   → hom-P-[ n ]-Coalg (X , m) (X , m) p
pr1 (id-hom-P-[ n ]-Coalg (X , m) p) = id
pr2 (id-hom-P-[ n ]-Coalg (X , m) p) =
  inv-htpy (map-P-[ n ]-id p) ·r m

comp-hom-P-[_]-Coalg : (n : ℕ) {i j k l : Level}
                     → ((X , m) : P-[ n ]-Coalg i j)
                     → ((Y , m') : P-[ n ]-Coalg i k)
                     → ((Z , m'') : P-[ n ]-Coalg i l)
                     → (p : is-[ succ-ℕ n ]-small i Y)
                     → (q : is-[ succ-ℕ n ]-small i Z)
                     → hom-P-[ n ]-Coalg (Y , m') (Z , m'') q
                     → hom-P-[ n ]-Coalg (X , m) (Y , m') p
                     → hom-P-[ n ]-Coalg (X , m) (Z , m'') q
pr1 (comp-hom-P-[ n ]-Coalg (X , m) (Y , m') (Z , m'') p q (g , β) (f , α)) = g ∘ f
pr2 (comp-hom-P-[ n ]-Coalg (X , m) (Y , m') (Z , m'') p q (g , β) (f , α)) =
  (β ·r f) ∙h
  (map-P-[ n ] q g ·l α) ∙h
  (inv-htpy (map-P-[ n ]-comp p q g f) ·r m)

-- Some special cases
P⁰-Coalg : ∀ i j → UU (lsuc i ⊔ lsuc j)
P⁰-Coalg = P-[ 0 ]-Coalg

P¹-Coalg : ∀ i j → UU (lsuc i ⊔ lsuc j)
P¹-Coalg = P-[ 1 ]-Coalg

-- Any P-[ n ]-coalgebra is a P∞-coalgebra
P-[_]-to-P∞-Coalg : (n : ℕ)
                  → {i j : Level}
                  → P-[ n ]-Coalg i j
                  → P∞-Coalg i j
pr1 (P-[ n ]-to-P∞-Coalg X) = ¦ X ¦
pr2 (P-[ n ]-to-P∞-Coalg X) =
  map-emb (P-[ n ]↪P∞ ¦ X ¦) ∘ (X ↓)

-- Terminal P-[ n ]-coalgebras
is-terminal-P-[_]-Coalg : (n : ℕ) {i j : Level}
                        → ((X , m) : P-[ n ]-Coalg i j)
                        → is-[ succ-ℕ n ]-small i X
                        → (j' : Level) → UU (lsuc i ⊔ j ⊔ lsuc j')
is-terminal-P-[ n ]-Coalg {i = i} (X , m) p j' =
  ((X' , m') : P-[ n ]-Coalg i j') →
    is-contr (hom-P-[ n ]-Coalg (X' , m') (X , m) p)

is-equiv-coalg-map-is-terminal-P-[_]-Coalg : (n : ℕ) {i : Level}
                                           → ((X , m) : P-[ n ]-Coalg i (lsuc i))
                                           → (p : is-[ succ-ℕ n ]-small i X)
                                           → is-terminal-P-[ n ]-Coalg (X , m) p (lsuc i)
                                           → is-equiv m
is-equiv-coalg-map-is-terminal-P-[ n ]-Coalg {i = i} (X , m) p X-is-terminal =
  is-equiv-is-invertible f is-sec-f is-retr-f
  where
    q : is-[ succ-ℕ n ]-small i (P-[ n ] i X)
    q = is-[succ-ℕ]-small-P-[ n ] p

    f : P-[ n ] i X → X
    f = pr1 (center (X-is-terminal (P-[ n ] i X , map-P-[ n ] q m)))

    is-hom-f : is-hom-P-[ n ]-Coalg (P-[ n ] i X , map-P-[ n ] q m) (X , m) p f
    is-hom-f = pr2 (center (X-is-terminal (P-[ n ] i X , map-P-[ n ] q m)))

    is-retr-f : f ∘ m ~ id
    is-retr-f =
      htpy-eq
        (ap pr1
          (eq-is-contr'
            (X-is-terminal (X , m))
            (comp-hom-P-[ n ]-Coalg
              (X , m) (P-[ n ] i X , map-P-[ n ] q m) (X , m) q p
              (f , is-hom-f)
              (m , refl-htpy))
            (id-hom-P-[ n ]-Coalg (X , m) p)))

    is-sec-f : m ∘ f ~ id
    is-sec-f x =
      equational-reasoning
        m (f x)
        ＝ map-P-[ n ] p f (map-P-[ n ] q m x) by is-hom-f x
        ＝ map-P-[ n ] p (f ∘ m) x             by inv (map-P-[ n ]-comp q p f m x)
        ＝ map-P-[ n ] p id x                  by ap (λ h → map-P-[ n ] p h x) (eq-htpy is-retr-f)
        ＝ x                                   by map-P-[ n ]-id p x

equiv-terminal-P-[_]-Coalg : (n : ℕ) {i : Level}
                           → ((X , m) : P-[ n ]-Coalg i (lsuc i))
                           → (p : is-[ succ-ℕ n ]-small i X)
                           → is-terminal-P-[ n ]-Coalg (X , m) p (lsuc i)
                           → X ≃ P-[ n ] i X
pr1 (equiv-terminal-P-[ n ]-Coalg (X , m) p X-is-terminal) = m
pr2 (equiv-terminal-P-[ n ]-Coalg (X , m) p X-is-terminal) =
  is-equiv-coalg-map-is-terminal-P-[ n ]-Coalg (X , m) p X-is-terminal

-- P-[ n ]-algebras
P-[_]-Alg : ℕ → (i j : Level) → UU (lsuc i ⊔ lsuc j)
P-[ n ]-Alg i j = Σ (UU j) (λ X → P-[ n ] i X → X)

-- Some special cases
P⁰-Alg : ∀ i j → UU (lsuc i ⊔ lsuc j)
P⁰-Alg = P-[ 0 ]-Alg

P¹-Alg : ∀ i j → UU (lsuc i ⊔ lsuc j)
P¹-Alg = P-[ 1 ]-Alg

-- P-[ n ]-algebra homomorphisms
module _ (n : ℕ) {i j j' : Level}
  (X : P-[ n ]-Alg i j) (Y : P-[ n ]-Alg i j')
  (p : is-[ succ-ℕ n ]-small i ¦ Y ¦)
  where

  is-hom-P-[_]-Alg : (¦ X ¦ → ¦ Y ¦)
                   → UU (lsuc i ⊔ j ⊔ j')
  is-hom-P-[_]-Alg f = (f ∘ X ↓) ~ (Y ↓ ∘ map-P-[ n ] p f)

  hom-P-[_]-Alg : UU (lsuc i ⊔ j ⊔ j')
  hom-P-[_]-Alg = Σ (¦ X ¦ → ¦ Y ¦) is-hom-P-[_]-Alg

-- Equality of P-[ n ]-algebra homomorphisms
hom-P-[_]-Alg-Eq : (n : ℕ)
                 → {i j j' : Level}
                 → (X : P-[ n ]-Alg i j) (Y : P-[ n ]-Alg i j')
                 → (p : is-[ succ-ℕ n ]-small i ¦ Y ¦)
                 → hom-P-[ n ]-Alg X Y p
                 → hom-P-[ n ]-Alg X Y p
                 → UU (lsuc i ⊔ j ⊔ j')
hom-P-[ n ]-Alg-Eq X Y p (φ , α) (ψ , β) =
  Σ (φ ~ ψ) λ σ →
    (α ∙h ((Y ↓) ·l map-P-[ n ]-~ p φ ψ σ)) ~
    ((σ ·r (X ↓)) ∙h β)

equiv-hom-P-[_]-Alg-Eq : (n : ℕ)
                       → {i j j' : Level}
                       → {X : P-[ n ]-Alg i j} {Y : P-[ n ]-Alg i j'}
                       → (p : is-[ succ-ℕ n ]-small i ¦ Y ¦)
                       → (φ ψ : hom-P-[ n ]-Alg X Y p)
                       → (φ == ψ)
                       ≃ hom-P-[ n ]-Alg-Eq X Y p φ ψ
equiv-hom-P-[ n ]-Alg-Eq {X = X} {Y = Y} p (φ , α) =
  extensionality-Σ
    (λ {ψ} β σ → (α ∙h ((Y ↓) ·l map-P-[ n ]-~ p φ ψ σ)) ~ ((σ ·r (X ↓)) ∙h β))
    (refl-htpy)
    (λ (A , f) → H A f)
    (λ _ → equiv-funext)
    (λ α' →
      equiv-Π-equiv-family (λ (A , f) → equiv-concat (H A f) (α' (A , f))) ∘e
      equiv-funext)
  where
    H : ∀ A f
      → (α ∙h ((Y ↓) ·l map-P-[ n ]-~ p φ φ refl-htpy)) (A , f) == α (A , f)
    H A f =
      ap (λ q → α (A , f) ∙ ap (Y ↓) q) (map-P-[ n ]-~-refl p φ (A , f)) ∙
      right-unit

hom-P-[_]-Alg-eq : (n : ℕ)
                 → {i j j' : Level}
                 → {X : P-[ n ]-Alg i j} {Y : P-[ n ]-Alg i j'}
                 → (p : is-[ succ-ℕ n ]-small i ¦ Y ¦)
                 → (φ ψ : hom-P-[ n ]-Alg X Y p)
                 → hom-P-[ n ]-Alg-Eq X Y p φ ψ
                 → φ == ψ
hom-P-[ n ]-Alg-eq p φ ψ = map-inv-equiv (equiv-hom-P-[ n ]-Alg-Eq p φ ψ)

{-# OPTIONS --without-K #-}

open import foundation.action-on-identifications-functions
open import foundation.cartesian-product-types
open import foundation.contractible-types
open import foundation.dependent-pair-types
  renaming (ind-Σ to uncurry; ev-pair to curry)
open import foundation.embeddings
open import foundation.equality-cartesian-product-types
open import foundation.equivalences
open import foundation.fibers-of-maps
open import foundation.function-extensionality
open import foundation.function-types
open import foundation.functoriality-dependent-function-types
open import foundation.functoriality-dependent-pair-types
open import foundation.homotopies
open import foundation.identity-types
open import foundation.monomorphisms
open import foundation.postcomposition-functions
open import foundation.raising-universe-levels
open import foundation.slice
open import foundation.structure-identity-principle
open import foundation.subtypes
open import foundation.transport-along-identifications
open import foundation.univalence
open import foundation.universal-property-equivalences
open import foundation.universe-levels
open import foundation.whiskering-homotopies

open import container
open import container.coalgebra
open import container.indexed
open import container.indexed.coalgebra
open import notation

module functor.slice where

private
  variable
    i j j' k k' l l' : Level

P∞ : ∀ i {j} → UU j → UU (lsuc i ⊔ j)
P∞ i = ⟦ UU i ◁ id ⟧

module _ {A : UU j} where

  infix 100 _==P∞_

  _==P∞_ : P∞ i A → P∞ i A → UU (i ⊔ j)
  (X , f) ==P∞ (Y , g)  =
    Σ (X ≃ Y) (λ e → f ~ (g ∘ (map-equiv e)))

  equiv-eq-P∞ : (s t : P∞ i A) → s == t ≃ s ==P∞ t
  equiv-eq-P∞ (X , f) =
    extensionality-Σ
      (λ g e → f ~ (g ∘ (map-equiv e)))
      (id-equiv)
      (refl-htpy)
      (λ Y → equiv-univalence)
      (λ f' → equiv-funext)

  _==P∞'_ : P∞ i A → P∞ i A → UU (i ⊔ j)
  (X , f) ==P∞' (Y , g)  =
    (z : A) → fiber f z ≃ fiber g z

  equiv-eq-P∞' : (s t : P∞ i A) → s == t ≃ s ==P∞' t
  equiv-eq-P∞' (X , f) (Y , g) =
    equiv-fam-equiv-equiv-slice f g
    ∘e equiv-eq-P∞ (X , f) (Y , g)

P∞-Coalg : ∀ i j → UU (lsuc j ⊔ lsuc i)
P∞-Coalg i = ⟦ UU i ◁ id ⟧-Coalg

module _ {i} {A : UU j} {B : UU k} where

  map-P∞ : (A → B) → (P∞ i A → P∞ i B)
  map-P∞ = map-⟦ UU i ◁ id ⟧

  is-emb-map-P∞ : {f : A → B}
               → is-emb f
               → is-emb (map-P∞ f)
  is-emb-map-P∞ = is-emb-map-⟦ UU i ◁ id ⟧

  emb-map-P∞ : (A ↪ B) → (P∞ i A ↪ P∞ i B)
  emb-map-P∞ = emb-⟦ UU i ◁ id ⟧

module _ ((X , m) : P∞-Coalg i j) ((Y , n) : P∞-Coalg i j') where

  is-hom-P∞-Coalg : (X → Y)
                 → UU (lsuc i ⊔ j ⊔ j')
  is-hom-P∞-Coalg = is-hom-⟦ UU i ◁ id ⟧-Coalg (X , m) (Y , n)

  hom-P∞-Coalg : UU (lsuc i ⊔ (j ⊔ j'))
  hom-P∞-Coalg = hom-⟦ UU i ◁ id ⟧-Coalg (X , m) (Y , n)


emb-raise-P∞ : ∀ {i} j {j'} (X : UU j') → P∞ i X ↪ P∞ (i ⊔ j) X
emb-raise-P∞ {i = i} j =
  emb-⟦ UU i ◁ id ⟧-⟦ UU (i ⊔ j) ◁ id ⟧
    (emb-raise j)
    (λ A → inv-equiv (compute-raise j A))

raise-P∞ : ∀ {i} j {j'} (X : UU j') → P∞ i X → P∞ (i ⊔ j) X
raise-P∞ j X = map-emb (emb-raise-P∞ j X)

-- Results about bisimulations on P∞-coalgebras
E' : ((X , m) : P∞-Coalg i j)
   → IndexedContainer i i (X × X)
pr1 (E' (X , m)) (x , y) = pr1 (m x) ≃ pr1 (m y)
pr1 (pr2 (E' (X , m))) {(x , y)} _ = pr1 (m x)
pr2 (pr2 (E' (X , m))) {(x , y)} {e} a =
  (pr2 (m x) a , pr2 (m y) (map-equiv e a))

eq-⦉E'⦊-== : ((X , m) : P∞-Coalg i j) ((x , y) : X × X)
           → ((g , α) (h , β) : ⦉ E' (X , m) ⦊ (uncurry _==_) (x , y))
           → ((g , α) == (h , β))
           ≃ Σ (map-equiv g ~ map-equiv h) λ H →
               (a : pr1 (m x)) → α a == (β a ∙ inv (ap (pr2 (m y)) (H a)))
eq-⦉E'⦊-== (X , m) (x , y) (g , α) =
  extensionality-Σ
    (λ β H → (a : pr1 (m x)) → α a == (β a ∙ inv (ap (pr2 (m y)) (H a))))
    refl-htpy
    (λ a → inv right-unit)
    (λ h → equiv-funext ∘e extensionality-type-subtype' is-equiv-Prop g h)
    (λ β → equiv-Π-equiv-family (λ a → equiv-concat' (α a) (inv right-unit)) ∘e
           equiv-funext)

==-E'-Coalg : ((X , m) : P∞-Coalg i j)
            → ⦉ E' (X , m) ⦊-Coalg j
pr1 (==-E'-Coalg (X , m)) (x , y) = x == y
pr2 (==-E'-Coalg (X , m)) (x , .x) refl = (id-equiv , refl-htpy)

module _
  ((X , m) : P∞-Coalg j j')
  ((Y , n) : P∞-Coalg j k)
  ((f , α) : hom-P∞-Coalg (X , m) (Y , n))
  where

  equiv-E'-precomp-hom : (R : Y × Y → UU l) ((x , x') : X × X)
                       → ⦉ E' (Y , n) ⦊ R (f x , f x')
                       ≃ ⦉ E' (X , m) ⦊ (R ∘ ⟨ f × f ⟩) (x , x')
  equiv-E'-precomp-hom R (x , x') =
    equiv-tr
      (λ (u , v) →
        Σ (pr1 u ≃ pr1 v) (λ e →
          (a : pr1 u) → R (pr2 u a , pr2 v (map-equiv e a))))
      (eq-pair (α x) (α x'))

  is-nat-equiv-E'-precomp-hom : (R : Y × Y → UU l) (R' : Y × Y → UU l')
                              → (g : ((y , y') : Y × Y) → R (y , y') → R' (y , y'))
                              → ((x , x') : X × X)
                              → map-equiv (equiv-E'-precomp-hom R' (x , x'))
                                ∘ map-⦉ E' (Y , n) ⦊ g (f x , f x')
                              ~ map-⦉ E' (X , m) ⦊ (g ∘ ⟨ f × f ⟩) (x , x')
                                ∘ map-equiv (equiv-E'-precomp-hom R (x , x'))
  is-nat-equiv-E'-precomp-hom R R' g (x , x') (e , σ) =
    inv (preserves-tr (λ _ → tot (λ _ → g _ ∘_)) (eq-pair (α x) (α x')) (e , σ))

  is-nat-equiv-E'-precomp-hom' : (R : Y × Y → UU l) (R' : Y × Y → UU l')
                               → (g : ((y , y') : Y × Y) → R (y , y') → R' (y , y'))
                               → ((x , x') : X × X)
                               → map-⦉ E' (Y , n) ⦊ g (f x , f x')
                                 ∘ map-inv-equiv (equiv-E'-precomp-hom R (x , x'))
                               ~ map-inv-equiv (equiv-E'-precomp-hom R' (x , x'))
                                 ∘ map-⦉ E' (X , m) ⦊ (g ∘ ⟨ f × f ⟩) (x , x')
  is-nat-equiv-E'-precomp-hom' R R' g (x , x') (e , σ) =
    preserves-tr (λ _ → tot (λ _ → g _ ∘_)) (inv (eq-pair (α x) (α x'))) (e , σ)

  compute-==-equiv-E'-precomp-hom : (x : X)
                                  → map-equiv (equiv-E'-precomp-hom (uncurry _==_) (x , x))
                                      (id-equiv , refl-htpy)
                                  == (id-equiv , refl-htpy)
  compute-==-equiv-E'-precomp-hom x = tr-lemma (α x)
    where
      tr-lemma : {s t : P∞ i Y}
               → (p : s == t)
               → tr (λ (u , v) →
                       Σ (pr1 u ≃ pr1 v) (λ e →
                         (a : pr1 u) → (uncurry _==_) (pr2 u a , pr2 v (map-equiv e a))))
                  (eq-pair p p)
                  (id-equiv , refl-htpy)
               == (id-equiv , refl-htpy)
      tr-lemma refl = refl

module _
  ((X , m) : P∞-Coalg j j')
  ((Y , n) : P∞-Coalg j k)
  ((f , α) : hom-P∞-Coalg (X , m) (Y , n))
  where

  is-hom-==-E'-Coalg-ap-hom : is-hom-⦉ E' (X , m) ⦊-Coalg
                                (==-E'-Coalg (X , m))
                                ((uncurry _==_ ∘ ⟨ f × f ⟩) ,
                                 (λ (x , x') →
                                   map-equiv (equiv-E'-precomp-hom (X , m) (Y , n) (f , α) (uncurry _==_) (x , x')) ∘
                                   pr2 (==-E'-Coalg (Y , n)) (f x , f x')))
                                (λ (x , x') → ap f {x} {x'})
  is-hom-==-E'-Coalg-ap-hom (x , .x) refl =
    compute-==-equiv-E'-precomp-hom (X , m) (Y , n) (f , α) x

  hom-==-E'-Coalg-ap : hom-⦉ E' (X , m) ⦊-Coalg
                         (==-E'-Coalg (X , m))
                         ((uncurry _==_ ∘ ⟨ f × f ⟩) ,
                          (λ (x , x') →
                            map-equiv (equiv-E'-precomp-hom (X , m) (Y , n) (f , α) (uncurry _==_) (x , x')) ∘
                            pr2 (==-E'-Coalg (Y , n)) (f x , f x')))
  pr1 hom-==-E'-Coalg-ap (x , x') = ap f {x} {x'}
  pr2 hom-==-E'-Coalg-ap = is-hom-==-E'-Coalg-ap-hom

-- Mapping a bisimulation along a homomorphism
module _ {X : UU i} {Y : UU j}
  (f : X → Y) (R : X × X → UU k)
  where

  _ᶠ∘ᴿ_ : Y × Y → UU (i ⊔ j ⊔ k)
  _ᶠ∘ᴿ_ (y , y') =
    Σ (fiber f y × fiber f y')
    λ ((x , _) , (x' , _)) → R (x , x')

  to-ᶠ∘ᴿ : {(x , x') : X × X}
         → R (x , x')
         → _ᶠ∘ᴿ_ (f x , f x')
  to-ᶠ∘ᴿ {(x , x')} xRx' = (((x , refl) , (x' , refl)) , xRx')

  equiv-Π-ᶠ∘ᴿ : (P : ((y , y') : Y × Y) → _ᶠ∘ᴿ_  (y , y') → UU l')
              → (((x , x') : X × X) (r : R (x , x')) → P (f x , f x') (to-ᶠ∘ᴿ r))
              ≃ (((y , y') : Y × Y) (fr : _ᶠ∘ᴿ_ (y , y')) → P (y , y') fr)
  pr1 (equiv-Π-ᶠ∘ᴿ P) σ (.(f x) , .(f x')) (((x , refl) , (x' , refl)) , r) =
    σ (x , x') r
  pr2 (equiv-Π-ᶠ∘ᴿ P) =
    is-equiv-is-invertible
      (λ σ (x , x') → σ (f x , f x') ∘ to-ᶠ∘ᴿ)
      (λ σ →
        eq-htpy (λ (y , y') →
          eq-htpy λ { (((x , refl) , (x' , refl)) , r) → refl }))
      (λ σ → eq-htpy (λ (x , x') → eq-htpy refl-htpy))

module _
  ((X , m) : P∞-Coalg j j')
  ((Y , n) : P∞-Coalg j k)
  ((f , α) : hom-P∞-Coalg (X , m) (Y , n))
  ((R , σ) : ⦉ E' (X , m) ⦊-Coalg l)
  where

  ᶠ∘ᴿ-map : ((y , y') : Y × Y)
          → (f ᶠ∘ᴿ R) (y , y')
          → ⦉ E' (Y , n) ⦊ (f ᶠ∘ᴿ R) (y , y')
  ᶠ∘ᴿ-map (.(f x) , .(f x')) (((x , refl) , (x' , refl)) , xRx') =
    map-inv-equiv
      (equiv-E'-precomp-hom (X , m) (Y , n) (f , α) (f ᶠ∘ᴿ R) (x , x'))
      (map-⦉ E' (X , m) ⦊ (λ u → to-ᶠ∘ᴿ f R {u}) (x , x')
        (σ (x , x') xRx'))

  ᶠ∘ᴿ-E'-Coalg : ⦉ E' (Y , n) ⦊-Coalg (j' ⊔ k ⊔ l)
  pr1 ᶠ∘ᴿ-E'-Coalg = f ᶠ∘ᴿ R
  pr2 ᶠ∘ᴿ-E'-Coalg = ᶠ∘ᴿ-map

module _
  ((X , m) : P∞-Coalg j j')
  ((Y , n) : P∞-Coalg j k)
  ((f , α) : hom-P∞-Coalg (X , m) (Y , n))
  ((R , σ) : ⦉ E' (X , m) ⦊-Coalg l)
  ((S , ϕ) : ⦉ E' (Y , n) ⦊-Coalg l') where

  private
    e : (Q : Y × Y → UU k')
      → ((x , x') : X × X)
      → ⦉ E' (Y , n) ⦊ Q (f x , f x')
      ≃ ⦉ E' (X , m) ⦊ (Q ∘ ⟨ f × f ⟩) (x , x')
    e = equiv-E'-precomp-hom (X , m) (Y , n) (f , α)

  equiv-hom-ᶠ∘ᴿ : hom-⦉ E' (X , m) ⦊-Coalg
                    (R , σ)
                    (S ∘ ⟨ f × f ⟩ , λ (x , x') → map-equiv (e S (x , x')) ∘ ϕ (f x , f x'))
                ≃ hom-⦉ E' (Y , n) ⦊-Coalg
                    (f ᶠ∘ᴿ R , ᶠ∘ᴿ-map _ _ (f , α) (R , σ))
                    (S , ϕ)
  equiv-hom-ᶠ∘ᴿ =
    equivalence-reasoning
      Σ (((x , x') : X × X) → R (x , x') → S (f x , f x')) (λ g →
        ((x , x') : X × X) →
          map-equiv (e S (x , x')) ∘ ϕ (f x , f x') ∘ g (x , x')
          ~ map-⦉ E' (X , m) ⦊ g (x , x') ∘ σ (x , x'))
      ≃ Σ (((x , x') : X × X) → R (x , x') → S (f x , f x')) (λ g →
          ((x , x') : X × X) →
            ϕ (f x , f x') ∘ g (x , x')
            ~ map-inv-equiv (e S (x , x')) ∘
              map-⦉ E' (X , m) ⦊ g (x , x') ∘
              σ (x , x'))                                               by equiv-tot (λ g →
                                                                             equiv-Π-equiv-family (λ (x , x') →
                                                                               equiv-Π-equiv-family (λ r →
                                                                                 equiv-concat
                                                                                   (inv (is-retraction-map-inv-equiv (e S (x , x')) _))
                                                                                   _ ∘e
                                                                                 equiv-ap (inv-equiv (e S (x , x')))_ _)))
      ≃ Σ (((y , y') : Y × Y) → (f ᶠ∘ᴿ R) (y , y') → S (y , y')) (λ g →
          ((x , x') : X × X) →
            ϕ (f x , f x') ∘ g (f x , f x') ∘ to-ᶠ∘ᴿ f R
            ~ map-inv-equiv (e S (x , x')) ∘
              map-⦉ E' (X , m) ⦊ (g ∘ ⟨ f × f ⟩) (x , x') ∘
              map-⦉ E' (X , m) ⦊ (λ u → to-ᶠ∘ᴿ f R {u}) (x , x') ∘
              σ (x , x'))                                               by equiv-Σ-equiv-base _
                                                                             (equiv-Π-ᶠ∘ᴿ f R (λ (y , y') _ → S (y , y')))
      ≃ Σ (((y , y') : Y × Y) → (f ᶠ∘ᴿ R) (y , y') → S (y , y')) (λ g →
          ((x , x') : X × X) →
            ϕ (f x , f x') ∘ g (f x , f x') ∘ to-ᶠ∘ᴿ f R
            ~ map-⦉ E' (Y , n) ⦊ g (f x , f x') ∘
              map-inv-equiv (e (f ᶠ∘ᴿ R) (x , x')) ∘
              map-⦉ E' (X , m) ⦊ (λ u → to-ᶠ∘ᴿ f R {u}) (x , x') ∘
              σ (x , x'))                                               by equiv-tot (λ g →
                                                                             equiv-Π-equiv-family (λ (x , x') →
                                                                               equiv-concat-htpy' _
                                                                                 (inv-htpy
                                                                                   (is-nat-equiv-E'-precomp-hom'
                                                                                     (X , m) (Y , n) (f , α)
                                                                                     (f ᶠ∘ᴿ R) S g (x , x')) ·r
                                                                                 (map-⦉ E' (X , m) ⦊ (λ u → to-ᶠ∘ᴿ f R {u}) (x , x') ∘ σ (x , x')))))
      ≃ Σ (((y , y') : Y × Y) → (f ᶠ∘ᴿ R) (y , y') → S (y , y')) (λ g →
          ((x , x') : X × X) →
            ϕ (f x , f x') ∘ g (f x , f x') ∘ to-ᶠ∘ᴿ f R
            ~ map-⦉ E' (Y , n) ⦊ g (f x , f x') ∘
              ᶠ∘ᴿ-map (X , m) (Y , n) (f , α) (R , σ) (f x , f x') ∘
              to-ᶠ∘ᴿ f R)                                               by id-equiv

      ≃ Σ (((y , y') : Y × Y) → (f ᶠ∘ᴿ R) (y , y') → S (y , y')) (λ g →
          ((y , y') : Y × Y) →
            ϕ (y , y') ∘ g (y , y')
            ~ map-⦉ E' (Y , n) ⦊ g (y , y') ∘
              ᶠ∘ᴿ-map (X , m) (Y , n) (f , α) (R , σ) (y , y'))         by equiv-tot (λ g →
                                                                             equiv-Π-ᶠ∘ᴿ f R
                                                                               (λ (y , y') fr →
                                                                                 ϕ (y , y') (g (y , y') fr)
                                                                                 == map-⦉ E' (Y , n) ⦊ g (y , y')
                                                                                    (ᶠ∘ᴿ-map (X , m) (Y , n) (f , α) (R , σ) (y , y') fr)))

module _
  ((X , m) : P∞-Coalg j j')
  ((Y , n) : P∞-Coalg j k)
  ((f , α) : hom-P∞-Coalg (X , m) (Y , n))
  (H : ∀ {l} → is-terminal-⦉ E' (Y , n) ⦊-Coalg (==-E'-Coalg (Y , n)) l)
  where

  is-terminal-E'-Coalg-==-f×f : ∀ {l}
                              → is-terminal-⦉ E' (X , m) ⦊-Coalg
                                  (uncurry _==_ ∘ ⟨ f × f ⟩ ,
                                  λ (x , x') →
                                    map-equiv (equiv-E'-precomp-hom (X , m) (Y , n) (f , α) (uncurry _==_) (x , x')) ∘
                                    pr2 (==-E'-Coalg (Y , n)) (f x , f x'))
                                  l
  is-terminal-E'-Coalg-==-f×f (R , σ) =
    is-contr-equiv _
      (equiv-hom-ᶠ∘ᴿ (X , m) (Y , n) (f , α) (R , σ) (==-E'-Coalg (Y , n)))
      (H ((f ᶠ∘ᴿ R) , ᶠ∘ᴿ-map (X , m) (Y , n) (f , α) (R , σ)))

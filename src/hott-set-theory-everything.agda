{-# OPTIONS --without-K --rewriting #-}

import image-factorisation
import notation
import rewriting
import set-quotient

import coiterative.infinity-multiset

import coiterative.set
import coiterative.set.properties

import container
import container.bisimulation
import container.coalgebra
import container.indexed
import container.m-types

import container.indexed.coalgebra
import container.indexed.m-types

import e-structure.accessible-elements
import e-structure.core
import e-structure.from-P-inf-coalgebra
import e-structure.from-P-n-coalgebra
import e-structure.graphs
import e-structure.internalisations
import e-structure.u-like

import e-structure.graph.to-P-n-coalgebra

import e-structure.property.aczel-anti-foundation
import e-structure.property.exponentiation
import e-structure.property.foundation
import e-structure.property.natural-numbers
import e-structure.property.replacement
import e-structure.property.restricted-separation
import e-structure.property.scott-anti-foundation
import e-structure.property.union
import e-structure.property.unordered-tupling

import e-structure.property.aczel-anti-foundation.from-terminal-coalgebra

import fixed-point.accessible-elements
import fixed-point.core
import fixed-point.empty-set
import fixed-point.exponentiation
import fixed-point.internalisations
import fixed-point.natural-numbers
import fixed-point.replacement
import fixed-point.restricted-separation
import fixed-point.union
import fixed-point.unordered-tupling

import functor.n-slice
import functor.slice

import iterative.set

import iterative.set.category
import iterative.set.cwf-structure
import iterative.set.properties

import iterative.set.category.properties
import iterative.set.category.slice

import iterative.set.category.slice.functor
import iterative.set.category.slice.properties

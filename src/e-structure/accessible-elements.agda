{-# OPTIONS --without-K --lossy-unification #-}

open import foundation.action-on-identifications-functions
open import foundation.contractible-types
open import foundation.dependent-pair-types
open import foundation.dependent-universal-property-equivalences
open import foundation.equality-dependent-pair-types
open import foundation.equivalences
open import foundation.equivalence-extensionality
open import foundation.equivalence-induction
open import foundation.function-types
open import foundation.function-extensionality
open import foundation.functoriality-dependent-function-types
open import foundation.functoriality-dependent-pair-types
open import foundation.homotopies
open import foundation.identity-types
open import foundation.propositions
open import foundation.subtypes
open import foundation.transport-along-identifications
open import foundation.type-arithmetic-dependent-pair-types
open import foundation.universe-levels

open import order-theory.accessible-elements-relations
  renaming (is-accessible-element-Relation to Acc; access to acc;
    is-accessible-element-prop-Relation to Acc-Prop;
    is-prop-is-accessible-element-Relation to is-prop-Acc)

open import e-structure.core
open import notation

module e-structure.accessible-elements
  {i j} (((M , _∈_) , p) : ∈-structure i j) where

open extensionality ((M , _∈_) , p)

-- The subtype of accessible elements forms an ∈-structure.
_∈-Acc_ : Σ M (Acc _∈_) → Σ M (Acc _∈_) → UU j
y ∈-Acc x = pr1 y ∈ pr1 x

open elements (M , _∈_)
open elements (Σ M (Acc _∈_) , _∈-Acc_) renaming
  (El to El-Acc; extensionality-El-equiv to extensionality-El-Acc-equiv;
  compute-extensionality-El-equiv to compute-extensionality-El-Acc-equiv)

private
  is-contr-Acc-z∈ : (w : Σ M (Acc _∈_))
                  → ((z , z∈w) : El (pr1 w))
                  → is-contr (Acc _∈_ z)
  is-contr-Acc-z∈ w (z , z∈w) =
    is-proof-irrelevant-is-prop
      (is-prop-Acc _∈_ z)
      (is-accessible-element-is-related-to-accessible-element-Relation _∈_ (pr2 w) z∈w)

acc-extensionality-equiv : {x y : Σ M (Acc _∈_)}
                         → (x == y)
                         ≃ ((z : Σ M (Acc _∈_)) → (z ∈-Acc x) ≃ (z ∈-Acc y))
acc-extensionality-equiv {x} {y} =
  equivalence-reasoning
    (x == y)
      ≃ (pr1 x == pr1 y)                      by extensionality-type-subtype' (Acc-Prop _∈_) x y

      ≃ ((z : M) → (z ∈ pr1 x) ≃ (z ∈ pr1 y)) by equiv-extensionality

      ≃ Σ (El (pr1 x) ≃ El (pr1 y))
          (λ e → (pr1 ∘ map-equiv e) ~ pr1)   by extensionality-El-equiv

      ≃ Σ (El (pr1 x) ≃ El (pr1 y)) (λ e →
          (pr1 ∘ (map-equiv e ∘ pr1))
          ~ (pr1 ∘ pr1))                      by equiv-tot (λ e →
                                                   equiv-precomp-Π
                                                     (equiv-pr1 (is-contr-Acc-z∈ x))
                                                     (λ (z , z∈x) → pr1 ((map-equiv e) (z , z∈x)) == z))
      ≃ Σ (Σ (El (pr1 x)) (Acc _∈_ ∘ pr1)
        ≃ Σ (El (pr1 y)) (Acc _∈_ ∘ pr1))
          (λ e → (pr1 ∘ (pr1 ∘ map-equiv e))
               ~ (pr1 ∘ pr1))                 by equiv-Σ _
                                                   (equiv-postcomp-equiv
                                                     (inv-equiv (equiv-pr1 (is-contr-Acc-z∈ y)))
                                                     (Σ (El (pr1 x)) (Acc _∈_ ∘ pr1))
                                                   ∘e equiv-precomp-equiv
                                                        (equiv-pr1 (is-contr-Acc-z∈ x))
                                                        (El (pr1 y)))
                                                   (λ e → equiv-Π-equiv-family (λ s →
                                                     equiv-concat
                                                       (ap pr1 (is-section-map-inv-equiv (equiv-pr1 (is-contr-Acc-z∈ y)) (map-equiv e (pr1 s))))
                                                       (pr1 (pr1 s))))
      ≃ Σ (El-Acc x ≃ El-Acc y)
          (λ e → (pr1 ∘ (pr1 ∘ map-equiv e))
               ~ (pr1 ∘ pr1))                 by equiv-Σ
                                                   (λ e → (pr1 ∘ (pr1 ∘ map-equiv e)) ~ (pr1 ∘ pr1))
                                                   (equiv-postcomp-equiv equiv-right-swap-Σ _
                                                   ∘e equiv-precomp-equiv equiv-right-swap-Σ _)
                                                   (λ e →
                                                     equiv-precomp-Π
                                                       equiv-right-swap-Σ
                                                       (λ s → pr1 (pr1 (map-equiv equiv-right-swap-Σ (map-equiv e s))) == pr1 (pr1 s)))
      ≃ Σ (El-Acc x ≃ El-Acc y)
          (λ e → (pr1 ∘ map-equiv e) ~ pr1)   by equiv-tot (λ e →
                                                   equiv-Π-equiv-family (λ z →
                                                     (inv-equiv
                                                       (extensionality-type-subtype'
                                                         (Acc-Prop _∈_) _ _))))
      ≃ ((z : Σ M (Acc _∈_))
        → (z ∈-Acc x) ≃ (z ∈-Acc y))          by inv-equiv (extensionality-El-Acc-equiv {x} {y})

compute-acc-extensionality-equiv : {x : Σ M (Acc _∈_)}
                                 → map-equiv (acc-extensionality-equiv {x} {x}) refl
                                 == (λ _ → id-equiv)
compute-acc-extensionality-equiv {x} =
  ap (map-inv-equiv (extensionality-El-Acc-equiv {x} {x})
     ∘ (map-equiv (equiv-tot (λ e → equiv-Π-equiv-family (λ z →
            (inv-equiv (extensionality-type-subtype' (Acc-Prop _∈_) _ _)))))
     ∘ (map-equiv (equiv-Σ _
            (equiv-postcomp-equiv equiv-right-swap-Σ _
            ∘e equiv-precomp-equiv equiv-right-swap-Σ _)
            (λ e → equiv-precomp-Π equiv-right-swap-Σ _))
     ∘ (map-equiv (equiv-Σ
            (λ e → (s : Σ (El (pr1 x)) (Acc _∈_ ∘ pr1))
              → pr1 (pr1 (map-equiv e s)) == pr1 (pr1 s))
            (equiv-postcomp-equiv (inv-equiv (equiv-pr1 (is-contr-Acc-z∈ x))) _
            ∘e equiv-precomp-equiv (equiv-pr1 (is-contr-Acc-z∈ x)) _)
            (λ e → equiv-Π-equiv-family (λ s → equiv-concat
                (ap pr1 (is-section-map-inv-equiv (equiv-pr1 (is-contr-Acc-z∈ x)) (map-equiv e (pr1 s)))) _)))
     ∘ map-equiv (equiv-tot (λ e → equiv-precomp-Π
             (equiv-pr1 (is-contr-Acc-z∈ x))
             (λ (z , z∈x) → pr1 ((map-equiv e) (z , z∈x)) == z)))))))
     (compute-extensionality-El-equiv {pr1 x})
  ∙ (ap (map-inv-equiv (extensionality-El-Acc-equiv {x} {x})
        ∘ (map-equiv (equiv-tot (λ e → equiv-Π-equiv-family (λ z →
               (inv-equiv (extensionality-type-subtype' (Acc-Prop _∈_) _ _)))))
        ∘ map-equiv (equiv-Σ _
               (equiv-postcomp-equiv equiv-right-swap-Σ _
               ∘e equiv-precomp-equiv equiv-right-swap-Σ _)
               (λ e → equiv-precomp-Π equiv-right-swap-Σ _))))
        (eq-pair-Σ refl (eq-htpy (λ s →
           right-unit ∙
           ap (ap pr1) (coherence-map-inv-equiv (equiv-pr1 (is-contr-Acc-z∈ x)) s)))
         ∙ lem (equiv-pr1 (is-contr-Acc-z∈ x)))
  ∙ (ap (map-inv-equiv (extensionality-El-Acc-equiv {x} {x})
        ∘ map-equiv (equiv-tot (λ e → equiv-Π-equiv-family (λ z →
               (inv-equiv (extensionality-type-subtype' (Acc-Prop _∈_) _ _))))))
        (ap map-right-swap-Σ
            (eq-type-subtype (is-equiv-Prop ∘ pr1) refl))
  ∙ (ap (map-inv-equiv (extensionality-El-Acc-equiv {x} {x}))
        (eq-pair-Σ refl (eq-htpy (λ z →
            is-retraction-map-inv-equiv
              (extensionality-type-subtype' (Acc-Prop _∈_) (pr1 z) (pr1 z))
              refl)))
  ∙ eq-htpy (λ z →
       eq-htpy-equiv (λ z∈-Accx →
         ap (λ s → map-equiv (map-inv-equiv (extensionality-El-Acc-equiv {x} {x}) s z) z∈-Accx)
            (inv (compute-extensionality-El-Acc-equiv {x})) ∙
         htpy-eq (ap map-equiv (htpy-eq
           (is-retraction-map-inv-equiv
             (extensionality-El-Acc-equiv {x} {x})
             (λ _ → id-equiv)) z)) z∈-Accx)))))
    where
      lem : {l1 l2 l3 : Level} {A : UU l1} {B : A → UU l2}
          → {C : (a : A) → B a → UU l3} {X : UU (l1 ⊔ l2 ⊔ l3)}
          → (e : Σ (Σ A B) (λ (a , b) → C a b) ≃ X)
          → Id {A = Σ (Σ (Σ A B) (λ (a , b) → C a b) ≃ Σ (Σ A B) (λ (a , b) → C a b))
                  λ e → (pr1 ∘ (pr1 ∘ map-equiv e)) ~ (pr1 ∘ pr1)}
            (inv-equiv e ∘e (id-equiv ∘e e) , ap pr1 ∘ (ap pr1 ∘ is-retraction-map-inv-equiv e))
            (id-equiv , refl-htpy)
      lem {A = A} {B} {C} =
        ind-equiv
          {A = Σ (Σ A B) (λ (a , b) → C a b)}
          (λ X e →
            (inv-equiv e ∘e (id-equiv ∘e e) , λ s → ap pr1 (ap pr1 (is-retraction-map-inv-equiv e s)))
            == (id-equiv , refl-htpy))
          (ap map-right-swap-Σ
              (eq-type-subtype (is-equiv-Prop ∘ pr1) refl))

-- The ∈-structure of accessible elements
Acc-∈-structure : ∈-structure (i ⊔ j) j
pr1 (pr1 Acc-∈-structure) = Σ M (Acc _∈_)
pr2 (pr1 Acc-∈-structure) = _∈-Acc_
pr2 Acc-∈-structure {x} {y} =
  tr is-equiv
     (eq-htpy (λ {refl → compute-acc-extensionality-equiv}))
     (is-equiv-map-equiv (acc-extensionality-equiv {x} {y}))

open import e-structure.property.foundation Acc-∈-structure

Acc-∈-from-Acc : (x : M) (q : Acc _∈_ x) → Acc _∈-Acc_ (x , q)
Acc-∈-from-Acc x (acc f) =
  acc-curried x (acc f) λ y s t → tr (Acc _∈-Acc_) (eq-type-subtype (Acc-Prop _∈_) refl) (Acc-∈-from-Acc y (f {y} t))
  where
    acc-curried : (x : M) (q : Acc _∈_ x)
                → ((y : M) (s : Acc _∈_ y) → (y , s) ∈-Acc (x , q) → Acc _∈-Acc_ (y , s)) → Acc _∈-Acc_ (x , q)
    acc-curried x q f = acc λ {y} → (ind-Σ f y)

-- This ∈-structure has foundation
foundation : Foundation
foundation = ind-Σ Acc-∈-from-Acc

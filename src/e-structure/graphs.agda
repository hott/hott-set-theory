{-# OPTIONS --without-K #-}

open import elementary-number-theory.natural-numbers

open import foundation.action-on-identifications-functions
open import foundation.cartesian-product-types
open import foundation.coproduct-types
open import foundation.dependent-pair-types
open import foundation.embeddings
open import foundation.equivalences
open import foundation.function-types
open import foundation.identity-types
open import foundation.propositional-maps
open import foundation.propositional-truncations
open import foundation.propositions
open import foundation.subtypes
open import foundation.transport-along-identifications
open import foundation.truncations
open import foundation.truncation-levels
  renaming (truncation-level-minus-one-ℕ to minus-one)
open import foundation.universe-levels

open import e-structure.core
open import functor.slice
open import notation

module e-structure.graphs
  {i j} (((M , _∈_) , p) : ∈-structure i j)
  (ordered-pairs : (M × M) ↪ M) where

-- Import notation for ordered pairs
open ordered-pairing-structure.notation M ordered-pairs

-- Import notation for elements
open elements (M , _∈_)

is-graph : M → UU (i ⊔ j)
is-graph g = (e : M) → e ∈ g → Σ (M × M) (λ (x , y) → e == ⟨ x , y ⟩)

Graph : UU (i ⊔ j)
Graph = Σ M is-graph

-- Convenient notation
source : (g : Graph) → El (pr1 g) → M
source (g , is-graph-g) (e , e∈g) = pr1 (pr1 (is-graph-g e e∈g))

target : (g : Graph) → El (pr1 g) → M
target (g , is-graph-g) (e , e∈g) = pr2 (pr1 (is-graph-g e e∈g))

-- The subtype of targets in a graph
is-target-Prop : Graph → M → Prop (i ⊔ j)
is-target-Prop G@(g , is-graph-g) x =
  trunc-Prop (Σ M λ y → ⟨ y , x ⟩ ∈ g)

is-target : Graph → M → UU (i ⊔ j)
is-target g x = type-Prop (is-target-Prop g x)

Target : Graph → UU (i ⊔ j)
Target g = Σ M (is-target g)

emb-Target : (g : Graph) → Target g ↪ M
emb-Target g = emb-subtype (is-target-Prop g)

-- Decorations of graphs
is-[_]-decoration : ℕ → Graph → (M → M) → UU (i ⊔ j)
is-[ n ]-decoration (g , _) d =
  (x z : M) → z ∈ d x ≃ type-trunc (minus-one n) (Σ M (λ y → ⟨ x , y ⟩ ∈ g × d y == z))

is-0-decoration : Graph → (M → M) → UU (i ⊔ j)
is-0-decoration = is-[ 0 ]-decoration

is-∞-decoration : Graph → (M → M) → UU (i ⊔ j)
is-∞-decoration (g , _) d =
  (x z : M) → z ∈ d x ≃ Σ M (λ y → ⟨ x , y ⟩ ∈ g × d y == z)

[_]-Decoration : ℕ → Graph → UU (i ⊔ j)
[ n ]-Decoration g = Σ (M → M) (is-[ n ]-decoration g)

0-Decoration : Graph → UU (i ⊔ j)
0-Decoration = [ 0 ]-Decoration

∞-Decoration : Graph → UU (i ⊔ j)
∞-Decoration g = Σ (M → M) (is-∞-decoration g)

Child : Graph → M → UU (i ⊔ j)
Child (g , is-graph-g) x =
  Σ M (λ y → ⟨ x , y ⟩ ∈ g)

-- G defines a large P∞-coalgebra
Graph-Coalg : Graph → P∞-Coalg (i ⊔ j) i
pr1 (Graph-Coalg g) = M
pr2 (Graph-Coalg g) x = (Child g x , pr1)

is-target-target : (g : Graph) (e : El (pr1 g))
                 → is-target g (target g e)
is-target-target g e =
  unit-trunc (source g e , tr (_∈ pr1 g) (pr2 ((pr2 g) (pr1 e) (pr2 e))) (pr2 e))

is-target-Child : (g : Graph) (x : M)
                → (y : Child g x)
                → is-target g (pr1 y)
is-target-Child g x (y , ⟨xy⟩∈g) = unit-trunc (x , ⟨xy⟩∈g)

Target-Child : (g : Graph) (x : M)
             → Child g x → Target g
Target-Child g x (y , ⟨xy⟩∈g) = (y , is-target-Child g x (y , ⟨xy⟩∈g))

Graph-sub-Coalg-map : (g : Graph) → Target g → P∞ (i ⊔ j) (Target g)
pr1 (Graph-sub-Coalg-map g (x , _)) = Child g x
pr2 (Graph-sub-Coalg-map g (x , _)) = Target-Child g x

Graph-sub-Coalg : Graph → P∞-Coalg (i ⊔ j) (i ⊔ j)
pr1 (Graph-sub-Coalg g) = Target g
pr2 (Graph-sub-Coalg g) = Graph-sub-Coalg-map g


{-# OPTIONS --without-K #-}

open import foundation.dependent-pair-types
open import foundation.universe-levels

open import order-theory.well-founded-relations

open import e-structure.core

module e-structure.property.foundation
  {i j} (((M , _∈_) , p) : ∈-structure i j) where

Foundation : UU (i ⊔ j)
Foundation = is-well-founded-Relation _∈_

{-# OPTIONS --without-K #-}

open import elementary-number-theory.addition-natural-numbers
open import elementary-number-theory.inequality-natural-numbers
open import elementary-number-theory.natural-numbers
open import elementary-number-theory.strict-inequality-natural-numbers

open import foundation.contractible-types
open import foundation.dependent-pair-types
open import foundation.equivalences
open import foundation.functoriality-dependent-pair-types
open import foundation.identity-types
open import foundation.locally-small-types
open import foundation.truncated-maps
open import foundation.truncated-types
open import foundation.truncation-levels
  renaming(truncation-level-ℕ to to-𝕋;
    truncation-level-minus-one-ℕ to minus-one)
open import foundation.universe-levels

open import e-structure.core
open import functor.n-slice
open import image-factorisation
open import notation

module e-structure.property.aczel-anti-foundation.from-terminal-coalgebra
  {i} (n : ℕ) ((V , desup) : P-[ n ]-Coalg i (lsuc i))
  (V-is-locally-small : is-locally-small i V)
  (V-is-terminal : is-terminal-P-[ n ]-Coalg (V , desup)
    (is-[succ-ℕ n ]-small-is-locally-small V-is-locally-small) (lsuc i))
  where

private
  V-is-succ-n-small : is-[ succ-ℕ n ]-small i V
  V-is-succ-n-small = is-[succ-ℕ n ]-small-is-locally-small V-is-locally-small

  V-is-succ-[_]-small : ∀ k → is-[ succ-ℕ k ]-small i V
  V-is-succ-[ k ]-small = is-[succ-ℕ k ]-small-is-locally-small V-is-locally-small

  fixed-point-V : V ≃ P-[ n ] i V
  fixed-point-V = equiv-terminal-P-[ n ]-Coalg (V , desup) V-is-succ-n-small V-is-terminal

-- -- Since V is a fixpoint for Pⁿ, it has an ∈-structure
open import fixed-point.core n V fixed-point-V hiding (desup)

-- The following properties hold for V

-- Replacement
open import e-structure.property.replacement ∈-structure-V

replacement : (k : ℕ) → k ≤-ℕ n → [ k ]-Replacement
replacement k k≤n = n-replacement
  where
    H : Σ ℕ λ l → add-ℕ l k == n
    H = subtraction-leq-ℕ k n k≤n

    open import fixed-point.replacement
      n k (pr1 H) (inv (pr2 H)) V
      fixed-point-V V-is-succ-[ k ]-small

-- Restricted separation
open import e-structure.property.restricted-separation ∈-structure-V

restricted-separation : Restricted[ n ]-Separation i
restricted-separation = restricted-n-separation
  where
    open import fixed-point.restricted-separation
      n V fixed-point-V

-- Union
open import e-structure.property.union ∈-structure-V

union : (k : ℕ) → k ≤-ℕ n → [ k ]-Union
union k k≤n = n-union
  where
    H : Σ ℕ λ l → add-ℕ l k == n
    H = subtraction-leq-ℕ k n k≤n

    open import fixed-point.union
      n k (pr1 H) (inv (pr2 H)) V
      fixed-point-V V-is-succ-[ k ]-small

-- Unordered tupling
open import e-structure.property.unordered-tupling ∈-structure-V

unordered-tupling : (k : ℕ) → k ≤-ℕ n
                  → (I : UU i)
                  → [ k ]-UnorderedTupling I
unordered-tupling k k≤n = n-unordered-tupling
  where
    H : Σ ℕ λ l → add-ℕ l k == n
    H = subtraction-leq-ℕ k n k≤n

    open import fixed-point.unordered-tupling
      n k (pr1 H) (inv (pr2 H)) V
      fixed-point-V V-is-succ-[ k ]-small

∞-unordered-tupling : (k : ℕ) → k <-ℕ n
                    → (I : Truncated-Type i (to-𝕋 k))
                    → ∞-UnorderedTupling (type-Truncated-Type I)
∞-unordered-tupling = ∞-unordered-tupling'
  where
    open import fixed-point.unordered-tupling
      n n 0 (commutative-add-ℕ n 0) V
      fixed-point-V V-is-succ-n-small
    open ∞-unordered-tupling-from-k=0 refl
      renaming (∞-unordered-tupling to ∞-unordered-tupling')

-- Exponentiation
open ordered-pairing-structure V

-- We can construct ordered pairs
ordered-pairs : OrderedPairs
ordered-pairs = ordered-pairs'
  where
    open import fixed-point.unordered-tupling
      n n 0 (commutative-add-ℕ n 0) V
      fixed-point-V V-is-succ-n-small
    open ordered-pairs-from-k=0 refl
      renaming (ordered-pairs to ordered-pairs')

open ordered-pairing-structure.notation V ordered-pairs

open import e-structure.property.exponentiation ∈-structure-V

exponentiation : (ordered-pairs' : OrderedPairs) → Exponentiation ordered-pairs'
exponentiation ordered-pairs' = exponentiation''
  where
    open import fixed-point.exponentiation
      n V fixed-point-V ordered-pairs'
      renaming (exponentiation to exponentiation'')

-- Natural numbers with respect to any truncated representation.
-- in particular with respect to the von Neumann encoding
open import e-structure.property.natural-numbers ∈-structure-V
open import e-structure.internalisations ∈-structure-V

natural-numbers-trunc-repr : (f : Representation ℕ)
                           → is-trunc-map (minus-one n) f
                           → NaturalNumbersRepresentedBy f
natural-numbers-trunc-repr = natural-numbers-trunc-repr'
  where
    open import fixed-point.natural-numbers
      n V fixed-point-V
      renaming (natural-numbers-trunc-repr to natural-numbers-trunc-repr')

open import fixed-point.empty-set n V fixed-point-V
open import fixed-point.unordered-tupling
  n zero-ℕ n refl V fixed-point-V V-is-locally-small
open import fixed-point.union
  n zero-ℕ n refl V fixed-point-V V-is-locally-small
open von-neumann empty-set singletons binary-union

natural-numbers-von-neumann : NaturalNumbersRepresentedBy von-neumann-repr
natural-numbers-von-neumann = natural-numbers-von-neumann' V-is-locally-small
  where
    open import fixed-point.natural-numbers
      n V fixed-point-V
      renaming (natural-numbers-von-neumann to natural-numbers-von-neumann')


-- The terminal Pⁿ-coalgebra is a model of [ n ]-AczelAntiFoundation
open import e-structure.graph.to-P-n-coalgebra
  n (V , desup) (is-emb-is-equiv (pr2 fixed-point-V))
  ordered-pairs V-is-locally-small

-- Import Aczel's anti-foundation axiom
open import e-structure.property.aczel-anti-foundation
  ∈-structure-V ordered-pairs

aczel-anti-foundation : [ n ]-AczelAntiFoundation
aczel-anti-foundation G =
  is-contr-equiv _
    (equiv-tot λ d → inv-equiv (equiv-is-decoration G d))
    (V-is-terminal (graph-Pⁿ-Coalg G))

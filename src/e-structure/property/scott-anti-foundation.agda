{-# OPTIONS --without-K #-}

open import elementary-number-theory.natural-numbers

open import foundation.cartesian-product-types
open import foundation.dependent-pair-types
open import foundation.embeddings
open import foundation.equivalences
open import foundation.function-types
open import foundation.functoriality-dependent-pair-types
open import foundation.propositions
open import foundation.truncated-maps
open import foundation.truncation-levels
  renaming (truncation-level-minus-one-ℕ to minus-one)
open import foundation.unique-existence
open import foundation.universe-levels

open import coiterative.infinity-multiset
open import e-structure.core
open import functor.slice
open import notation

module e-structure.property.scott-anti-foundation
  {i j} (((M , _∈_) , p) : ∈-structure i j)
  (ordered-pairs : (M × M) ↪ M) where

-- Import definition of graphs and decorations
open import e-structure.graphs ((M , _∈_) , p) ordered-pairs

-- Bisimulations on the target nodes of graphs
is-scott-bisimulation : ∀ {k} → (g : Graph)
                      → (Target g → Target g → UU k)
                      → UU (i ⊔ j ⊔ k)
is-scott-bisimulation g R =
  ∀ x y → R x y →
  Σ (Child g (pr1 x) ≃ Child g (pr1 y)) (λ α →
      (x' : Child g (pr1 x))
      → R (Target-Child g (pr1 x) x') (Target-Child g (pr1 y) (α 〈 x' 〉)))

ScottBisimulation : ∀ k → Graph → UU (i ⊔ j ⊔ lsuc k)
ScottBisimulation k g = Σ (Target g → Target g → UU k) (is-scott-bisimulation g)

-- Equality is a bisimulation on any graph
eq-is-scott-bisimulation : (g : Graph) → is-scott-bisimulation g _==_
eq-is-scott-bisimulation g x y = pr2 (==-E'-Coalg (Graph-sub-Coalg g)) (x , y)

-- Scott extensional graphs
is-scott-[_]-extensional : ℕ → Graph → UU (lsuc i ⊔ lsuc j)
is-scott-[ n ]-extensional g =
  is-trunc-map (minus-one n) (corec∞ (Graph-sub-Coalg g))

[_]-ScottExtensionalGraph : ℕ → UU (lsuc i ⊔ lsuc j)
[ n ]-ScottExtensionalGraph = Σ Graph is-scott-[ n ]-extensional

-- Scott's anti-foundation axiom
[_]-ScottAntiFoundation₁ : ℕ → UU (lsuc i ⊔ lsuc j)
[ n ]-ScottAntiFoundation₁ =
  ((g , _) : [ n ]-ScottExtensionalGraph) → ∞-Decoration g

ScottAntiFoundation₂ : UU (i ⊔ j)
ScottAntiFoundation₂ =
  (g : Graph) → is-prop (∞-Decoration g)

[_]-ScottAntiFoundation : ℕ → UU (lsuc i ⊔ lsuc j)
[ n ]-ScottAntiFoundation =
  [ n ]-ScottAntiFoundation₁ × ScottAntiFoundation₂

-- At level 0, being Scott extensional is the same as being bisimulation simple
is-bisim-simple-graph : ∀ k → Graph → UU (i ⊔ j ⊔ lsuc k)
is-bisim-simple-graph k g =
  ((R , σ) : ScottBisimulation k g) →
    ∃! (∀ x y → R x y → x == y) (λ f →
       ∀ x y xRy →
         eq-is-scott-bisimulation g x y (f x y xRy) ==
         tot (λ _ → map-Π λ _ → f _ _) (σ x y xRy))

BisimSimpleGraph : ∀ k → UU (i ⊔ j ⊔ lsuc k)
BisimSimpleGraph k = Σ Graph (is-bisim-simple-graph k)

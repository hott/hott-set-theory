{-# OPTIONS --without-K #-}

open import elementary-number-theory.natural-numbers

open import foundation.cartesian-product-types
open import foundation.dependent-pair-types
open import foundation.equivalences
open import foundation.propositions
open import foundation.truncated-types
open import foundation.truncation-levels
  renaming (truncation-level-minus-one-ℕ to minus-one)
open import foundation.universe-levels

open import e-structure.core
open import notation

module e-structure.property.restricted-separation
  {i j} (((M , _∈_) , _) : ∈-structure i j) where

open e-structure.core.elements (M , _∈_)

Restricted[_]-Separation : ℕ → (k : Level) → UU (i ⊔ (j ⊔ lsuc k))
Restricted[ n ]-Separation k = (x : M) → (P : El x → Truncated-Type k (minus-one n))
  → Σ M λ ｛x|P｝ → (z : M) → (z ∈ ｛x|P｝) ≃ (Σ (z ∈ x) λ p → type-Truncated-Type (P (z , p)))

-- Special case
Restricted0-Separation : ∀ k → UU (i ⊔ (j ⊔ lsuc k))
Restricted0-Separation = Restricted[ zero-ℕ ]-Separation

Restricted∞-Separation : ∀ k → UU (i ⊔ (j ⊔ lsuc k))
Restricted∞-Separation k = (P : M → UU k) → (x : M)
  → Σ M (λ [x|P] → (z : M) → (z ∈ [x|P]) ≃ ((z ∈ x) × (P z)))


{-# OPTIONS --without-K #-}

open import elementary-number-theory.natural-numbers

open import foundation.cartesian-product-types
open import foundation.contractible-types
open import foundation.dependent-pair-types
open import foundation.embeddings
open import foundation.universe-levels

open import e-structure.core

module e-structure.property.aczel-anti-foundation
  {i j} (((M , _∈_) , p) : ∈-structure i j)
  (ordered-pairs : (M × M) ↪ M) where

-- Import definition of graphs and decorations
open import e-structure.graphs ((M , _∈_) , p) ordered-pairs

[_]-AczelAntiFoundation : ℕ → UU (i ⊔ j)
[ n ]-AczelAntiFoundation = (g : Graph) → is-contr ([ n ]-Decoration g)


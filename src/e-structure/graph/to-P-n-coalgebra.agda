{-# OPTIONS --without-K #-}

open import elementary-number-theory.natural-numbers

open import foundation.action-on-identifications-functions
open import foundation.cartesian-product-types
open import foundation.contractible-types
open import foundation.dependent-pair-types
  renaming (ind-Σ to uncurry; ev-pair to curry)
open import foundation.embeddings
open import foundation.equality-cartesian-product-types
open import foundation.equivalences
open import foundation.fibers-of-maps
open import foundation.function-types
open import foundation.functoriality-dependent-function-types
open import foundation.functoriality-dependent-pair-types
open import foundation.functoriality-truncation
open import foundation.homotopies
open import foundation.identity-types
open import foundation.locally-small-types
open import foundation.propositional-maps
open import foundation.raising-universe-levels
open import foundation.subtypes
open import foundation.truncated-maps
open import foundation.truncated-types
open import foundation.truncations
open import foundation.truncation-levels
  renaming (truncation-level-minus-one-ℕ to minus-one)
open import foundation.type-arithmetic-dependent-pair-types
open import foundation.universe-levels

open import e-structure.core
open import functor.n-slice
open import functor.slice
open import image-factorisation
open import notation

module e-structure.graph.to-P-n-coalgebra
  {i j} (n : ℕ) ((X , m) : P-[ n ]-Coalg i j)
  (m-is-emb : is-emb m) (ordered-pairs : (X × X) ↪ X)
  (X-is-locally-small : is-locally-small i X) where

-- Import notation 〈_,_〉 for ordered pairs
open ordered-pairing-structure.notation X ordered-pairs

-- Import ∈-structure given by coalgebra map
open import e-structure.from-P-n-coalgebra n (X , m) m-is-emb

-- Import definition of graphs and decorations
open import e-structure.graphs ∈-structure-P ordered-pairs

-- Imports the notation _≈_ for the small identity type of
-- a locally small type
open notation.locally-small-types X-is-locally-small


-- A graph induces a Pⁿ-coalgebra on X
module _ (G@(g , is-graph-g) : Graph) where

  -- The (small) index type of the children of x in g
  index-children : X → UU i
  index-children x =
    let (E , edge) = m g in
    Σ E (λ e → source G (edge 〈 e 〉 , (e , refl)) ≈ x)

  -- The type above is equivalent to the (large) type Σ (y : V) (⟨ x , y ⟩ ∈ g)
  equiv-index-children : (x : X)
                       → index-children x
                       ≃ Child G x
  equiv-index-children x =
    let (E , edge) = m g in

    equivalence-reasoning

      index-children x

      ≃ Σ E (λ e →
          source G (edge 〈 e 〉 , (e , refl)) == x)                by equiv-tot (λ e →
                                                                   inv-equiv (pr2 (X-is-locally-small _ x)))
      ≃ Σ (Σ E (λ e →
             source G (edge 〈 e 〉 , (e , refl)) == x))
          (λ (e , _) →
            Σ X (λ y →
              target G (edge 〈 e 〉 , (e , refl)) == y))           by inv-right-unit-law-Σ-is-contr (λ (e , _) →
                                                                   is-torsorial-path _)
      ≃ Σ X (λ y →
          Σ E (λ e →
            source G (edge 〈 e 〉 , (e , refl)) == x ×
            target G (edge 〈 e 〉 , (e , refl)) == y))             by ((λ ((e , p) , (y , q)) → (y , (e , (p , q)))) ,
                                                                  is-equiv-is-invertible
                                                                    (λ (y , (e , (p , q))) → ((e , p) , (y , q)))
                                                                    refl-htpy
                                                                    refl-htpy)
      ≃ Σ X (λ y →
          Σ E (λ e →
            pr1 (is-graph-g (edge 〈 e 〉) (e , refl)) == (x , y))) by equiv-tot (λ y →
                                                                   equiv-tot (λ e →
                                                                     equiv-eq-pair _ _))
      ≃ Σ X (λ y →
          Σ E (λ e →
            ordered-pairs
              〈 pr1 (is-graph-g (edge 〈 e 〉) (e , refl)) 〉 ==
            ⟨ x , y ⟩))                                       by equiv-tot (λ y →
                                                                   equiv-tot (λ e →
                                                                     equiv-ap-emb ordered-pairs))
      ≃ Σ X (λ y → ⟨ x , y ⟩ ∈ g)                  by equiv-tot (λ y →
                                                                   equiv-tot (λ e →
                                                                     equiv-concat
                                                                       (pr2 (is-graph-g (edge 〈 e 〉) (e , refl)))
                                                                       ⟨ x , y ⟩))

  child : (x : X) → index-children x → X
  child x (e , _) =
    let (E , edge) = m g in
    target G (edge 〈 e 〉 , (e , refl))

  triangle-equiv-index-children : (x : X)
                                → pr1
                                ~ child x ∘ map-inv-equiv (equiv-index-children x)
  triangle-equiv-index-children x z =
    ap pr1 (inv (is-section-map-inv-equiv (equiv-index-children x) z))

  -- We classify the fibers of child x
  equiv-fiber-child : (x y : X)
                    → fiber (child x) y
                    ≃ (⟨ x , y ⟩ ∈ g)
  equiv-fiber-child x y =
    equivalence-reasoning

      fiber (child x) y

      ≃ Σ (Σ X (λ y' → ⟨ x , y' ⟩ ∈ g))
          (λ (y' , _) → y' == y)                   by equiv-Σ-equiv-base _ (equiv-index-children x)

      ≃ Σ (Σ X λ y' → y' == y)
          (λ (y' , _) → ⟨ x , y' ⟩ ∈ g)            by equiv-right-swap-Σ

      ≃ ⟨ x , y ⟩ ∈ g                              by left-unit-law-Σ-is-contr
                                                        (is-torsorial-path' y)
                                                        (y , refl)

  is-trunc-map-child : (x : X) → is-trunc-map (minus-one n) (child x)
  is-trunc-map-child x y =
    is-trunc-equiv (minus-one n) _
      (equiv-fiber-child x y)
      is-trunc-∈

  trunc-map-child : (x : X)
                  → trunc-map (minus-one n) (index-children x) X
  pr1 (trunc-map-child x) = child x
  pr2 (trunc-map-child x) = is-trunc-map-child x

  graph-Pⁿ-Coalg-map : X → P-[ n ] i X
  pr1 (graph-Pⁿ-Coalg-map x) = index-children x
  pr2 (graph-Pⁿ-Coalg-map x) = trunc-map-child x

  graph-Pⁿ-Coalg : P-[ n ]-Coalg i j
  pr1 graph-Pⁿ-Coalg = X
  pr2 graph-Pⁿ-Coalg = graph-Pⁿ-Coalg-map

  graph-P∞-Coalg : P∞-Coalg i j
  graph-P∞-Coalg = P-[ n ]-to-P∞-Coalg graph-Pⁿ-Coalg

  -- Coalgebra on only the target nodes in the graph
  is-target-child : (x : X) → (a : index-children x) → is-target G (child x a)
  is-target-child x (e , _) =
    let (E , edge) = m g in
    is-target-target G (edge 〈 e 〉 , (e , refl))

  sub-child : (x : X) → index-children x → Target G
  pr1 (sub-child x a) = child x a
  pr2 (sub-child x a) = is-target-child x a

  is-trunc-map-sub-child : (x : X) → is-trunc-map (minus-one n) (sub-child x)
  is-trunc-map-sub-child x =
    is-trunc-map-right-factor (minus-one n) (map-emb (emb-Target G)) (sub-child x)
      (is-trunc-map-is-prop-map _ (is-prop-map-emb (emb-Target G)))
      (is-trunc-map-child x)

  trunc-map-sub-child : (x : X)
                      → trunc-map (minus-one n) (index-children x) (Target G)
  pr1 (trunc-map-sub-child x) = sub-child x
  pr2 (trunc-map-sub-child x) = is-trunc-map-sub-child x

  graph-Pⁿ-sub-Coalg-map : Target G → P-[ n ] i (Target G)
  pr1 (graph-Pⁿ-sub-Coalg-map x) = index-children (pr1 x)
  pr2 (graph-Pⁿ-sub-Coalg-map x) = trunc-map-sub-child (pr1 x)

  graph-Pⁿ-sub-Coalg : P-[ n ]-Coalg i (i ⊔ j)
  pr1 graph-Pⁿ-sub-Coalg = Target G
  pr2 graph-Pⁿ-sub-Coalg = graph-Pⁿ-sub-Coalg-map

  graph-P∞-sub-Coalg : P∞-Coalg i (i ⊔ j)
  graph-P∞-sub-Coalg = P-[ n ]-to-P∞-Coalg graph-Pⁿ-sub-Coalg

  -- Graph-sub-Coalg is the same as raising graph-Pⁿ-sub-Coalg
  htpy-raise-graph-P∞-sub-Coalg-map : (raise-P∞ j (Target G) ∘ map-emb (P-[ n ]↪P∞ (Target G)) ∘ graph-Pⁿ-sub-Coalg-map)
                                     ~ Graph-sub-Coalg-map G
  htpy-raise-graph-P∞-sub-Coalg-map (x , _) =
    map-inv-equiv
      (equiv-eq-P∞
        (raise j (index-children x) , sub-child x ∘ map-inv-equiv (compute-raise j (index-children x)))
        (Child G x , Target-Child G x))
      ((equiv-index-children x ∘e inv-equiv (compute-raise j (index-children x))) ,
       (λ s → eq-type-subtype (is-target-Prop G) refl))


  -- Being an n-decoration is equivalent to being a Pⁿ-coalgebra
  -- homomorphism from graph-Pⁿ-Coalg into (X,m)
  equiv-is-decoration : (d : X → X)
                      → is-hom-P-[ n ]-Coalg graph-Pⁿ-Coalg (X , m)
                          (is-[succ-ℕ n ]-small-is-locally-small X-is-locally-small) d
                      ≃ is-[ n ]-decoration G d
  equiv-is-decoration d =
    let p = is-[succ-ℕ n ]-small-is-locally-small X-is-locally-small in

    equivalence-reasoning

      (m ∘ d ~ map-P-[ n ] p d ∘ graph-Pⁿ-Coalg-map)

      ≃ (∀ x z →
          fiber (pr1 (pr2 (m (d x)))) z ≃
          fiber (pr1 (trunc-image-inclusion p (d ∘ child x))) z) by equiv-Π-equiv-family (λ x →
                                                                      equiv-eq-P-[ n ]' _ _)
      ≃ (∀ x z →
          (z ∈ d x) ≃
          type-trunc (minus-one n) (fiber ((d ∘ child x)) z))    by equiv-Π-equiv-family (λ x →
                                                                      equiv-Π-equiv-family (λ z →
                                                                        equiv-postcomp-equiv
                                                                          (equiv-fiber-trunc-Image-im p (d ∘ child x) z)
                                                                          _))
      ≃ (∀ x z →
          (z ∈ d x) ≃
          type-trunc (minus-one n)
            (Σ (fiber d z) λ (y , _) → fiber (child x) y))       by equiv-Π-equiv-family (λ x →
                                                                      equiv-Π-equiv-family (λ z →
                                                                        equiv-postcomp-equiv
                                                                          (equiv-trunc (minus-one n)
                                                                            (compute-fiber-comp d (child x) z))
                                                                          _))
      ≃ (∀ x z →
          (z ∈ d x) ≃
          type-trunc (minus-one n)
            (Σ (fiber d z) λ (y , _) → ⟨ x , y ⟩ ∈ g))           by equiv-Π-equiv-family (λ x →
                                                                      equiv-Π-equiv-family (λ z →
                                                                        equiv-postcomp-equiv
                                                                          (equiv-trunc (minus-one n)
                                                                            (equiv-tot (λ (y , _) →
                                                                              equiv-fiber-child x y)))
                                                                          _))
      ≃ is-[ n ]-decoration G d                                  by equiv-Π-equiv-family (λ x →
                                                                      equiv-Π-equiv-family (λ z →
                                                                        equiv-postcomp-equiv
                                                                          (equiv-trunc (minus-one n)
                                                                            ((λ ((y , q) , H) → (y , (H , q))) ,
                                                                            (is-equiv-is-invertible
                                                                              (λ (y , (H , q)) → ((y , q) , H))
                                                                              refl-htpy
                                                                              refl-htpy)))
                                                                          _))

  -- Being an ∞-decoration is equivalent to being a P∞-coalgebra
  -- homomorphism from graph-Pⁿ-Coalg into (X,m)
  equiv-is-∞-decoration : (d : X → X)
                        → is-hom-P∞-Coalg graph-P∞-Coalg (P-[ n ]-to-P∞-Coalg (X , m)) d
                        ≃ is-∞-decoration G d
  equiv-is-∞-decoration d =
    equivalence-reasoning
      is-hom-P∞-Coalg
        graph-P∞-Coalg
        (P-[ n ]-to-P∞-Coalg (X , m))
        d
      ≃ (∀ x z → fiber (pr1 (pr2 (m (d x)))) z
               ≃ fiber (d ∘ child x) z)                      by equiv-Π-equiv-family (λ x →
                                                                    equiv-eq-P∞' _ _)
      ≃ (∀ x z → z ∈ d x
               ≃ Σ (fiber d z) λ (y , _) → fiber (child x) y) by equiv-Π-equiv-family (λ x →
                                                                     equiv-Π-equiv-family (λ z →
                                                                       equiv-postcomp-equiv
                                                                         (compute-fiber-comp d (child x) z)
                                                                         _))
      ≃ (∀ x z → z ∈ d x
               ≃ Σ (fiber d z) λ (y , _) → ⟨ x , y ⟩ ∈ g)   by equiv-Π-equiv-family (λ x →
                                                                     equiv-Π-equiv-family (λ z →
                                                                       equiv-postcomp-equiv
                                                                         (equiv-tot (λ (y , _) →
                                                                               equiv-fiber-child x y))
                                                                         _))
      ≃ is-∞-decoration G d                                     by equiv-Π-equiv-family (λ x →
                                                                     equiv-Π-equiv-family (λ z →
                                                                       equiv-postcomp-equiv
                                                                         ((λ ((y , q) , H) → (y , (H , q))) ,
                                                                          (is-equiv-is-invertible
                                                                            (λ (y , (H , q)) → ((y , q) , H))
                                                                            refl-htpy
                                                                            refl-htpy))
                                                                         _))

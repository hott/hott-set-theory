{-# OPTIONS --without-K --lossy-unification #-}

open import elementary-number-theory.natural-numbers

open import foundation.action-on-identifications-functions
open import foundation.binary-relations
  renaming (total-space-Relation to tot-space)
open import foundation.cartesian-product-types
open import foundation.contractible-types
open import foundation.dependent-pair-types
  renaming (ind-Σ to uncurry; ev-pair to curry)
open import foundation.dependent-universal-property-equivalences
open import foundation.embeddings
open import foundation.equality-dependent-pair-types
open import foundation.equivalences
open import foundation.function-extensionality
open import foundation.function-types
open import foundation.functoriality-dependent-function-types
open import foundation.functoriality-dependent-pair-types
open import foundation.homotopies
open import foundation.identity-types
open import foundation.injective-maps
open import foundation.locally-small-types
open import foundation.monomorphisms
open import foundation.postcomposition-functions
open import foundation.propositional-maps
open import foundation.propositions
open import foundation.raising-universe-levels
open import foundation.small-types
open import foundation.structure-identity-principle
open import foundation.subtypes
open import foundation.transport-along-identifications
open import foundation.truncated-maps
open import foundation.truncation-levels
  renaming (truncation-level-minus-one-ℕ to minus-one;
    truncation-level-minus-two-ℕ to minus-two)
open import foundation.type-arithmetic-dependent-pair-types
open import foundation.univalence
open import foundation.universe-levels
open import foundation.whiskering-homotopies

open import coiterative.infinity-multiset
open import container
open import container.bisimulation
open import container.indexed
open import container.indexed.coalgebra
open import functor.n-slice
open import functor.slice
open import image-factorisation
open import notation

module coiterative.set where

private
  variable
    i j : Level

-- Coiterative n-types
is-[_]-coiterative-[_]-type : ℕ → 𝕋 → V∞∞ i → UU (lsuc i)
is-[ zero-ℕ ]-coiterative-[ n ]-type x = is-trunc-map n (desup∞∞ x ↓)
is-[ succ-ℕ k ]-coiterative-[ n ]-type x =
  (a : ¦ desup∞∞ x ¦) → is-[ k ]-coiterative-[ n ]-type ((desup∞∞ x ↓) a)

is-prop-is-[_]-coiterative-[_]-type : (k : ℕ) (n : 𝕋) (x : V∞∞ i)
                                    → is-prop (is-[ k ]-coiterative-[ n ]-type x)
is-prop-is-[ zero-ℕ ]-coiterative-[ n ]-type x = is-prop-is-trunc-map n (desup∞∞ x ↓)
is-prop-is-[ succ-ℕ k ]-coiterative-[ n ]-type x =
  is-prop-Π (λ a → is-prop-is-[ k ]-coiterative-[ n ]-type ((desup∞∞ x ↓) a))

is-[_]-coiterative-[_]-type-Prop : ℕ → 𝕋 → V∞∞ i → Prop (lsuc i)
pr1 (is-[ k ]-coiterative-[ n ]-type-Prop x) = is-[ k ]-coiterative-[ n ]-type x
pr2 (is-[ k ]-coiterative-[ n ]-type-Prop x) = is-prop-is-[ k ]-coiterative-[ n ]-type x

is-coiterative-[_]-type : 𝕋 → V∞∞ i → UU (lsuc i)
is-coiterative-[ n ]-type x =
  (k : ℕ) → is-[ k ]-coiterative-[ n ]-type x

is-prop-is-coiterative-[_]-type : (n : 𝕋) (x : V∞∞ i)
                                → is-prop (is-coiterative-[ n ]-type x)
is-prop-is-coiterative-[ n ]-type x =
  is-prop-Π (λ k → is-prop-is-[ k ]-coiterative-[ n ]-type x)

is-coiterative-[_]-type-Prop : 𝕋 → V∞∞ i → Prop (lsuc i)
pr1 (is-coiterative-[ n ]-type-Prop x) = is-coiterative-[ n ]-type x
pr2 (is-coiterative-[ n ]-type-Prop x) = is-prop-is-coiterative-[ n ]-type x


-- V∞-[ n ] is the subtype of V∞∞ of coiterative (n-1)-types
V∞-[_] : ℕ → (i : Level) → UU (lsuc i)
V∞-[ n ] i = Σ (V∞∞ i) is-coiterative-[ minus-one n ]-type

-- Some special cases
V∞⁰ : ∀ i → UU (lsuc i)
V∞⁰ = V∞-[ 0 ]

V∞¹ : ∀ i → UU (lsuc i)
V∞¹ = V∞-[ 1 ]

equiv-is-coiterative-[_]-type : (n : 𝕋) (x : V∞∞ i)
                              → is-coiterative-[ n ]-type x
                              ≃ (is-trunc-map n (desup∞∞ x ↓))
                              × ((a : ¦ desup∞∞ x ¦) → is-coiterative-[ n ]-type ((desup∞∞ x ↓) a))
equiv-is-coiterative-[ n ]-type x =
  equiv-prop
    (is-prop-is-coiterative-[ n ]-type x)
    (is-prop-prod
      (is-prop-is-trunc-map n (desup∞∞ x ↓))
      (is-prop-Π (λ a → is-prop-is-coiterative-[ n ]-type ((desup∞∞ x ↓) a))))
    (λ p → (p 0 , λ a k → p (succ-ℕ k) a))
    (λ (p , q) → ind-ℕ p λ k _ a → q a k)

desup∞-[_] : (n : ℕ) → V∞-[ n ] i → P-[ n ] i (V∞-[ n ] i)
pr1 (desup∞-[ n ] x) = ¦ desup∞∞ (pr1 x) ¦
pr2 (desup∞-[ n ] x) =
  trunc-map-into-subtype (minus-one n)
    is-coiterative-[ minus-one n ]-type-Prop
    (desup∞∞ (pr1 x) ↓ , pr2 x 0)
    (λ a → pr2 (map-equiv (equiv-is-coiterative-[ minus-one n ]-type (pr1 x)) (pr2 x)) a)

-- Inclusion of V∞-[ n ] into V∞∞
V∞-[_]↪V∞∞ : (n : ℕ) → V∞-[ n ] i ↪ V∞∞ i
V∞-[ n ]↪V∞∞ = emb-subtype is-coiterative-[ minus-one n ]-type-Prop

V∞-[_]-to-V∞∞ : (n : ℕ) → V∞-[ n ] i → V∞∞ i
V∞-[ n ]-to-V∞∞ = map-emb V∞-[ n ]↪V∞∞

-- Some special cases
V∞⁰↪V∞∞ : V∞⁰ i ↪ V∞∞ i
V∞⁰↪V∞∞ = V∞-[ 0 ]↪V∞∞

V∞¹↪V∞∞ : V∞¹ i ↪ V∞∞ i
V∞¹↪V∞∞ = V∞-[ 1 ]↪V∞∞

V∞⁰-to-V∞∞ : V∞⁰ i → V∞∞ i
V∞⁰-to-V∞∞ = V∞-[ 0 ]-to-V∞∞

V∞¹-to-V∞∞ : V∞¹ i → V∞∞ i
V∞¹-to-V∞∞ = V∞-[ 1 ]-to-V∞∞

is-trunc-map-V∞-[_]↪V∞∞ : (n : ℕ)
                        → is-trunc-map (minus-one n) (map-emb (V∞-[_]↪V∞∞ {i} n))
is-trunc-map-V∞-[ n ]↪V∞∞ =
  is-trunc-map-is-prop-map
    (minus-two n)
    (is-prop-map-is-emb
      (is-emb-map-emb V∞-[ n ]↪V∞∞))

-- Extensionality for P-[ n ] i (V∞-[ n ] i)
extensionality-P-V∞-[_] : ∀ {i} (n : ℕ)
                        → ((A , f) (B , g) : P-[ n ] i (V∞-[ n ] i))
                        → (A , f) == (B , g)
                        ≃ (A , pr1 ∘ map-trunc-map f) == (B , pr1 ∘ map-trunc-map g)
extensionality-P-V∞-[_] {i} n (A , f) (B , g) =
  inv-equiv (equiv-eq-P∞ (A , pr1 ∘ map-trunc-map f) (B , pr1 ∘ map-trunc-map g)) ∘e
  extensionality-Σ
    (λ g' e → (pr1 ∘ map-trunc-map f) ~ (pr1 ∘ (map-trunc-map g' ∘ map-equiv e)))
    id-equiv
    refl-htpy
    (λ _ → equiv-univalence)
    (λ f' →
      equiv-funext ∘e
      equiv-postcomp-is-mono
        (i)
        (pr1)
        (is-mono-is-emb pr1 (is-emb-inclusion-subtype is-coiterative-[ minus-one n ]-type-Prop))
        (map-trunc-map f)
        (map-trunc-map f') ∘e
      extensionality-type-subtype' (is-trunc-map-Prop (minus-one n)) f f')
    (B , g)

-- V∞-[ n ] is a fixed point for P-[ n ]
sup∞-[_] : (n : ℕ) → P-[ n ] i (V∞-[ n ] i) → V∞-[ n ] i
pr1 (sup∞-[ n ] (A , f)) = sup∞∞ (A , pr1 ∘ map-trunc-map f)
pr2 (sup∞-[ n ] (A , f)) =
  map-inv-equiv
    (equiv-is-coiterative-[ minus-one n ]-type _)
    (inv-tr (is-trunc-map (minus-one n) ∘ pr2)
            (is-section-map-inv-equiv fixed-point-V∞∞ (A , pr1 ∘ map-trunc-map f))
            (is-trunc-map-comp _ _ _
              is-trunc-map-V∞-[ n ]↪V∞∞
              (pr2 f)) ,
    inv-tr (λ u → (s : ¦ u ¦) → is-coiterative-[ minus-one n ]-type ((u ↓) s))
           (is-section-map-inv-equiv fixed-point-V∞∞ (A , pr1 ∘ map-trunc-map f))
           (λ a → pr2 (map-trunc-map f a)))

-- Some special cases
sup∞⁰ : P⁰ i (V∞⁰ i) → V∞⁰ i
sup∞⁰ = sup∞-[ 0 ]

sup∞¹ : P¹ i (V∞¹ i) → V∞¹ i
sup∞¹ = sup∞-[ 1 ]

-- sup∞-[ n ] and desup∞-[ n ] form an equivalence
sup∞-desup∞-[_] : (n : ℕ) (x : V∞-[ n ] i)
                → sup∞-[ n ] (desup∞-[ n ] x) == x
sup∞-desup∞-[ n ] x =
  eq-type-subtype
    is-coiterative-[ minus-one n ]-type-Prop
    (is-retraction-map-inv-equiv fixed-point-V∞∞ (pr1 x))

desup∞-sup∞-[_] : (n : ℕ) (s : P-[ n ] i (V∞-[ n ] i))
                → desup∞-[ n ] (sup∞-[ n ] s) == s
desup∞-sup∞-[ n ] (A , f) =
  map-inv-equiv
    (extensionality-P-V∞-[ n ] (desup∞-[ n ] (sup∞-[ n ] (A , f))) (A , f))
    (is-section-map-inv-equiv fixed-point-V∞∞ (A , pr1 ∘ map-trunc-map f))

fixed-point-V∞-[_] : (n : ℕ) → V∞-[ n ] i ≃ P-[ n ] i (V∞-[ n ] i)
pr1 fixed-point-V∞-[ n ] = desup∞-[ n ]
pr2 fixed-point-V∞-[ n ] =
  is-equiv-is-invertible
    sup∞-[ n ]
    desup∞-sup∞-[ n ]
    sup∞-desup∞-[ n ]

-- V∞-[ n ] is locally small
is-locally-small-V∞-[_] : (n : ℕ) → is-locally-small i (V∞-[ n ] i)
is-locally-small-V∞-[ n ] x y =
  is-small-equiv _
    (extensionality-type-subtype' is-coiterative-[ minus-one n ]-type-Prop x y)
    (is-locally-small-V∞∞ (pr1 x) (pr1 y))

is-[succ-ℕ_]-small-V∞-[_] : (k n : ℕ) → is-[ succ-ℕ k ]-small i (V∞-[ n ] i)
is-[succ-ℕ k ]-small-V∞-[ n ] =
  is-[succ-ℕ k ]-small-is-locally-small is-locally-small-V∞-[ n ]

-- The inclusion of V∞-[ n ] into V∞∞ is a P∞-coalgebra homomorphism
is-hom-P∞-Coalg-V∞-[_]-incl : (n : ℕ)
                           → is-hom-P∞-Coalg
                               (P-[ n ]-to-P∞-Coalg (V∞-[ n ] i , desup∞-[ n ]))
                               (V∞∞ i , desup∞∞)
                               (map-emb V∞-[ n ]↪V∞∞)
is-hom-P∞-Coalg-V∞-[ n ]-incl = refl-htpy

-- The type of P∞-coalgebra homomorphisms into V∞-[ n ] is a proposition
emb-hom-P∞-Coalg-V∞-[_]-V∞∞ : (n : ℕ)
                            → ((X , m) : P∞-Coalg i j)
                            → hom-P∞-Coalg (X , m) (P-[ n ]-to-P∞-Coalg (V∞-[ n ] i , desup∞-[ n ]))
                            ↪ hom-P∞-Coalg (X , m) (V∞∞ i , desup∞∞)
emb-hom-P∞-Coalg-V∞-[ n ]-V∞∞ (X , m) =
  emb-Σ _
    (pr1 ∘_ , is-mono-is-emb pr1 (is-emb-inclusion-subtype is-coiterative-[ minus-one n ]-type-Prop) _)
    λ f →
      emb-equiv
        (equiv-Π-equiv-family (λ x →
          equiv-concat (is-hom-P∞-Coalg-V∞-[ n ]-incl (f x)) _ ∘e
          equiv-ap-emb (emb-map-P∞ V∞-[ n ]↪V∞∞)))

is-prop-hom-P∞-Coalg-V∞-[_] : (n : ℕ)
                            → ((X , m) : P∞-Coalg i j)
                            → is-prop (hom-P∞-Coalg (X , m) (P-[ n ]-to-P∞-Coalg (V∞-[ n ] i , desup∞-[ n ])))
is-prop-hom-P∞-Coalg-V∞-[ n ] (X , m) =
  is-prop-emb
    (emb-hom-P∞-Coalg-V∞-[ n ]-V∞∞ (X , m))
    (is-prop-is-contr (V∞∞-is-terminal (X , m)))

desup∞∞⁺ : ∀ {i} → V∞∞ i → P∞ (lsuc i) (V∞∞ i)
desup∞∞⁺ {i} = raise-P∞ (lsuc i) (V∞∞ i) ∘ desup∞∞

corec∞⁺ : ∀ i → V∞∞ i → V∞∞ (lsuc i)
corec∞⁺ i = corec∞ (V∞∞ i , desup∞∞⁺)

-- corec∞⁺ is a P∞-coalgebra homomorphism
is-hom-corec∞⁺ : ∀ {i}
               → is-hom-P∞-Coalg
                   (V∞∞ i , desup∞∞⁺)
                   (V∞∞ (lsuc i) , desup∞∞)
                   (corec∞⁺ i)
is-hom-corec∞⁺ {i} =
  is-hom-P∞-Coalg-corec∞ (V∞∞ i , map-emb (emb-raise-P∞ (lsuc i) (V∞∞ i)) ∘ desup∞∞)

hom-corec∞⁺ : ∀ {i} → hom-P∞-Coalg (V∞∞ i , desup∞∞⁺) (V∞∞ (lsuc i) , desup∞∞)
pr1 (hom-corec∞⁺ {i}) = corec∞⁺ i
pr2 hom-corec∞⁺ = is-hom-corec∞⁺

is-emb-corec∞⁺ : ∀ {i} → is-emb (corec∞⁺ i)
is-emb-corec∞⁺ {i} x y =
  is-fiberwise-equiv-hom-is-terminal-⦉ E' (V∞∞ i , desup∞∞) ⦊-Coalg
    (==-E'-Coalg (V∞∞ i , desup∞∞))
    (R , m)
    ((λ (x , y) → ap (corec∞⁺ i) {x} {y}) , is-hom-ap-corec∞⁺)
    is-terminal-R-m
    ==-E'-Coalg-is-terminal
    (x , y)
  where
    R : V∞∞ i × V∞∞ i → UU (lsuc (lsuc i))
    R (x , y) = corec∞⁺ i x == corec∞⁺ i y

    e : ((x , y) : V∞∞ i × V∞∞ i)
      → ⦉ E' (V∞∞ (lsuc i) , desup∞∞) ⦊ (uncurry _==_) (corec∞⁺ i x , corec∞⁺ i y)
      ≃ ⦉ E' (V∞∞ i , desup∞∞⁺) ⦊ R (x , y)
    e = equiv-E'-precomp-hom (V∞∞ i , desup∞∞⁺) (V∞∞ (lsuc i) , desup∞∞) hom-corec∞⁺ (uncurry _==_)

    e' : ∀ {j} (R' : V∞∞ i × V∞∞ i → UU j)
       → ((x , y) : V∞∞ i × V∞∞ i)
       → ⦉ E' (V∞∞ i , desup∞∞⁺) ⦊ R' (x , y)
       ≃ ⦉ E' (V∞∞ i , desup∞∞) ⦊ R' (x , y)
    e' R' (x , y) =
      equiv-Σ _
        (equiv-postcomp-equiv (inv-equiv (compute-raise (lsuc i) (pr1 (desup∞∞ y)))) _ ∘e
         equiv-precomp-equiv (compute-raise (lsuc i) (pr1 (desup∞∞ x))) _)
        (λ e → equiv-precomp-Π (compute-raise (lsuc i) (pr1 (desup∞∞ x))) _)

    m : ((x , y) : V∞∞ i × V∞∞ i) → R (x , y) → ⦉ E' (V∞∞ i , desup∞∞) ⦊ R (x , y)
    m (x , y) =
      map-equiv (e' R (x , y)) ∘
      map-equiv (e (x , y)) ∘
      pr2 (==-E'-Coalg (V∞∞ (lsuc i) , desup∞∞)) (corec∞⁺ i x , corec∞⁺ i y)

    is-hom-ap-corec∞⁺ : is-hom-⦉ E' (V∞∞ i , desup∞∞) ⦊-Coalg
                          (==-E'-Coalg (V∞∞ i , desup∞∞))
                          (R , m)
                          (λ (x , y) → ap (corec∞⁺ i) {x} {y})
    is-hom-ap-corec∞⁺ (x , .x) refl =
      ap (map-equiv (e' R (x , x)))
         (compute-==-equiv-E'-precomp-hom (V∞∞ i , desup∞∞⁺) (V∞∞ (lsuc i) , desup∞∞) hom-corec∞⁺ x)

    is-terminal-R-m : ∀ {l} → is-terminal-⦉ E' (V∞∞ i , desup∞∞) ⦊-Coalg (R , m) l
    is-terminal-R-m (R' , σ) =
      is-contr-equiv
        (hom-⦉ E' (V∞∞ i , desup∞∞⁺) ⦊-Coalg
          (R' , λ (x , y) → map-inv-equiv (e' R' (x , y)) ∘ σ (x , y))
          (R , λ (x , y) → map-equiv (e (x , y)) ∘ pr2 (==-E'-Coalg (V∞∞ (lsuc i) , desup∞∞)) (corec∞⁺ i x , corec∞⁺ i y)))
        (equiv-tot (λ f →
          equiv-Π-equiv-family (λ (x , y) →
            equiv-Π-equiv-family (λ r →
              inv-equiv (equiv-ap (e' R (x , y)) _ _)) ∘e
            equiv-concat-htpy' (m (x , y) ∘ f (x , y))
              ((map-⦉ E' (V∞∞ i , desup∞∞) ⦊ f (x , y)) ·l
              (inv-htpy (is-section-map-inv-equiv (e' R' (x , y))) ·r (σ (x , y)))))))
        (is-terminal-E'-Coalg-==-f×f
          (V∞∞ i , desup∞∞⁺)
          (V∞∞ (lsuc i) , desup∞∞)
          hom-corec∞⁺
          ==-E'-Coalg-is-terminal
          (R' , λ (x , y) → map-inv-equiv (e' R' (x , y)) ∘ σ (x , y)))


-- V∞∞ i embeds into V∞∞ (lsuc i)
V∞∞↪V∞∞⁺ : ∀ i → V∞∞ i ↪ V∞∞ (lsuc i)
pr1 (V∞∞↪V∞∞⁺ i) = corec∞⁺ i
pr2 (V∞∞↪V∞∞⁺ i) = is-emb-corec∞⁺


{-
Given f : X → V∞ⁿ we can construct the following diagram:

           f              pr1
    X ----------> V∞ⁿ ----------> V∞∞
    |              |               |
  m |     (I)      | desup∞ⁿ       |
    v              v               |
  Pⁿ i X -----> Pⁿ i V∞ⁿ           | desup∞∞
    |   map-Pⁿ f   |               |
    |              |               |
    v              v               v
  P∞ i X -----> P∞ i V∞ⁿ -----> P∞ i V∞∞
        map-P∞ f        map-P∞ pr1

If f is an (n-1)-truncated map, then the square (I) commutes if and only if the outer square commutes,
i.e f is a Pⁿ-coalgebra homomorphism into V∞ⁿ if and only if it is a
P∞-coalgebra homomorphism into V∞∞.
-}

module _ {j} (n : ℕ)
  ((X , m) : P-[ n ]-Coalg i j)
  (f : trunc-map (minus-one n) X (V∞-[ n ] i))
  where

  equiv-is-hom-P∞-Coalg-V∞∞-is-hom-P-Coalg-V∞-[_] :
    is-hom-P∞-Coalg (P-[ n ]-to-P∞-Coalg (X , m)) (V∞∞ i , desup∞∞) (pr1 ∘ map-trunc-map f) ≃
    is-hom-P-[ n ]-Coalg (X , m) (V∞-[ n ] i , desup∞-[ n ]) is-[succ-ℕ n ]-small-V∞-[ n ] (map-trunc-map f)
  equiv-is-hom-P∞-Coalg-V∞∞-is-hom-P-Coalg-V∞-[_] =
    equivalence-reasoning
        (desup∞∞ ∘ pr1 ∘ map-trunc-map f
        ~ map-P∞ (pr1 ∘ map-trunc-map f) ∘ P-[ n ]-to-P∞-Coalg (X , m) ↓)

      ≃ (map-P∞ pr1 ∘ P-[ n ]-to-P∞ (V∞-[ n ] i)
          ∘ desup∞-[ n ] ∘ map-trunc-map f
        ~ map-P∞ (pr1 ∘ map-trunc-map f) ∘ P-[ n ]-to-P∞-Coalg (X , m) ↓) by id-equiv

      ≃ (map-P∞ pr1 ∘ P-[ n ]-to-P∞ (V∞-[ n ] i)
          ∘ desup∞-[ n ] ∘ map-trunc-map f
        ~ map-P∞ pr1 ∘ P-[ n ]-to-P∞ (V∞-[ n ] i)
          ∘ map-P-[ n ] p (map-trunc-map f) ∘ m)                          by equiv-concat-htpy'
                                                                               (map-P∞ pr1 ∘ P-[ n ]-to-P∞ (V∞-[ n ] i)
                                                                                 ∘ desup∞-[ n ] ∘ map-trunc-map f)
                                                                               (htpy-left-whisk
                                                                                 (map-P∞ pr1)
                                                                                 (htpy-right-whisk
                                                                                   (map-P-[ n ]-map-P-comm-trunc-map-~ p f)
                                                                                   m))
      ≃ (desup∞-[ n ] ∘ map-trunc-map f
        ~ map-P-[ n ] p (map-trunc-map f) ∘ m)                            by inv-equiv (equiv-htpy-postcomp-emb
                                                                               (comp-emb
                                                                                 (emb-map-P∞ V∞-[ n ]↪V∞∞)
                                                                                 (P-[ n ]↪P∞ (V∞-[ n ] i)))
                                                                               (desup∞-[ n ] ∘ map-trunc-map f)
                                                                               (map-P-[ n ] p (map-trunc-map f) ∘ m))
    where
      p = is-[succ-ℕ n ]-small-V∞-[ n ]

{- The type of P-[ n ]-coalgebra homomorphisms from (X , m) to (V∞-[ n ] , desup∞-[ n ])
which are (n-1)-truncated maps, is a subtype of the type of P∞-coalgebra homomorphisms
from (X , m) to (V∞∞ , desup∞∞) -}
emb-hom-P-[_]-Coalg-hom-P∞-Coalg :
  ∀ {j} (n : ℕ) ((X , m) : P-[ n ]-Coalg i j)
  → Σ (hom-P-[ n ]-Coalg (X , m) (V∞-[ n ] i , desup∞-[ n ]) is-[succ-ℕ n ]-small-V∞-[ n ])
      (λ (f' , _) → is-trunc-map (minus-one n) f')
  ↪ hom-P∞-Coalg (P-[ n ]-to-P∞-Coalg (X , m)) (V∞∞ i , desup∞∞)
emb-hom-P-[ n ]-Coalg-hom-P∞-Coalg (X , m) =
  comp-emb
    (emb-Σ-emb-base
      (pr1 ∘_ , is-mono-is-emb pr1 (is-emb-inclusion-subtype is-coiterative-[ minus-one n ]-type-Prop) _)
      _)
    (comp-emb
      (emb-subtype
        (is-trunc-map-Prop (minus-one n) ∘ pr1))
      (comp-emb
        (emb-equiv equiv-right-swap-Σ)
        (comp-emb
          (emb-tot (λ f' → emb-equiv (inv-equiv (equiv-is-hom-P∞-Coalg-V∞∞-is-hom-P-Coalg-V∞-[ n ] (X , m) f'))))
          (emb-equiv equiv-right-swap-Σ))))

{-
Given a P-[ n ]-coalgebra (X , m) for which corec∞ is an (n-1)-truncated map,
the type of P-[ n ]-coalgebras into V∞-[ n ] which are (n-1)-truncated maps
is contractible.
-}

module _ (n : ℕ) {j} ((X , m) : P-[ n ]-Coalg i j)
  (is-trunc-map-corec∞ : is-trunc-map (minus-one n) (corec∞ (P-[ n ]-to-P∞-Coalg (X , m))))
  where

  private
    corec∞-X : X → V∞∞ i
    corec∞-X = corec∞ (P-[ n ]-to-P∞-Coalg (X , m))

    is-hom-P-Coalg-corec∞-X :
      is-hom-P∞-Coalg
        (P-[ n ]-to-P∞-Coalg (X , m))
        (V∞∞ i , desup∞∞)
        (corec∞ (P-[ n ]-to-P∞-Coalg (X , m)))
    is-hom-P-Coalg-corec∞-X = is-hom-P∞-Coalg-corec∞ (P-[ n ]-to-P∞-Coalg (X , m))

  is-coiterative-[_]-type-corec∞ : (x : X)
                                 → is-coiterative-[ minus-one n ]-type (corec∞-X x)
  is-coiterative-[_]-type-corec∞ x zero-ℕ =
    inv-tr
      (is-trunc-map (minus-one n) ∘ pr2)
      (is-hom-P-Coalg-corec∞-X x)
      (is-trunc-map-comp (minus-one n) corec∞-X (pr1 (m x ↓)) is-trunc-map-corec∞ (pr2 (m x ↓)))
  is-coiterative-[_]-type-corec∞ x (succ-ℕ k) =
    inv-tr
      (λ (X' , m') → (a : X') → is-[ k ]-coiterative-[ minus-one n ]-type (m' a))
      (is-hom-P-Coalg-corec∞-X x)
      (λ a → is-coiterative-[_]-type-corec∞ ((m x ↓) 〈 a 〉) k)

  corec-[_] : X → V∞-[ n ] i
  pr1 (corec-[_] x) = corec∞-X x
  pr2 (corec-[_] x) = is-coiterative-[_]-type-corec∞ x

  is-trunc-map-corec-[_] : is-trunc-map (minus-one n) corec-[_]
  is-trunc-map-corec-[_] =
    is-trunc-map-into-subtype (minus-one n)
      is-coiterative-[ minus-one n ]-type-Prop
      is-trunc-map-corec∞
      is-coiterative-[_]-type-corec∞

  trunc-map-corec-[_] : trunc-map (minus-one n) X (V∞-[ n ] i)
  pr1 trunc-map-corec-[_] = corec-[_]
  pr2 trunc-map-corec-[_] = is-trunc-map-corec-[_]

  is-hom-P-[_]-Coalg-corec :
    is-hom-P-[ n ]-Coalg
      (X , m)
      (V∞-[ n ] i , desup∞-[ n ])
      is-[succ-ℕ n ]-small-V∞-[ n ]
      corec-[_]
  is-hom-P-[_]-Coalg-corec =
    map-equiv
     (equiv-is-hom-P∞-Coalg-V∞∞-is-hom-P-Coalg-V∞-[ n ] (X , m)
      trunc-map-corec-[_])
     is-hom-P-Coalg-corec∞-X

  is-hom-P∞-Coalg-corec-[_] :
    is-hom-P∞-Coalg
      (P-[ n ]-to-P∞-Coalg (X , m))
      (P-[ n ]-to-P∞-Coalg (V∞-[ n ] i , desup∞-[ n ]))
      corec-[_]
  is-hom-P∞-Coalg-corec-[_] x =
    ap (P-[ n ]-to-P∞ (V∞-[ n ] i)) (is-hom-P-[_]-Coalg-corec x) ∙
    inv (map-P-[ n ]-map-P-comm-trunc-map-~ is-[succ-ℕ n ]-small-V∞-[ n ] trunc-map-corec-[_] (m x))

  hom-P-[_]-Coalg-corec :
    hom-P-[ n ]-Coalg
      (X , m)
      (V∞-[ n ] i , desup∞-[ n ])
      is-[succ-ℕ n ]-small-V∞-[ n ]
  pr1 hom-P-[_]-Coalg-corec = corec-[_]
  pr2 hom-P-[_]-Coalg-corec = is-hom-P-[_]-Coalg-corec

  V∞-[_]-is-terminal-wrt-trunc-maps :
    is-contr
      (Σ (hom-P-[ n ]-Coalg (X , m) (V∞-[ n ] i , desup∞-[ n ]) is-[succ-ℕ n ]-small-V∞-[ n ])
         (is-trunc-map (minus-one n) ∘ pr1))
  V∞-[_]-is-terminal-wrt-trunc-maps =
    is-proof-irrelevant-is-prop
      (is-prop-emb
        (emb-hom-P-[ n ]-Coalg-hom-P∞-Coalg (X , m))
        (is-prop-is-contr (V∞∞-is-terminal (P-[ n ]-to-P∞-Coalg (X , m)))))
      (hom-P-[_]-Coalg-corec , is-trunc-map-corec-[_])

  corec-[_]-unique :
    (f : hom-P-[ n ]-Coalg
           (X , m)
           (V∞-[ n ] i , desup∞-[ n ])
           is-[succ-ℕ n ]-small-V∞-[ n ])
    → is-trunc-map (minus-one n) (pr1 f)
    → corec-[_] == pr1 f
  corec-[_]-unique (f , is-hom-f) is-trunc-map-f =
    ap (pr1 ∘ pr1)
       (eq-is-contr'
         V∞-[_]-is-terminal-wrt-trunc-maps
         (hom-P-[_]-Coalg-corec , is-trunc-map-corec-[_])
         ((f , is-hom-f) , is-trunc-map-f))


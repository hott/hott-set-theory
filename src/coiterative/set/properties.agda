{-# OPTIONS --without-K --lossy-unification #-}

open import elementary-number-theory.addition-natural-numbers
open import elementary-number-theory.inequality-natural-numbers
open import elementary-number-theory.natural-numbers
open import elementary-number-theory.strict-inequality-natural-numbers

open import foundation.action-on-identifications-functions
open import foundation.cartesian-product-types
open import foundation.contractible-types
open import foundation.dependent-pair-types
  renaming (ind-Σ to uncurry; ev-pair to curry)
open import foundation.dependent-universal-property-equivalences
open import foundation.embeddings
open import foundation.equality-cartesian-product-types
open import foundation.equality-dependent-pair-types
open import foundation.equivalences
open import foundation.equivalence-induction
open import foundation.families-of-equivalences
open import foundation.fibers-of-maps
open import foundation.function-extensionality
open import foundation.function-types
open import foundation.functoriality-cartesian-product-types
open import foundation.functoriality-dependent-function-types
open import foundation.functoriality-dependent-pair-types
open import foundation.functoriality-truncation
open import foundation.homotopies
open import foundation.identity-types
open import foundation.injective-maps
open import foundation.postcomposition-functions
open import foundation.propositional-maps
open import foundation.propositions
open import foundation.structure-identity-principle
open import foundation.subtypes
open import foundation.transport-along-identifications
open import foundation.truncated-maps
open import foundation.truncated-types
open import foundation.truncations
open import foundation.truncation-levels
  renaming(truncation-level-ℕ to to-𝕋;
    truncation-level-minus-one-ℕ to minus-one)
open import foundation.type-arithmetic-dependent-pair-types
open import foundation.universal-property-dependent-pair-types
  renaming (equiv-ind-Σ to equiv-uncurry; equiv-ev-pair to equiv-curry)
open import foundation.universe-levels
open import foundation.whiskering-homotopies

open import container.indexed
open import container.indexed.coalgebra
open import coiterative.infinity-multiset
open import coiterative.set
open import e-structure.core
open import functor.n-slice
open import functor.slice
open import image-factorisation
open import notation

module coiterative.set.properties (i : Level) (n : ℕ) where

-- Since V∞ⁿ is a fixpoint for Pⁿ, it has an ∈-structure
open import fixed-point.core n (V∞-[ n ] i) fixed-point-V∞-[ n ]


-- The following properties hold for V∞ⁿ

-- Replacement
open import e-structure.property.replacement ∈-structure-V

replacement : (k : ℕ) → k ≤-ℕ n → [ k ]-Replacement
replacement k k≤n = n-replacement
  where
    H : Σ ℕ λ l → add-ℕ l k == n
    H = subtraction-leq-ℕ k n k≤n

    open import fixed-point.replacement
      n k (pr1 H) (inv (pr2 H)) (V∞-[ n ] i)
      fixed-point-V∞-[ n ] is-[succ-ℕ k ]-small-V∞-[ n ]

-- Restricted separation
open import e-structure.property.restricted-separation ∈-structure-V

restricted-separation : Restricted[ n ]-Separation i
restricted-separation = restricted-n-separation
  where
    open import fixed-point.restricted-separation
      n (V∞-[ n ] i) fixed-point-V∞-[ n ]

-- Union
open import e-structure.property.union ∈-structure-V

union : (k : ℕ) → k ≤-ℕ n → [ k ]-Union
union k k≤n = n-union
  where
    H : Σ ℕ λ l → add-ℕ l k == n
    H = subtraction-leq-ℕ k n k≤n

    open import fixed-point.union
      n k (pr1 H) (inv (pr2 H)) (V∞-[ n ] i)
      fixed-point-V∞-[ n ] is-[succ-ℕ k ]-small-V∞-[ n ]

-- Unordered tupling
open import e-structure.property.unordered-tupling ∈-structure-V

unordered-tupling : (k : ℕ) → k ≤-ℕ n
                  → (I : UU i)
                  → [ k ]-UnorderedTupling I
unordered-tupling k k≤n = n-unordered-tupling
  where
    H : Σ ℕ λ l → add-ℕ l k == n
    H = subtraction-leq-ℕ k n k≤n

    open import fixed-point.unordered-tupling
      n k (pr1 H) (inv (pr2 H)) (V∞-[ n ] i)
      fixed-point-V∞-[ n ] is-[succ-ℕ k ]-small-V∞-[ n ]

∞-unordered-tupling : (k : ℕ) → k <-ℕ n
                    → (I : Truncated-Type i (to-𝕋 k))
                    → ∞-UnorderedTupling (type-Truncated-Type I)
∞-unordered-tupling = ∞-unordered-tupling'
  where
    open import fixed-point.unordered-tupling
      n n 0 (commutative-add-ℕ n 0) (V∞-[ n ] i)
      fixed-point-V∞-[ n ] is-[succ-ℕ n ]-small-V∞-[ n ]
    open ∞-unordered-tupling-from-k=0 refl
      renaming (∞-unordered-tupling to ∞-unordered-tupling')

-- Exponentiation
open ordered-pairing-structure (V∞-[ n ] i)

-- We can construct ordered pairs
ordered-pairs : OrderedPairs
ordered-pairs = ordered-pairs'
  where
    open import fixed-point.unordered-tupling
      n n 0 (commutative-add-ℕ n 0) (V∞-[ n ] i)
      fixed-point-V∞-[ n ] is-[succ-ℕ n ]-small-V∞-[ n ]
    open ordered-pairs-from-k=0 refl
      renaming (ordered-pairs to ordered-pairs')

open ordered-pairing-structure.notation (V∞-[ n ] i) ordered-pairs

open import e-structure.property.exponentiation ∈-structure-V

exponentiation : (ordered-pairs' : OrderedPairs) → Exponentiation ordered-pairs'
exponentiation ordered-pairs' = exponentiation''
  where
    open import fixed-point.exponentiation
      n (V∞-[ n ] i) fixed-point-V∞-[ n ] ordered-pairs'
      renaming (exponentiation to exponentiation'')

-- Natural numbers with respect to any truncated representation.
-- in particular with respect to the von Neumann encoding
open import e-structure.property.natural-numbers ∈-structure-V
open import e-structure.internalisations ∈-structure-V

natural-numbers-trunc-repr : (f : Representation ℕ)
                           → is-trunc-map (minus-one n) f
                           → NaturalNumbersRepresentedBy f
natural-numbers-trunc-repr = natural-numbers-trunc-repr'
  where
    open import fixed-point.natural-numbers
      n (V∞-[ n ] i) fixed-point-V∞-[ n ]
      renaming (natural-numbers-trunc-repr to natural-numbers-trunc-repr')

open import fixed-point.empty-set n (V∞-[ n ] i) fixed-point-V∞-[ n ]
open import fixed-point.unordered-tupling
  n zero-ℕ n refl (V∞-[ n ] i) fixed-point-V∞-[ n ] is-locally-small-V∞-[ n ]
open import fixed-point.union
  n zero-ℕ n refl (V∞-[ n ] i) fixed-point-V∞-[ n ] is-locally-small-V∞-[ n ]
open von-neumann empty-set singletons binary-union

natural-numbers-von-neumann : NaturalNumbersRepresentedBy von-neumann-repr
natural-numbers-von-neumann = natural-numbers-von-neumann' is-locally-small-V∞-[ n ]
  where
    open import fixed-point.natural-numbers
      n (V∞-[ n ] i) fixed-point-V∞-[ n ]
      renaming (natural-numbers-von-neumann to natural-numbers-von-neumann')

-- Moreover, V∞ⁿ satisfies Scott's anti-foundation axiom
open import e-structure.graphs ∈-structure-V ordered-pairs
open import e-structure.property.scott-anti-foundation
  ∈-structure-V ordered-pairs

-- Import construction of Pⁿ-coalgebra from a graph
open import e-structure.graph.to-P-n-coalgebra
  n (V∞-[ n ] i , desup∞-[ n ]) (is-emb-is-equiv (pr2 fixed-point-V∞-[ n ]))
  ordered-pairs is-locally-small-V∞-[ n ]


-- Utilities
is-trunc-map-add-ℕ' : ∀ {i j} {X : UU i} {Y : UU j} {f : X → Y}
                    → {k n l : ℕ} → add-ℕ' l k == n
                    → is-trunc-map (minus-one k) f → is-trunc-map (minus-one n) f
is-trunc-map-add-ℕ' {l = zero-ℕ} refl = id
is-trunc-map-add-ℕ' {f = f} {l = succ-ℕ l} p H =
  tr (λ n' → is-trunc-map (minus-one n') f) p
    (is-trunc-map-succ-is-trunc-map _
      (is-trunc-map-add-ℕ' refl H))

is-trunc-map-add-ℕ : ∀ {i j} {X : UU i} {Y : UU j} {f : X → Y}
                   → {k n l : ℕ} → add-ℕ l k == n
                   → is-trunc-map (minus-one k) f → is-trunc-map (minus-one n) f
is-trunc-map-add-ℕ {k = k} {l = l} p =
  is-trunc-map-add-ℕ' (commutative-add-ℕ k l ∙ p)

is-trunc-map-leq-ℕ : ∀ {i j} {X : UU i} {Y : UU j} {f : X → Y}
                   → {k n : ℕ} → k ≤-ℕ n
                   → is-trunc-map (minus-one k) f → is-trunc-map (minus-one n) f
is-trunc-map-leq-ℕ {k = k} {n} k≤n =
  is-trunc-map-add-ℕ (pr2 (subtraction-leq-ℕ k n k≤n))


private
  abstract
    tr-lemma1 : ∀ {ℓ₁} {X : UU ℓ₁} {x x' y y' : X}
              → (p : x == y) (q : x' == y') (r : x == x')
              → tr (uncurry _==_) (eq-pair p q) r == (inv p ∙ r ∙ q)
    tr-lemma1 refl refl r = inv right-unit

    tr-lemma2 : ∀ {ℓ₁} {X : UU ℓ₁} {x y y' : X}
              → (p : x == y) (q : x == y')
              → tr (uncurry _==_) (eq-pair p q) refl == (inv p ∙ q)
    tr-lemma2 refl q = tr-lemma1 refl q refl

    inv-eq-type-subtype : ∀ {ℓ₁ ℓ₂} {X : UU ℓ₁} (P : X → Prop ℓ₂)
                        → {s t : type-subtype P}
                        → (p : pr1 s == pr1 t)
                        → inv (eq-type-subtype P {s} {t} p)
                        == eq-type-subtype P {t} {s} (inv p)
    inv-eq-type-subtype P {s} {t} p =
      ap (inv ∘ eq-type-subtype P {s} {t})
         (inv (is-section-map-inv-equiv (e s t) p)) ∙
      ap (inv ∘ eq-type-subtype P {s} {t})
         (is-section-map-inv-equiv (e s t) p) ∙
      inv (is-retraction-map-inv-equiv
            (e t s)
            (inv (eq-type-subtype P {s} {t} p))) ∙
      ap (eq-type-subtype P {t} {s})
         (α s t (eq-type-subtype P {s} {t} p)) ∙
      ap (eq-type-subtype P {t} {s} ∘ inv)
         (is-section-map-inv-equiv (e s t) p)
      where
        e : (a b : type-subtype P) → (a ＝ b) ≃ (pr1 a ＝ pr1 b)
        e = extensionality-type-subtype' P

        α : (a b : type-subtype P) (r : a == b)
          → map-equiv (e b a) (inv r) == inv (map-equiv (e a b) r)
        α a .a refl = refl

    concat-eq-type-subtype : ∀ {ℓ₁ ℓ₂} {X : UU ℓ₁} (P : X → Prop ℓ₂)
                           → {s t u : type-subtype P}
                           → (p : pr1 s == pr1 t) (q : pr1 t == pr1 u)
                           → (eq-type-subtype P {s} {t} p ∙ eq-type-subtype P {t} {u} q)
                           == eq-type-subtype P (p ∙ q)
    concat-eq-type-subtype P {s} {t} {u} p q =
      inv (is-retraction-map-inv-equiv
            (e s u)
            (eq-type-subtype P {s} {t} p ∙ eq-type-subtype P {t} {u} q)) ∙
      ap (eq-type-subtype P {s} {u})
         (α s t u (eq-type-subtype P {s} {t} p) (eq-type-subtype P {t} {u} q)) ∙
      ap (λ (x , y) → eq-type-subtype P {s} {u} (x ∙ y))
         (eq-pair
           (is-section-map-inv-equiv (e s t) p)
           (is-section-map-inv-equiv (e t u) q))
      where
        e : (a b : type-subtype P) → (a ＝ b) ≃ (pr1 a ＝ pr1 b)
        e = extensionality-type-subtype' P

        α : (a b c : type-subtype P) (v : a == b) (w : b == c)
          → map-equiv (e a c) (v ∙ w)
          == (map-equiv (e a b) v ∙ map-equiv (e b c) w)
        α a .a b refl v = refl

    lemma1 : ∀ {ℓ₁ ℓ₂} {A : UU ℓ₁} {B : UU ℓ₂} (f : A → B)
           → ((g , α) (h , β) : Σ (A → A) (λ g → f ~ f ∘ g))
           → ((g , α) == (h , β))
           ≃ Σ (g ~ h) (λ H → (a : A) → α a == (β a ∙ inv (ap f (H a))))
    lemma1 {A = A} f (g , α) =
      extensionality-Σ
        (λ β H → (a : A) → α a == (β a ∙ inv (ap f (H a))))
        refl-htpy
        (λ a → inv right-unit)
        (λ h → equiv-funext)
        (λ β → equiv-Π-equiv-family (λ a → equiv-concat' (α a) (inv right-unit)) ∘e
               equiv-funext)

    lemma2 : ∀ {ℓ₁ ℓ₂ ℓ₃ ℓ₄ ℓ₅} {A : UU ℓ₁} {B : UU ℓ₂} {C : UU ℓ₃} {D : C → UU ℓ₄} (P : B → Prop ℓ₅)
          → (f : A → B) (g : Σ C D → B) (e : A ≃ Σ C D) (H : (g ∘ map-equiv e) == f)
          → (pf : (a : A) → pr1 (P (f a))) (pg : (s : Σ C D) → pr1 (P (g s)))
          → Id {A = Σ (A ≃ A) (λ e' → (a : A) → (f a , pf a) == (f (map-equiv e' a) , pf (map-equiv e' a)))}
               (inv-equiv e ∘e id-equiv ∘e e ,
               (λ a →
                 eq-type-subtype P
                   {(f a , pf a)}
                   {(f (map-inv-equiv e (map-equiv e a)) , pf (map-inv-equiv e (map-equiv e a)))}
                   (tr (λ h → h a == h (map-inv-equiv e (map-equiv e a))) H
                      (ap g (inv (is-section-map-inv-equiv e (map-equiv e a)))))))
               (id-equiv , refl-htpy)
    lemma2 P f g e refl pf pg =
      ap map-right-swap-Σ
         (eq-type-subtype
           (is-equiv-Prop ∘ pr1)
           (map-inv-equiv-ap
             (equiv-tot λ h → equiv-Π-equiv-family (λ a →
               extensionality-type-subtype' P
                 (f a , pf a)
                 (f (h a) , pf (h a))))
             (map-inv-equiv e ∘ map-equiv e ,
               (λ a →
                  eq-type-subtype P
                  (ap g (inv (is-section-map-inv-equiv e (map-equiv e a))))))
             (id , refl-htpy)
             (eq-pair-eq-pr2 (eq-htpy (λ a →
               is-section-map-inv-equiv
                 (extensionality-type-subtype' P
                   (f a , pf a)
                   (f (map-inv-equiv e (map-equiv e a)) ,
                    pf (map-inv-equiv e (map-equiv e a))))
                 (ap g (inv (is-section-map-inv-equiv e (map-equiv e a)))) ∙
                 ap-inv g (is-section-map-inv-equiv e (map-equiv e a)) ∙
                 ap (inv ∘ ap g)
                    (coherence-map-inv-equiv e a) ∙
                 ap inv (inv (ap-comp g (map-equiv e) (is-retraction-map-inv-equiv e a))))) ∙
             map-inv-equiv
               (lemma1 f
                 (map-inv-equiv e ∘ map-equiv e ,
                 λ a → inv (ap f (is-retraction-map-inv-equiv e a)))
                 (id , refl-htpy))
               (is-retraction-map-inv-equiv e , refl-htpy))))


module _ (G@(g , is-graph-g) : Graph) where

  equiv-is-bisim-simple-graph-is-terminal-E'-large-==-Coalg : ∀ k
                                                            → is-bisim-simple-graph k G
                                                            ≃ is-terminal-⦉ E' (Graph-sub-Coalg G) ⦊-Coalg (==-E'-Coalg (Graph-sub-Coalg G)) k
  equiv-is-bisim-simple-graph-is-terminal-E'-large-==-Coalg k =
    equiv-Π-equiv-family (λ (R , σ) →
      equiv-is-contr-equiv
        (equiv-Σ _ equiv-uncurry (λ _ → equiv-uncurry))) ∘e
    equiv-precomp-Π
      (equiv-Σ _ equiv-curry λ _ → equiv-curry)
      _

  equiv-is-terminal-E'-large-small-==-Coalg : ∀ k
                                            → is-terminal-⦉ E' (Graph-sub-Coalg G) ⦊-Coalg (==-E'-Coalg (Graph-sub-Coalg G)) k
                                            ≃ is-terminal-⦉ E' (graph-P∞-sub-Coalg G) ⦊-Coalg (==-E'-Coalg (graph-P∞-sub-Coalg G)) k
  equiv-is-terminal-E'-large-small-==-Coalg k =
    equiv-tr (λ C → is-terminal-⦉ E' (graph-P∞-sub-Coalg G) ⦊-Coalg C k) compute-equiv ∘e
    equiv-is-terminal-⦉ E' (Graph-sub-Coalg G) ⦊-⦉ E' (graph-P∞-sub-Coalg G) ⦊-Coalg
      eA eB er (==-E'-Coalg (Graph-sub-Coalg G))
    where
      eA : (((x , _) , (y , _)) : Target G × Target G)
         → (Child G x ≃ Child G y)
         ≃ (index-children G x ≃ index-children G y)
      eA ((x , _) , (y , _)) =
        equiv-postcomp-equiv (inv-equiv (equiv-index-children G y)) _ ∘e
        equiv-precomp-equiv (equiv-index-children G x) _

      eB : (((x , _) , (y , _)) : Target G × Target G)
         → (e : Child G x ≃ Child G y)
         → index-children G x ≃ Child G x
      eB ((x , _) , (y , _)) _ = equiv-index-children G x

      er : (((x , _) , (y , _)) : Target G × Target G)
         → (e : Child G x ≃ Child G y)
         → (a : index-children G x)
         →  (Target-Child G x (map-equiv (equiv-index-children G x) a) ,
             Target-Child G y (map-equiv e (map-equiv (equiv-index-children G x) a)))
         == (sub-child G x a ,
             sub-child G y (map-inv-equiv (equiv-index-children G y)
               (map-equiv e (map-equiv (equiv-index-children G x) a))))
      er ((x , p) , (y , q)) e a =
        eq-pair
          (eq-type-subtype (is-target-Prop G) refl)
          (eq-type-subtype
            (is-target-Prop G)
            (triangle-equiv-index-children G y
              (map-equiv e (map-equiv (equiv-index-children G x) a))))

      compute-er : ((x , p) : Target G) (a : index-children G x)
                 → tr (uncurry _==_) (er ((x , p) , (x , p)) id-equiv a) refl
                 == eq-type-subtype
                      (is-target-Prop G)
                      {sub-child G x a}
                      {sub-child G x (map-inv-equiv (equiv-index-children G x)
                        (map-equiv (equiv-index-children G x) a))}
                      (triangle-equiv-index-children G x
                           (map-equiv (equiv-index-children G x) a))
      compute-er (x , p) a =
        tr-lemma2
          (eq-type-subtype (is-target-Prop G) refl)
          (eq-type-subtype
            (is-target-Prop G)
            (triangle-equiv-index-children G x
              (map-equiv (equiv-index-children G x) a))) ∙
        ap (_∙ eq-type-subtype
                 (is-target-Prop G)
                 (triangle-equiv-index-children G x
                   (map-equiv (equiv-index-children G x) a)))
           (inv-eq-type-subtype (is-target-Prop G) refl) ∙
        concat-eq-type-subtype
          (is-target-Prop G)
          refl
          (triangle-equiv-index-children G x
            (map-equiv (equiv-index-children G x) a))

      compute-pr2-equiv : (((x , p) , (y , q)) : Target G × Target G)
                        → (s : (x , p) == (y , q))
                        → map-equiv
                            (equiv-⦉ E' (Graph-sub-Coalg G) ⦊-⦉ E' (graph-P∞-sub-Coalg G) ⦊ eA eB er
                            (uncurry _==_) ((x , p) , (y , q)))
                            (pr2 (==-E'-Coalg (Graph-sub-Coalg G)) ((x , p) , (y , q)) s)
                        == pr2 (==-E'-Coalg (graph-P∞-sub-Coalg G)) ((x , p) , (y , q)) s
      compute-pr2-equiv ((x , p) , .(x , p)) refl =
        eq-pair-eq-pr2 (eq-htpy (λ a → compute-er (x , p) a)) ∙
        lemma2
          (is-target-Prop G)
          (child G x)
          (pr1 ∘ Target-Child G x)
          (equiv-index-children G x)
          refl
          (is-target-child G x)
          (is-target-Child G x)

      compute-equiv : map-equiv
                        (equiv-⦉ E' (Graph-sub-Coalg G) ⦊-⦉ E' (graph-P∞-sub-Coalg G) ⦊-Coalg eA eB er)
                        (==-E'-Coalg (Graph-sub-Coalg G))
                    == (==-E'-Coalg (graph-P∞-sub-Coalg G))
      compute-equiv = eq-pair-eq-pr2 (eq-htpy (eq-htpy ∘ compute-pr2-equiv))


  equiv-is-bisim-simple-graph-is-terminal-small-E'-==-Coalg : ∀ k
                                                            → is-bisim-simple-graph k G
                                                            ≃ is-terminal-⦉ E' (graph-P∞-sub-Coalg G) ⦊-Coalg (==-E'-Coalg (graph-P∞-sub-Coalg G)) k
  equiv-is-bisim-simple-graph-is-terminal-small-E'-==-Coalg  k =
    equiv-is-terminal-E'-large-small-==-Coalg k ∘e
    equiv-is-bisim-simple-graph-is-terminal-E'-large-==-Coalg k

  is-emb-corec∞-is-terminal-E'-==-Coalg : (∀ {l} → is-terminal-⦉ E' (graph-P∞-sub-Coalg G) ⦊-Coalg (==-E'-Coalg (graph-P∞-sub-Coalg G)) l)
                                        → is-emb (corec∞ (graph-P∞-sub-Coalg G))
  is-emb-corec∞-is-terminal-E'-==-Coalg H x y =
    is-fiberwise-equiv-hom-is-terminal-⦉ E' (graph-P∞-sub-Coalg G) ⦊-Coalg
      (==-E'-Coalg (graph-P∞-sub-Coalg G))
      ((uncurry _==_ ∘ ⟨ corec∞ (graph-P∞-sub-Coalg G) × corec∞ (graph-P∞-sub-Coalg G) ⟩) ,
      (λ (x , y) →
        map-equiv
          (equiv-E'-precomp-hom
            (graph-P∞-sub-Coalg G)
            (V∞∞ i , desup∞∞)
            (hom-P∞-Coalg-corec∞ (graph-P∞-sub-Coalg G))
            (uncurry _==_)
            (x , y)) ∘
        pr2 (==-E'-Coalg (V∞∞ i , desup∞∞))
            (corec∞ (graph-P∞-sub-Coalg G) x , corec∞ (graph-P∞-sub-Coalg G) y)))
      (hom-==-E'-Coalg-ap (graph-P∞-sub-Coalg G) (V∞∞ i , desup∞∞) (hom-P∞-Coalg-corec∞ (graph-P∞-sub-Coalg G)))
      (is-terminal-E'-Coalg-==-f×f
        (graph-P∞-sub-Coalg G)
        (V∞∞ i , desup∞∞)
        (hom-P∞-Coalg-corec∞ (graph-P∞-sub-Coalg G))
        ==-E'-Coalg-is-terminal)
      H
      (x , y)

  is-emb-corec∞-is-bisim-simple-graph : (∀ {l} → is-bisim-simple-graph l G)
                                      → is-emb (corec∞ (graph-P∞-sub-Coalg G))
  is-emb-corec∞-is-bisim-simple-graph is-bisim-simple-G =
    is-emb-corec∞-is-terminal-E'-==-Coalg
      (λ {l} → map-equiv (equiv-is-bisim-simple-graph-is-terminal-small-E'-==-Coalg l) is-bisim-simple-G)



hom-graph-P∞-Coalg-Vⁿ∞ : (k : ℕ) → k ≤-ℕ n
                       → ((g , _) : [ k ]-ScottExtensionalGraph)
                       → hom-P∞-Coalg
                           (graph-P∞-Coalg g)
                           (P-[ n ]-to-P∞-Coalg (V∞-[ n ] i , desup∞-[ n ]))
hom-graph-P∞-Coalg-Vⁿ∞ k k≤n (g , is-scott-ext-g) = (ext-d , is-hom-ext-d)
  where
    is-large-hom-corec∞ : is-hom-P∞-Coalg
                            (Graph-sub-Coalg g)
                            (V∞∞ (lsuc i) , desup∞∞) 
                            (corec∞⁺ i ∘ corec∞ (graph-P∞-sub-Coalg g))
    is-large-hom-corec∞ =
      (is-hom-P∞-Coalg-corec∞ (V∞∞ i , desup∞∞⁺) ·r (corec∞ (graph-P∞-sub-Coalg g))) ∙h
      ((map-P∞ (corec∞⁺ i) ∘ raise-P∞ (lsuc i) (V∞∞ i)) ·l is-hom-P∞-Coalg-corec∞ (graph-P∞-sub-Coalg g)) ∙h
      (map-P∞ (corec∞⁺ i ∘ corec∞ (graph-P∞-sub-Coalg g)) ·l htpy-raise-graph-P∞-sub-Coalg-map g)

    is-trunc-map-corec∞ : is-trunc-map (minus-one n) (corec∞ (graph-P∞-sub-Coalg g))
    is-trunc-map-corec∞ =
      is-trunc-map-right-factor (minus-one n)
        (corec∞⁺ i)
        (corec∞ (graph-P∞-sub-Coalg g))
        (is-trunc-map-is-prop-map _ (is-prop-map-is-emb is-emb-corec∞⁺))
        (tr (is-trunc-map (minus-one n))
            (inv (uniqueness-corec∞
                  (Graph-sub-Coalg g)
                  (corec∞⁺ i ∘ corec∞ (graph-P∞-sub-Coalg g) , is-large-hom-corec∞)))
            (is-trunc-map-leq-ℕ k≤n is-scott-ext-g))

    d : Target g → V∞-[ n ] i
    d = corec-[ n ] (graph-Pⁿ-sub-Coalg g) is-trunc-map-corec∞

    is-trunc-map-d : is-trunc-map (minus-one n) d
    is-trunc-map-d = is-trunc-map-corec-[ n ] (graph-Pⁿ-sub-Coalg g) is-trunc-map-corec∞

    is-Pⁿ-hom-d : is-hom-P-[ n ]-Coalg
                    (graph-Pⁿ-sub-Coalg g)
                    (V∞-[ n ] i , desup∞-[ n ])
                    is-[succ-ℕ n ]-small-V∞-[ n ]
                    d
    is-Pⁿ-hom-d = is-hom-P-[ n ]-Coalg-corec (graph-Pⁿ-sub-Coalg g) is-trunc-map-corec∞

    ext-d : V∞-[ n ] i → V∞-[ n ] i
    ext-d x =
      sup∞-[ n ]
        (index-children g x ,
        (d ∘ sub-child g x ,
        is-trunc-map-comp _ d (sub-child g x)
          is-trunc-map-d
          (is-trunc-map-sub-child g x)))

    is-hom-ext-d : is-hom-P∞-Coalg
                     (graph-P∞-Coalg g)
                     (P-[ n ]-to-P∞-Coalg (V∞-[ n ] i , desup∞-[ n ]))
                     ext-d
    is-hom-ext-d x =
      ap (map-emb (P-[ n ]↪P∞ (V∞-[ n ] i))) (desup∞-sup∞-[ n ] _) ∙
      eq-pair-eq-pr2 (eq-htpy (λ a →
        inv (sup∞-desup∞-[ n ] _) ∙
        ap sup∞-[ n ]
           (is-injective-emb
             (P-[ n ]↪P∞ (V∞-[ n ] i))
             (is-hom-P∞-Coalg-corec-[ n ] (graph-Pⁿ-sub-Coalg g) is-trunc-map-corec∞ _))))


scott-anti-foundation₁ : (k : ℕ) → k ≤-ℕ n → [ k ]-ScottAntiFoundation₁
scott-anti-foundation₁ k k≤n (g , is-scott-ext-g) =
  map-equiv (equiv-tot (equiv-is-∞-decoration g))
            (hom-graph-P∞-Coalg-Vⁿ∞ k k≤n (g , is-scott-ext-g))

scott-anti-foundation₂ : ScottAntiFoundation₂
scott-anti-foundation₂ g =
  is-prop-equiv
    (equiv-tot (λ d → inv-equiv (equiv-is-∞-decoration g d)))
    (is-prop-hom-P∞-Coalg-V∞-[ n ] (graph-P∞-Coalg g))

scott-anti-foundation : (k : ℕ) → k ≤-ℕ n → [ k ]-ScottAntiFoundation
scott-anti-foundation k k≤n = (scott-anti-foundation₁ k k≤n , scott-anti-foundation₂)


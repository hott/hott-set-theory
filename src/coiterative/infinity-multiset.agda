{-# OPTIONS --without-K #-}

open import foundation.action-on-identifications-functions
open import foundation.cartesian-product-types
open import foundation.contractible-types
open import foundation.dependent-pair-types
  renaming (ind-Σ to uncurry; ev-pair to curry)
open import foundation.equality-dependent-pair-types
open import foundation.equivalences
open import foundation.function-extensionality
open import foundation.function-types
open import foundation.homotopies
open import foundation.identity-types
open import foundation.locally-small-types
open import foundation.transport-along-identifications
open import foundation.univalence
open import foundation.universe-levels

open import container.bisimulation
open import container.indexed
open import container.indexed.coalgebra
open import functor.slice
open import notation

module coiterative.infinity-multiset where

private
  variable
    i : Level

V∞∞ : ∀ i → UU (lsuc i)
V∞∞ i = M where
  open import container.m-types (UU i , id)

module _ {i : Level} where
  open import container.m-types (UU i , id)
    renaming (desup-M to desup∞∞;
      M-is-terminal to V∞∞-is-terminal;
      fixed-point-M to fixed-point-V∞∞;
      ==-E-Coalg-M to ==-E-Coalg-V∞∞) public

module _ {j} ((X , m) : P∞-Coalg i j) where

  hom-P∞-Coalg-corec∞ : hom-P∞-Coalg (X , m) (V∞∞ i , desup∞∞)
  hom-P∞-Coalg-corec∞ = pr1 (V∞∞-is-terminal (X , m))

  corec∞ : X → V∞∞ i
  corec∞ = pr1 hom-P∞-Coalg-corec∞

  is-hom-P∞-Coalg-corec∞ : is-hom-P∞-Coalg (X , m) (V∞∞ i , desup∞∞) corec∞
  is-hom-P∞-Coalg-corec∞ = pr2 hom-P∞-Coalg-corec∞

  uniqueness-corec∞ : (f : hom-P∞-Coalg (X , m) (V∞∞ i , desup∞∞))
                    → pr1 f == corec∞
  uniqueness-corec∞ f =
    ap pr1
       (eq-is-contr'
         (V∞∞-is-terminal (X , m))
         f
         hom-P∞-Coalg-corec∞)

sup∞∞ : P∞ i (V∞∞ i) → V∞∞ i
sup∞∞ = map-inv-equiv fixed-point-V∞∞

{-
The identity type on V∞∞ is the terminal coalgebra of the
(V∞∞ × V∞∞)-indexed polynomial functor
R (x , y)
  ↦ Σ (e : ¦ desup∞∞ x ¦ ≃ ¦ desup∞∞ y ¦)
       Π (a : ¦ desup∞∞ x ¦) (R (desup∞∞ x ↓ a) (desup∞∞ y ↓ (e a)))
-}

compute-map-eq : ∀ {l} {A : UU l} {B : UU l}
               → (p : A == B)
               → tr id p ~ map-eq p
compute-map-eq refl = refl-htpy

private
  eA : ((x , y) : V∞∞ i × V∞∞ i)
     → (¦ desup∞∞ x ¦ == ¦ desup∞∞ y ¦)
     ≃ (¦ desup∞∞ x ¦ ≃ ¦ desup∞∞ y ¦)
  eA (x , y) = equiv-univalence

  eB : ((x , y) : V∞∞ i × V∞∞ i)
     → (p : ¦ desup∞∞ x ¦ == ¦ desup∞∞ y ¦)
     → ¦ desup∞∞ x ¦ ≃ ¦ desup∞∞ x ¦
  eB (x , y) p = id-equiv

  er : ((x , y) : V∞∞ i × V∞∞ i)
     → (p : ¦ desup∞∞ x ¦ == ¦ desup∞∞ y ¦)
     → (a : ¦ desup∞∞ x ¦)
     → ((desup∞∞ x ↓) a , (desup∞∞ y ↓) (tr id p a))
     == ((desup∞∞ x ↓) a , (desup∞∞ y ↓) (map-eq p a))
  er (x , y) p a = eq-pair-eq-pr2 (ap (desup∞∞ y ↓) (compute-map-eq p a))

compute-equiv-==-⦉E⦊-⦉E'⦊-Coalg : ∀ {i}
                             → map-equiv
                                 (equiv-⦉ E (V∞∞ i , desup∞∞) ⦊-⦉ E' (V∞∞ i , desup∞∞) ⦊-Coalg eA eB er)
                                 ==-E-Coalg-V∞∞
                             == ==-E'-Coalg (V∞∞ i , desup∞∞)
compute-equiv-==-⦉E⦊-⦉E'⦊-Coalg =
  eq-pair-eq-pr2
    (eq-htpy (λ (x , y) →
      eq-htpy (λ { refl → refl })))

abstract
  ==-E'-Coalg-is-terminal : ∀ {i k}
    → is-terminal-⦉ E' (V∞∞ i , desup∞∞) ⦊-Coalg (==-E'-Coalg (V∞∞ i , desup∞∞)) k
  ==-E'-Coalg-is-terminal {i = i} {k} =
    tr (λ X → is-terminal-⦉ E' (V∞∞ i , desup∞∞) ⦊-Coalg X k)
       compute-equiv-==-⦉E⦊-⦉E'⦊-Coalg
       (map-equiv
         (equiv-is-terminal-⦉ E (V∞∞ i , desup∞∞) ⦊-⦉ E' (V∞∞ i , desup∞∞) ⦊-Coalg eA eB er
           (==-E-Coalg (V∞∞ i , desup∞∞)))
         ==-E-Coalg-is-terminal)

-- V∞∞ is locally small
is-locally-small-V∞∞ : ∀ {i} → is-locally-small i (V∞∞ i)
is-locally-small-V∞∞ {i} x y =
  (indexed-M (x , y) ,
  equiv-terminal-⦉ E' (V∞∞ i , desup∞∞) ⦊-Coalg
    (==-E'-Coalg (V∞∞ i , desup∞∞))
    indexed-M-Coalg
    ==-E'-Coalg-is-terminal
    indexed-M-is-terminal
    (x , y))
  where
    open import container.indexed.m-types (E' (V∞∞ i , desup∞∞))


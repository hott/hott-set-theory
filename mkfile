
MKSHELL=$PLAN9/bin/rc

AGDA=$HOME/bin/agda --no-project --without-K -i src -i agda-unimath/src

%.agdae:Q: %.agda
   echo 'Compiling.'
   $AGDA $stem.agda | ssam -e 'x/:[0-9]+,[0-9]+-[0-9]+/ x/-.*/ d' | ssam -e 's/:([0-9]+),([0-9]+)/:\1:\2/g' > $target
   echo 'Done compiling.'
   if ( grep Unsolved $target > /dev/null) {
	  echo 'Found unsolved metas.'
      echo '
      ───────────────
      ' >> $target
      /bin/echo -e ':m\n:quit' | $AGDA -I $stem.agda | ssam -ne 'x/Main>(.|\n)*/ p' | ssam -e 'x/Main. / d' | ssam -e 'x/^\?/ i/\n/' >> $target
   }
   test $target -nt $stem.agda

%.agdai:Q: %.agda %.agdae
   if (test $stem.agdae -nt $stem.agda) {
      echo 'Showing output of compilation.'
      cat $stem.agdae
   }
   echo 'Cleaning up.'
   test $target -nt $stem.agda && {rm $stem.agdae || echo 'Done'}

clean:QV:
  agdais=`{find . | grep '\.agda[ie]$'}\
        || echo 'Nothing to clean.'
  for (q in $agdais)
    rm -v $q || echo -n ''


all:QV: src/notation.agdai
   echo 'Done.'
